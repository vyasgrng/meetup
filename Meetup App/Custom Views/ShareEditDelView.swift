//
//  ShareEditDelView.swift
//  Meetup App
//
//  Created by STL on 09/01/20.
//  Copyright © 2020 Birju Bhatt. All rights reserved.
//

import Alamofire
import SwiftyJSON
import UIKit

class ShareEditDelView: UIView {
    
    static func getInstance(partyId: String, owner: UIViewController, refresh: @escaping ()->Void) -> ShareEditDelView {
        let view = Bundle.main.loadNibNamed("ShareEditDelView", owner: owner, options: nil)![0] as! ShareEditDelView
        view.owner = owner
        view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        view.partyId = partyId
        view.refresh = refresh
        return view
    }
    
    func show() {
        self.setVisibility()
        appDelegate.window?.addSubview(self)
    }
    
    @IBOutlet weak var m_viewShare: UIView!
    @IBOutlet weak var m_viewReport: UIView!
    @IBOutlet weak var m_viewDelete: UIView!
    @IBOutlet weak var m_viewEdit: UIView!
    @IBOutlet weak var m_viewCancel: UIView!
    @IBOutlet weak var m_constBottom: NSLayoutConstraint!
    private var partyId: String?
    private var owner: UIViewController?
    
    var isUpdatable: Bool = false
    var isCancelable: Bool = false
    var isUpcoming: Bool = false
    
    var refresh: (()->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    private func setupUI() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(dismiss))
        addGestureRecognizer(gesture)
        alpha = 0.0
        
        m_viewReport.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(report)))
        m_viewEdit.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(edit)))
        m_viewDelete.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(deleteTapped)))
        m_viewShare.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(share)))
        m_viewCancel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(cancel)))
        
    }
    
    private func setVisibility() {
        let isUser = UserDefaults.standard.currentUserType() == UserType.Joinee.rawValue
        
        /*1)My Create Event(Host create)
         if(updatable){
         share,delete,edit
         }else{
         share
         }
         2)Join Side (Home List and wish list)
         //Share,Report
         
         3)Upcoming Event
         //Share.Cancel Event*/
        
        if isUser {
            m_viewEdit.isHidden = true
            m_viewDelete.isHidden = true
            m_viewCancel.isHidden = !isUpcoming
            m_viewReport.isHidden = isUpcoming
        } else {
            m_viewEdit.isHidden = !isUpdatable
            m_viewDelete.isHidden = !isUpdatable
            m_viewCancel.isHidden = true
            m_viewReport.isHidden = true
        }
    }
    
    @objc func dismiss() {
        removeFromSuperview()
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        UIView.animate(withDuration: 0.3) {
            self.alpha = 1
        }
    }
    
    
    @objc private func share() {
        dismiss()
        
    }
    
    @objc private func report() {
        dismiss()
        let reprtVC = ReportEventViewController.getIntance(eventId: partyId!)
        owner?.present(reprtVC, animated: true)
    }
    
    @objc private func cancel() {
        self.owner?.present(ConfirmDialogViewController.getIntance {
            self.cancelRequest()
        }, animated: true)
        dismiss()
    }
    
    @objc private func edit() {
        dismiss()
        partyDetailAPI()
    }
    
    @objc private func deleteTapped() {
        self.owner?.present(ConfirmDialogViewController.getIntance {
            self.deleteRequest()
        }, animated: true)
        dismiss()
    }
    
    private func deleteRequest() {
        
        AppSingleton.shared.startLoading()
        let params = ["token":UserDefaults.standard.currentToken() ?? "",
                      "event_id": self.partyId ?? ""]
        Alamofire.request(AppConstant.API.cancel, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        self.refresh?()
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    private func cancelRequest() {
        
        AppSingleton.shared.startLoading()
        let params = ["token":UserDefaults.standard.currentToken() ?? "",
                      "event_id": self.partyId ?? ""]
        Alamofire.request(AppConstant.API.cancelBooking, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        self.refresh?()
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    //MARK:- Party detail api call
    func partyDetailAPI() {
        AppSingleton.shared.startLoading()
        let params = ["token":UserDefaults.standard.currentToken() ?? "",
                      "id": partyId ?? ""]
        Alamofire.request(AppConstant.API.partyDetail, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        do {
                            let rowData = try responseObject["data"]!.rawData()
                            let info = try JSONDecoder().decode(PartyDetails.self, from: rowData)
                            var startDate = ""
                            var startTime = ""
                            if let start = info.start {
                                startTime = start.timeStampToDate(format: "hh:mm a")
                                startDate = start.timeStampToDate(format: "yyyy-MM-dd")
                            }
                            var endDate = ""
                            var endTime = ""
                            if let end = info.end {
                                endTime = end.timeStampToDate(format: "hh:mm a")
                                endDate = end.timeStampToDate(format: "yyyy-MM-dd")
                            }
                            let currency = AppSingleton.shared.getCurrencyCode(currency:(info.currencyReadable ?? AppSingleton.shared.userData!.currency_readable)!)
                            let dict: [String: String] = [
                                "isEditing": "yes",
                                "id": info.id ?? "",
                                "name": info.name ?? "",
                                "theme": info.theme ?? "",
                                "startDate": startDate,
                                "endDate": endDate,
                                "startTime": startTime,
                                "endTime": endTime,
                                "no_of_guest": info.noOfGuest,
                                "price": info.price,
                                "currency": currency,
                                "address": info.address ?? "",
                                "city": info.city ?? "",
                                "state": info.state ?? "",
                                "zip_code": info.zipCode ?? "",
                                "country": info.country ?? "",
                                "images": (info.images ?? []).joined(separator: ","),
                                "description": info.descriptionField ?? "",
                                "facilities": info.facilities ?? "",
                                "terms": info.terms ?? "",
                                "lat": info.lat,
                                "lng": info.lng
                            ]
                            
                            let addPartyVC = AddPartyVC.instance
                            addPartyVC.dictData = dict
                            self.owner?.navigationController?.pushViewController(addPartyVC, animated: true)
                        } catch(let error) {
                            print(error.localizedDescription)
                        }
                        
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
}
