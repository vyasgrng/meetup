//
//  PaymentSelectionVC.swift
//  Meetup App
//
//  Created by Aditi Pancholi on 03/08/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol  ProtocolPaymentSelectionVC {
    func navigateToChangeDefualtCard()
}

class PaymentSelectionVC: UIView {
    
    func setInfo(eventId: String?) {
        self.eventId = eventId
    }

    @IBOutlet weak var lblValletBalance: UILabel!
    @IBOutlet weak var m_viewWalletsContraint: NSLayoutConstraint!
    @IBOutlet weak var m_bottomVeiwHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblCardName: UILabel!
    @IBOutlet weak var lblCardNumber: UILabel!
    
    var delegate : ProtocolPaymentSelectionVC!
    var eventId: String?
    var cardId: String?
    
    
    @IBAction func btnChangeDefaultCard(_ sender: UIButton) {
        delegate.navigateToChangeDefualtCard()
    }
    
    
    @IBAction func btnCancelAction(_ sender: Any) {
        self.removeViewWithAnimation()
    }
    
    @IBAction func btnProcedePaymentAction(_ sender: Any) {
        //self.removeViewWithAnimation()
        guard let cardId = self.cardId, let eventId = self.eventId else {
            return
        }
    
        let params: [String : Any] = ["token":UserDefaults.standard.currentToken()!,
                      "event_id": eventId,
                      "card_id": cardId,
                      "use_wallet": false,
                      "pay_type": "stripe"]
        
        AppSingleton.shared.startLoading()
        Alamofire.request(AppConstant.API.makePayment, method: .post, parameters: params as Parameters,encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if let info = responseObject["data"]?.dictionaryValue, let paid = info["paid"]?.stringValue{
                        print(paid)
                        AppSingleton.shared.showSuccessAlert(msg: "Payment successful")
                        self.removeViewWithAnimation()
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func addViewWithAnimation(onView:UIView){
        addTapGestureToBgView()
        if let tabarVC = onView.viewContainingController()?.getTabarVC() {
            m_bottomVeiwHeightContraint.constant = tabarVC.bottomBarView.frame.height - 30
        }
        if UserDefaults.standard.getReferralCurrency() == " "{
            m_viewWalletsContraint.constant = 0
            self.layoutIfNeeded()
        }else{
            lblValletBalance.text = "$ \(UserDefaults.standard.getReferralCurrency())"
        }
        getListOfSavedCards()
        self.viewBg.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        self.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width:UIScreen.main.bounds.width , height: UIScreen.main.bounds.height)
        onView.addSubview(self)
        UIView.animate(withDuration: 1, delay: 0, options: [.curveEaseIn], animations: {
           // self.transform.scaledBy(x: 1, y: 1)
            self.frame = CGRect(x: 0, y: 0, width:UIScreen.main.bounds.width , height: UIScreen.main.bounds.height)
        }) { (success) in }
    
        UIView.animate(withDuration: 0.2, delay: 0.8, options: .curveEaseIn, animations: {
            self.viewBg.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        }) { (success) in
        }
        
    }
    
    func removeViewWithAnimation(){
        self.viewBg.backgroundColor = .clear
        UIView.animate(withDuration: 0.8, delay: 0, options: [.curveEaseIn], animations: {
            self.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width:UIScreen.main.bounds.width , height: UIScreen.main.bounds.height)
        }) { (success) in
            self.removeFromSuperview()
        }
    }
    
    func getListOfSavedCards(){
        //AppSingleton.shared.startLoading()
        let params = ["token":UserDefaults.standard.currentToken()!]
        Alamofire.request(AppConstant.API.cardList, method: .post, parameters: params as Parameters,encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                   // AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        let cardDetails = responseObject["data"]!.dictionaryValue
                        self.lblCardNumber.text = "Card ending in \(cardDetails["default_card"]!["last4"].intValue)"
                        self.lblCardName.text = cardDetails["default_card"]!["name"].stringValue
                        self.cardId = cardDetails["default_card"]?["id"].stringValue
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func addTapGestureToBgView(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideView))
        viewBg.addGestureRecognizer(tapGesture)
    }
    
    
    @objc func hideView(){
        self.removeViewWithAnimation()
    }
}
