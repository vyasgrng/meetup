//
//  PaymentMethodsVC.swift
//  Meetup App
//
//  Created by STL on 28/01/20.
//  Copyright © 2020 Birju Bhatt. All rights reserved.
//

import UIKit

class PaymentMethodsVC: BaseVC {

    @IBOutlet weak var m_constTop: NSLayoutConstraint!
    @IBOutlet weak var m_viewCreditDebitCard: UIView!
    @IBOutlet weak var m_btnAddCard: DefaultButton!
    @IBOutlet weak var m_viewPaypal: UIView!
    @IBOutlet weak var m_btnLinkAccount: DefaultButton!
    @IBOutlet weak var m_viewRewardCoins: UIView!
    @IBOutlet weak var m_lblReward: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
    }

    //MARK:- Basic Setup
    func basicSetUp() {
        addBackButton(isFirstHierarchy: false)
        addTopTitle(title: "Payment Methods")
        m_constTop.constant = topView.frame.height + 10
        
        m_viewCreditDebitCard.addShadowWithCorner(cornerRadius: 5, offset: .zero, color: .lightGray, radius: 2, opacity: 0.6)
        m_viewPaypal.addShadowWithCorner(cornerRadius: 5, offset: .zero, color: .lightGray, radius: 2, opacity: 0.6)
        m_viewRewardCoins.addShadowWithCorner(cornerRadius: 5, offset: .zero, color: .lightGray, radius: 2, opacity: 0.6)
        m_btnAddCard.removeCorners()
        m_btnLinkAccount.removeCorners()
        
    }
    
    //MARK:- Action Add Card
    @IBAction func actionAddCard(_ sender: DefaultButton) {
        let vcObj = self.storyboard?.instantiateViewController(withIdentifier: "AddCardVC") as? AddCardVC
        self.navigationController?.pushViewController(vcObj!, animated: true)
    }
    //MARK:- Action Link Account Paypal
    @IBAction func actionLinkAcc(_ sender: DefaultButton) {
        
    }
}
