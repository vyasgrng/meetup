//
//  ConfirmDialogViewController.swift
//  Meetup App
//
//  Created by Gaurang Vyas on 03/04/20.
//  Copyright © 2020 Birju Bhatt. All rights reserved.
//

import UIKit

class ConfirmDialogViewController: UIViewController {
    
    static func getIntance(completion: @escaping () -> Void) -> ConfirmDialogViewController {
        let vc = ConfirmDialogViewController(nibName: "ConfirmDialogViewController", bundle: nil)
        vc.completion = completion
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        return vc
    }
    @IBOutlet var contentView: UIView!
    @IBOutlet var messageLabel: UILabel!
    @IBOutlet var yesButton: UIButton!
    @IBOutlet var nobutton: UIButton!
    
    var completion: (() -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
        
    }
    
    private func prepareUI() {
        contentView.layer.cornerRadius = 8
        yesButton.layer.cornerRadius = 6
        nobutton.layer.cornerRadius = 6
        nobutton.layer.borderColor = UIColor.lightGray.cgColor
        nobutton.layer.borderWidth = 0.5
        messageLabel.text = "Are you sure You want to cancel this Event?"
    }

    @IBAction func closeTapped() {
        dismiss(animated: true)
    }
    
    @IBAction func yesTapped() {
        dismiss(animated: true) {
            self.completion?()
        }
    }
}
