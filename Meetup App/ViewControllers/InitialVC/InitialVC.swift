//
//  InitialVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 02/04/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class InitialVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if UserDefaults.standard.currentToken()!.count != 0 {
            checkLogin()
        } else {
            if UserDefaults.standard.isIntroVisit() {
                redirectToWelcomScreen()
            } else {
                redirectToIntroScreen()
            }
        }
    }
    
     //MARK:- Check user existance
    func checkLogin() {
        AppSingleton.shared.startLoading()
        let params = ["token":UserDefaults.standard.currentToken()!]
        Alamofire.request(AppConstant.API.autoLogin, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                        self.redirectToWelcomScreen()

                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                        UserDefaults.standard.removeToken()
                        self.redirectToWelcomScreen()
                    } else {
                        UserDefaults.standard.storeToken(token: responseObject["data"]!["token"].stringValue)
                        UserDefaults.standard.set(encodable: UserModal(data: JSON(value)), forKey: "userData")
                                                
                        //====
                        appDelegate.setRootAfterLogin()
                        //====
                        appDelegate.socketConnection()
                        
                        
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }

    func redirectToWelcomScreen() {
        let welcomeVC = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeVC") as? WelcomeVC
        self.navigationController?.pushViewController(welcomeVC!, animated: true)
    }

    func redirectToIntroScreen() {
         let vcObj = UIStoryboard(name: "Intro", bundle: Bundle.main).instantiateViewController(withIdentifier: "IntroVC") as? IntroVC
        self.navigationController?.pushViewController(vcObj!, animated: true)
    }

}
