//
//  BaseVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 12/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import SnapKit

class BaseVC: UIViewController {

    let topView = UIView()
//    let bgImgView = UIImageView()
    let backButton = UIButton()
    let rightButton = UIButton()
    var paddingY = 10.0
    var topTitleLabel = UILabel()
    var subTitleLabel = UILabel()
    let userImage = UIImageView.init()

    
    var hasTopNotch: Bool {
        if #available(iOS 11.0,  *) {
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        initialSetUp()
    }


    //MARK:- Setup elements
    func initialSetUp() {
        
//        bgImgView.image = UIImage(named: "bg")
//        self.view.addSubview(bgImgView)
//        self.view.sendSubviewToBack(bgImgView)
//        bgImgView.snp.makeConstraints { (make) in
//            make.leading.trailing.top.equalToSuperview()
//        }
        
        topView.backgroundColor = UIColor().appThemeBlueColor()
        self.view.addSubview(topView)
        topView.snp.makeConstraints { (make) in
            make.top.leading.trailing.equalToSuperview()
            if hasTopNotch {
                paddingY = 20.0
                make.height.equalTo(104)
            } else {
                make.height.equalTo(84)
            }
        }
        
        hideBgImage()
    }
    
    //MARK:- Hide background image
    func hideBgImage() {
//        bgImgView.isHidden = true
    }
    //MARK:- Add back button
    func addBackButton(withImage:UIImage = #imageLiteral(resourceName: "ic_back").maskWithColor(color: .white)!,isFirstHierarchy:Bool = true) {
        backButton.setImage(withImage, for: .normal)
        if isFirstHierarchy {
            backButton.addTarget(self, action: #selector(backClickedForFirHierarchy(_:)), for: .touchUpInside)
        } else {
            backButton.addTarget(self, action: #selector(backClicked(_:)), for: .touchUpInside)
        }
        topView.addSubview(backButton)
        
        backButton.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(15)
            make.centerY.equalToSuperview().offset(paddingY)
            make.width.equalTo(withImage.size.width + 5)
            make.height.equalTo(withImage.size.height + 5)
        }
    }
    
    // Back button action
    @objc func backClicked(_ sender:UIButton) {
        self.popVC()
    }
    
    @objc func backClickedForFirHierarchy(_ sender:UIButton) {
        let storyboardTab = UIStoryboard(name: "Tabbar", bundle: Bundle.main)
        let mainViewController = storyboardTab.instantiateViewController(withIdentifier: "ParentVC") as! ParentVC
        let nvc = UINavigationController(rootViewController:mainViewController)
        nvc.navigationBar.isHidden = true
        self.slideMenuController()?.changeMainViewController(nvc, close: true)
    }
    
    //MARK:- Add right button
    func addRightButton(withText:String = "", withImageName:String = "",withImgColor:UIColor = .clear, vc:UIViewController, target:Selector) {
        
        rightButton.addTarget(vc, action: target, for: .touchUpInside)
        topView.addSubview(rightButton)
        if withText.isEmpty {
            let img = UIImage(named: withImageName)
            if withImgColor != .clear {
                rightButton.setImage(img?.maskWithColor(color: withImgColor), for: .normal)
            } else {
                rightButton.setImage(img, for: .normal)
            }
            
            rightButton.snp.makeConstraints { (make) in
                make.trailing.equalToSuperview().offset(-20)
                make.centerY.equalToSuperview().offset(paddingY)
                make.width.equalTo(img!.size.width + 5)
                make.height.equalTo(img!.size.height + 5)
            }
        } else {
            rightButton.setTitle(withText, for: .normal)
            rightButton.setTitleColor(UIColor.init(red: 12, green: 25, blue: 105, alpha: 1.0), for: .normal)
            rightButton.titleLabel!.font = UIFont().RobotoBold(size: 21.0)

            rightButton.snp.makeConstraints { (make) in
                make.trailing.equalToSuperview().offset(-20)
                make.bottom.equalToSuperview().offset(0.0)
                make.width.equalTo(50)
                make.height.equalTo(25)
            }
        }
    }
    
    //MARK:- Add top title
    func addTopTitle(title:String) {
        topTitleLabel.text = title
        topTitleLabel.textColor = .white//UIColor.init(red: 12, green: 25, blue: 105, alpha: 1.0)
        topTitleLabel.font = UIFont().RobotoMedium(size: 20.0)
        topView.addSubview(topTitleLabel)
        
        topTitleLabel.snp.makeConstraints { (make) in
//            make.bottom.equalToSuperview().offset(-10.0)
            make.centerY.equalTo(backButton.snp.centerY)
            
            if backButton.isDescendant(of: topView) {
                make.leading.equalTo(backButton.snp.trailing).offset(35)
            } else {
                make.leading.equalToSuperview().offset(60.0)
            }
        }
    }
    
    //MARK: Setup navigationbar for chatting vc
    func setupHeader(title:String ,userImg : UIImage, subTitleText : String = ""){
        
        //set user profile photo
        userImage.image = userImg
        topView.addSubview(userImage)
        userImage.contentMode = .scaleAspectFill
        
        userImage.snp.makeConstraints { (make) in
            make.width.equalTo(40)
            make.height.equalTo(40)
           // make.centerY.equalTo(topView.snp.centerY)
            make.bottom.equalToSuperview().offset(-10.0)

            if backButton.isDescendant(of: topView) {
                make.leading.equalTo(backButton.snp.trailing).offset(10)
            } else {
                make.leading.equalToSuperview().offset(35.0)
            }
        }

        
        let outerView = UIView()
        outerView.backgroundColor = .clear
        topView.addSubview(outerView)
        
        outerView.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(0.0)
            make.leading.equalTo(userImage.snp.trailing).offset(10)
        }
        
        topTitleLabel.text = title
        topTitleLabel.textColor = .white
            //UIColor.init(red: 12, green: 25, blue: 105, alpha: 1.0)
        topTitleLabel.font = UIFont().RobotoRegular(size: 19.0)
        //ABBold(size: 18.0)
        topTitleLabel.textAlignment = .left
        //topView.addSubview(topTitleLabel)
        outerView.addSubview(topTitleLabel)

        topTitleLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(-10.0)
            make.left.right.equalTo(outerView)
            //make.leading.equalTo(userImage.snp.trailing).offset(10)
        }
        
        subTitleLabel.text = subTitleText
        subTitleLabel.textColor = .white
        subTitleLabel.textAlignment = .left
        subTitleLabel.font = UIFont().RobotoLight(size: 12.0)
        //topView.addSubview(subTitleLabel)
        outerView.addSubview(subTitleLabel)
        
        
        subTitleLabel.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-10.0)
            make.left.right.equalTo(outerView)
            make.top.equalTo(topTitleLabel.snp.bottom).offset(4.0)
            //make.leading.equalTo(userImage.snp.trailing).offset(10)
        }
        
        
    }
}

