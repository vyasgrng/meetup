//
//  InvitationVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 23/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class InvitationVC: BaseVC {

    @IBOutlet weak var m_constTop: NSLayoutConstraint!
    
    @IBOutlet weak var m_tblInvitations: UITableView!
    var arrayInvitations:Array<JSON> = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
    }
    
    
    //MARK:- Set initial UI
    func basicSetUp() {
        addBackButton(withImage: #imageLiteral(resourceName: "Burger Button"),isFirstHierarchy: false)
        addTopTitle(title: "Invitations")
        m_constTop.constant = topView.frame.height + 5
        
        //Get ongoing list
        invitationAPI()
        m_tblInvitations.delegate = self
        m_tblInvitations.dataSource = self
        m_tblInvitations.tableFooterView = UIView()
    }
    
    override func backClicked(_ sender: UIButton) {
        self.slideMenuController()?.openLeft()
    }
    
    //MARK:- Get invitations list
    func invitationAPI() {
        AppSingleton.shared.startLoading()
        let params = ["token":UserDefaults.standard.currentToken()!]
        Alamofire.request(AppConstant.API.invitationList, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        self.arrayInvitations = (responseObject["data"]?.arrayValue)!
                        self.m_tblInvitations.reloadData()
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    //MARK:- Accept api action
    @objc func acceptInvitation(_ sender: UIButton) {
        let data = arrayInvitations[sender.tag].dictionaryValue
        acceptRejectAPI(action: "accept", eventId: data["event"]!["id"].stringValue, userId: data["user"]!["id"].stringValue,index: sender.tag)
    }
    
    //MARK:- Reject api action
    @objc func rejectInvitation(_ sender: UIButton) {
        let data = arrayInvitations[sender.tag].dictionaryValue
        acceptRejectAPI(action: "reject", eventId: data["user"]!["id"].stringValue, userId: data["event"]!["id"].stringValue,index: sender.tag)
    }

    //MARK: Accept Reject Api call
    func acceptRejectAPI(action:String, eventId:String, userId:String, index:Int) {
        AppSingleton.shared.startLoading()
        let params = ["token":UserDefaults.standard.currentToken()!,
                      "event_id":eventId,
                      "user_id":userId,
                      "response":action]
        Alamofire.request(AppConstant.API.eventAcceptReject, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                       self.arrayInvitations.remove(at: index)
                        self.m_tblInvitations.deleteRows(at: [IndexPath(row: index, section: 0)], with: UITableView.RowAnimation.fade)
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func boldCustomString(wholeStr:String, particuarStr:[String]) -> NSAttributedString {
        let main_string = wholeStr
        let attributedString = NSMutableAttributedString(string:main_string)
        
        for i in particuarStr {
            let range = (main_string as NSString).range(of: i)
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont().RobotoBold(size: 13.0) , range: range)
        }
        
        return attributedString
    }
}

extension InvitationVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayInvitations.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = arrayInvitations[indexPath.row].dictionaryValue
        let cell = tableView.dequeueReusableCell(withIdentifier: "InvitationCell") as? InvitationCell
        let wholeStr = data["user"]!["first_name"].stringValue + " " + data["user"]!["last_name"].stringValue + " " + "Requested to join your party"
        cell?.m_lblUserName.attributedText = boldCustomString(wholeStr: wholeStr, particuarStr: [data["user"]!["first_name"].stringValue + " " + data["user"]!["last_name"].stringValue])
        let timeinterval = data["requested_at"]?.stringValue.dropLast(3)
        let date = Date(timeIntervalSince1970: (timeinterval! as NSString).doubleValue)
        cell?.m_lblTime.text = date.timeAgoSinceDate()
        cell?.m_imgViewUser.kf.setImage(with:  data["user"]!["profile_pic"].url, placeholder: UIImage(named: "image01"), options: [], progressBlock: nil, completionHandler: { (result) in
            cell?.m_imgViewUser.addCornerRadius(radius: 10.0)
        })        
        cell?.m_btnAccept.tag = indexPath.row
        cell?.m_btnDeclined.tag = indexPath.row
        cell?.m_btnAccept.addTarget(self, action: #selector(acceptInvitation(_:)), for: .touchUpInside)
        cell?.m_btnDeclined.addTarget(self, action: #selector(rejectInvitation(_:)), for: .touchUpInside)
        return cell!
    }
    
}
