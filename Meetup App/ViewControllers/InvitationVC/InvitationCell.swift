//
//  InvitationCell.swift
//  Meetup App
//
//  Created by Birju Bhatt on 23/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class InvitationCell: UITableViewCell {

    
    @IBOutlet weak var m_btnDeclined: UIButton!
    @IBOutlet weak var m_btnAccept: DefaultButton!
    @IBOutlet weak var m_lblUserName: UILabel!
    @IBOutlet weak var m_imgViewUser: UIImageView!
    @IBOutlet weak var m_viewContain: UIView!
    @IBOutlet weak var m_lblTime: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
                    
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        m_btnDeclined.addBorder(width: 1, color: UIColor().convertHexStringToColor(hexString: "E94A47"))
        m_btnDeclined.addCornerRadius(radius: 8)
        m_viewContain.addShadowWithCorner(cornerRadius: 5, offset: .zero, color: .lightGray, radius: 2, opacity: 0.6)
        // Configure the view for the selected state
    }

}
