//
//  PartyDetailImagesCollectionCell.swift
//  Meetup App
//
//  Created by Birju Bhatt on 24/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class PartyDetailImagesCollectionCell: UICollectionViewCell {
    @IBOutlet weak var m_imgView: UIImageView!
    
}
