//
//  AddPartyTimeCell.swift
//  Meetup App
//
//  Created by Birju Bhatt on 21/05/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class AddPartyTimeCell: UITableViewCell {

    @IBOutlet weak var m_txtFldEndTime: UITextField!
    @IBOutlet weak var m_viewEndTime: UIView!
    @IBOutlet weak var m_viewStartTime: UIView!
    @IBOutlet weak var m_txtFldStartTime: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
