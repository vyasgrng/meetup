//
//  AddPartyVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 24/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

struct addPartyData {
    var placeHolders:String?
    var key:String?
    var kbType:UIKeyboardType?
    var isDropDown:Bool?
}

class AddPartyVC: BaseVC {
    
    static var instance: AddPartyVC {
        let vc = StoryboardID.party.getViewController(sceneId: "AddPartyVC") as! AddPartyVC
        return vc
    }
    
    var dictData:Dictionary<String,String>?
    let datePicker = UIDatePicker()
    let timePicker = UIDatePicker()
    var arrImages:Array<UIImage> = []
    var txtFldStartTime = UITextField()
    var txtFldEndTime = UITextField()
    var txtFldStartDate = UITextField()
    var txtFldEndDate = UITextField()
    var structAddParty:[addPartyData]?
    
    @IBOutlet weak var m_btnAdd: DefaultButton!
    @IBOutlet weak var m_tblView: UITableView!
    @IBOutlet weak var m_constTop: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
    }
    
    //MARK:- Set initial UI
    func basicSetUp() {
        addBackButton()
        addTopTitle(title: "Add Party")
        m_constTop.constant = topView.frame.size.height + 15
        m_tblView.delegate = self
        m_tblView.dataSource = self
        //        m_btnAdd.addCornerRadius(radius: 8)
        //        m_btnAdd.addBorder(width: 1.0, color: UIColor(red: 189, green: 211, blue: 229, alpha: 1.0))
        //        m_btnAdd.gradientBackground(from: UIColor(red: 93, green: 73, blue: 230, alpha: 1.0), to: UIColor(red: 40, green: 117, blue: 208, alpha: 1.0), direction: .rightToLeft)
        
        structAddParty = [
            addPartyData(placeHolders: "Party Name",key:"name",kbType:.default,isDropDown:true),
            addPartyData(placeHolders: "Party Theme",key:"theme",kbType:.default,isDropDown:true),
            //addPartyData(placeHolders: "Party Date",key:"date",kbType:.default,isDropDown:true),
            addPartyData(placeHolders: "Start Date,End Date",key:"start,end",kbType:.default,isDropDown:true),
            addPartyData(placeHolders: "Start Time,End Time",key:"start,end",kbType:.default,isDropDown:true),
            addPartyData(placeHolders: "No. of Guest",key:"no_of_guest",kbType:.numberPad,isDropDown:true),
            addPartyData(placeHolders: "Pricing ($)",key:"price",kbType:.numberPad,isDropDown:true),
        ]
        
        if self.dictData?["isEditing"] != "yes" {
            dictData = ["name": "",
                        "theme": "",
                        "startDate": "",
                        "endDate": "",
                        "startTime": "",
                        "endTime": "",
                        "no_of_guest": "",
                        "price": "",
                        "currency": AppSingleton
                            .shared
                            .getCurrencyCode(currency:(AppSingleton.shared.userData!.currency_readable)!)]
        }
        
        
    }
    
    //MARK: Show Date Picker
    func showDatePicker(textField: UITextField){
        //Formate Date
        datePicker.datePickerMode = .date
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        //done button & cancel button
        let selector = textField == txtFldStartDate ? #selector(doneStartDatePicker) : #selector(doneEndDatePicker)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: selector)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        // add toolbar to textField
        textField.inputAccessoryView = toolbar
        // add datepicker to textField
        textField.inputView = datePicker
    }
    
    @objc func doneStartDatePicker() {
        donedatePicker(txtFldStartDate)
    }
    
    @objc func doneEndDatePicker() {
        donedatePicker(txtFldEndDate)
    }
    
    func donedatePicker(_ sender: UITextField){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        sender.text = formatter.string(from: datePicker.date)
        //dismiss date picker dialog
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
    }
    
    //MARK: Show Date Picker
    func showTimePickerForStartTime(){
        //Formate Date
        timePicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneStartTimePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelStartTimePicker))
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        // add toolbar to textField
        txtFldStartTime.inputAccessoryView = toolbar
        // add datepicker to textField
        txtFldStartTime.inputView = timePicker
    }
    @objc func doneStartTimePicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        txtFldStartTime.text! = formatter.string(from: timePicker.date)
        //dismiss date picker dialog
        self.view.endEditing(true)
    }
    
    @objc func cancelStartTimePicker(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
    }
    
    //MARK: Show Date Picker
    func showTimePickerForEndTime(){
        //Formate Date
        timePicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneEndTimePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelEndTimePicker))
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        // add toolbar to textField
        txtFldEndTime.inputAccessoryView = toolbar
        // add datepicker to textField
        txtFldEndTime.inputView = timePicker
    }
    @objc func doneEndTimePicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        txtFldEndTime.text! = formatter.string(from: timePicker.date)
        //dismiss date picker dialog
        self.view.endEditing(true)
    }
    
    @objc func cancelEndTimePicker(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
    }
    
    //MARK:- Data validation
    func dataValidation() -> Bool {
        self.view.endEditing(true)
        if (dictData!["name"]?.isEmpty)! {
            AppSingleton.shared.showErrorAlert(msg: "Name Required.")
            return false
        } else if (dictData!["theme"]?.isEmpty)! {
            AppSingleton.shared.showErrorAlert(msg: "Theme Required.")
            return false
        } else if (dictData!["startDate"]?.isEmpty)! {
            AppSingleton.shared.showErrorAlert(msg: "Start Date Required.")
            return false
        } else if (dictData!["endDate"]?.isEmpty)! {
            AppSingleton.shared.showErrorAlert(msg: "End Date Required.")
            return false
        } else if (dictData!["startTime"]?.isEmpty)! {
            AppSingleton.shared.showErrorAlert(msg: "Start Required.")
            return false
        } else if (dictData!["endTime"]?.isEmpty)! {
            AppSingleton.shared.showErrorAlert(msg: "End Required.")
            return false
        } else if (dictData!["no_of_guest"]?.isEmpty)! {
            AppSingleton.shared.showErrorAlert(msg: "Guest Required.")
            return false
        } else if (dictData!["price"]?.isEmpty)! {
            AppSingleton.shared.showErrorAlert(msg: "Price Required.")
            return false
        }
        return true
    }
    
    
    //MARK:- Go to Party Address
    @IBAction func goToPartyAddress(_ sender: UIButton) {
        
        if dataValidation() {
            
            let selectedStartTime = dictData!["startDate"]! + " " + dictData!["startTime"]!
            let selectedEndTime = dictData!["endDate"]! + " " + dictData!["endTime"]!
            
            let format = DateFormatter()
            format.dateFormat = "yyyy-MM-dd hh:mm a"
            let dtStart = format.date(from: selectedStartTime)
            let dtEnd = format.date(from: selectedEndTime)
            
            dictData!["startDate"] = "\(dictData!["startDate"]!.convertToDate(format: "yyyy-MM-dd").timeIntervalSince1970 * 1000.0)"
            dictData!["endDate"] = "\(dictData!["endDate"]!.convertToDate(format: "yyyy-MM-dd").timeIntervalSince1970 * 1000.0)"
            dictData!["start"] = "\(dtStart!.timeIntervalSince1970 * 1000)"
            dictData!["end"] = "\(dtEnd!.timeIntervalSince1970 * 1000)"
            
            let partyAddObj = self.storyboard?.instantiateViewController(withIdentifier: "PartyAddressVC") as? PartyAddressVC
            partyAddObj?.dictData = dictData!
            self.navigationController?.pushViewController(partyAddObj!, animated: true)
        }
    }
}

extension AddPartyVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return structAddParty!.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddPartyTimeCell") as? AddPartyTimeCell
            
            cell?.m_viewStartTime.addShadow(offset: .zero, color: .lightGray, opacity: 0.6, radius: 2)
            cell?.m_viewEndTime.addShadow(offset: .zero, color: .lightGray, opacity: 0.6, radius: 2)
            
            cell?.m_txtFldStartTime.placeholder = structAddParty![indexPath.row].placeHolders?.components(separatedBy: ",").first
            cell?.m_txtFldEndTime.placeholder = structAddParty![indexPath.row].placeHolders?.components(separatedBy: ",").last
            
            cell?.m_txtFldStartTime.delegate = self
            cell?.m_txtFldEndTime.delegate = self
            
            cell?.m_txtFldStartTime.text = dictData!["startDate"]
            cell?.m_txtFldEndTime.text = dictData!["endDate"]
            
            txtFldStartDate = (cell?.m_txtFldStartTime)!
            txtFldEndDate = (cell?.m_txtFldEndTime)!
            
            cell?.m_txtFldStartTime.tag = indexPath.row
            cell?.m_txtFldEndTime.tag = indexPath.row
            
            return cell!
        }
        else if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddPartyTimeCell") as? AddPartyTimeCell
            
            //            cell?.m_viewStartTime.addShadowDefault()//(isOnlyRightSide: true)
            //            cell?.m_viewEndTime.addShadowDefault()//(isOnlyRightSide: true)
            cell?.m_viewStartTime.addShadow(offset: .zero, color: .lightGray, opacity: 0.6, radius: 2)
            cell?.m_viewEndTime.addShadow(offset: .zero, color: .lightGray, opacity: 0.6, radius: 2)
            
            
            cell?.m_txtFldStartTime.placeholder = structAddParty![indexPath.row].placeHolders?.components(separatedBy: ",").first
            cell?.m_txtFldEndTime.placeholder = structAddParty![indexPath.row].placeHolders?.components(separatedBy: ",").last
            
            cell?.m_txtFldStartTime.delegate = self
            cell?.m_txtFldEndTime.delegate = self
            
            cell?.m_txtFldStartTime.text = dictData!["startTime"]
            cell?.m_txtFldEndTime.text = dictData!["endTime"]
            
            txtFldStartTime = (cell?.m_txtFldStartTime)!
            txtFldEndTime = (cell?.m_txtFldEndTime)!
            
            cell?.m_txtFldStartTime.tag = indexPath.row
            cell?.m_txtFldEndTime.tag = indexPath.row
            
            return cell!
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddPartyDetailsCell") as? AddPartyDetailsCell
            //            cell?.m_viewContain.addShadowDefault()//(isOnlyRightSide: true)
            cell?.m_viewContain.addShadow(offset: .zero, color: .lightGray, opacity: 0.6, radius: 2)
            cell?.m_txtFld.placeholder = structAddParty![indexPath.row].placeHolders
            cell?.m_txtFld.tag = indexPath.row
            cell?.m_txtFld.delegate = self
            cell?.m_txtFld.keyboardType = structAddParty![indexPath.row].kbType!
            cell?.m_txtFld.text = dictData![structAddParty![indexPath.row].key!]
            return cell!
        }
    }
}
extension AddPartyVC: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtFldStartDate {
            dictData?["startDate"] = textField.text
        } else if textField == txtFldEndDate {
            dictData?["endDate"] = textField.text
        } else if textField == txtFldStartTime {
            dictData!["startTime"] = textField.text
        } else if textField == txtFldEndTime {
            dictData!["endTime"] = textField.text
        } else {
            dictData![structAddParty![textField.tag].key!] = textField.text
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtFldStartDate || textField == txtFldEndDate {
            showDatePicker(textField: textField)
        } else if textField == txtFldStartTime {
            showTimePickerForStartTime()
        } else if textField == txtFldEndTime {
            showTimePickerForEndTime()
        }
        return true
    }
}
