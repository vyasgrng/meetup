//
//  PartyDetailCollectionMemberCell.swift
//  Meetup App
//
//  Created by Birju Bhatt on 24/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class PartyDetailCollectionMemberCell: UICollectionViewCell {
    
    @IBOutlet weak var m_lblUserName: UILabel!
    @IBOutlet weak var m_btnUserPhoto: UIButton!
    @IBOutlet weak var m_imgView: UIImageView!
    @IBOutlet weak var m_btnRemove: UIButton!
}
