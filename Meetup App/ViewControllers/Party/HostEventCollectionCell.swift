//
//  HostEventCollectionCell.swift
//  Meetup App
//
//  Created by Birju Bhatt on 25/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class HostEventCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var m_imgView: UIImageView!
    @IBOutlet weak var m_viewDate: UIView!
    @IBOutlet weak var m_lblDate: UILabel!
    @IBOutlet weak var m_lblName: UILabel!
    @IBOutlet weak var m_btnTheme: UIButton!
    @IBOutlet weak var m_lblPrice: UILabel!
}
