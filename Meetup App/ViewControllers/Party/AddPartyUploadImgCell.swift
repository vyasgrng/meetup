//
//  AddPartyUploadImgCell.swift
//  Meetup App
//
//  Created by Birju Bhatt on 24/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class AddPartyUploadImgCell: UITableViewCell {

    @IBOutlet weak var m_imgViewUpload: UIImageView!
    @IBOutlet weak var m_collectionViewImages: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
