//
//  PartyAddedVC.swift
//  Meetup App
//
//  Created by STL on 23/12/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class PartyAddedVC: UIViewController {

    static func getInstance(message: String?) -> PartyAddedVC {
        let vc = StoryboardID.party.getViewController(sceneId: "PartyAddedVC") as! PartyAddedVC
        vc.message = message ?? "Your Party Added Successfully"
        return vc
    }
    var message: String = "Your Party Added Successfully"
    @IBOutlet var m_messageLabel: UILabel!
    @IBOutlet weak var m_viewContain: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        m_messageLabel.text = message
        m_viewContain.addShadow(offset: CGSize(width: 0, height: 5), color: .lightGray, opacity: 0.8, radius: 5)
    }
    @IBAction func actionDone(_ sender: DefaultButton) {
        
        for vc in self.navigationController!.viewControllers {
            if vc is ParentVC {
                self.navigationController?.popToViewController(vc, animated: true)
            }
        }
    }
}
