//
//  PartyDescVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 21/05/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import IQKeyboardManagerSwift

class PartyDescVC: BaseVC {

    @IBOutlet weak var m_btnAdd: DefaultButton!
    @IBOutlet weak var m_viewContain: UIView!
    @IBOutlet weak var m_txtView: IQTextView!
    @IBOutlet weak var m_constTop: NSLayoutConstraint!
    var dictData:Dictionary<String,Any> = ["":""]
    var isUpdating = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
    }
    
    //MARK:- Set initial UI
    func basicSetUp() {
        addBackButton(isFirstHierarchy: false)
        addTopTitle(title: "Add Party")
        m_constTop.constant = topView.frame.height + 15
//        m_viewContain.addShadowDefault(isOnlyRightSide: true)
        m_viewContain.addShadow(offset: .zero, color: .lightGray, opacity: 0.6, radius: 2)
        isUpdating = (self.dictData["isEditing"] as? String) == "yes"
        if !isUpdating {
            dictData["description"] = ""
            dictData["facilities"] = ""
            dictData["terms"] = ""
            m_txtView.text = ""
        } else {
            m_txtView.text = dictData["description"] as? String
        }
        m_btnAdd.setTitle(isUpdating ? "Save" : "Add", for: .normal)
        
    }
    
    //MARK:- Action Add Party
    @IBAction func actionAdd(_ sender: UIButton) {
        
        m_txtView.resignFirstResponder()
        var params:Dictionary<String,String> = ["token":UserDefaults.standard.currentToken()!,
                                                "name":dictData["name"] as! String,
                                                "theme":dictData["theme"] as! String,
                                                "date":dictData["startDate"] as! String,
                                                "no_of_guest":dictData["no_of_guest"] as! String,
                                                "description":dictData["description"] as! String,
                                                "facilities":dictData["facilities"] as! String,
                                                "price":dictData["price"] as! String,
                                                "terms":dictData["terms"] as! String,
                                                "currency":dictData["currency"] as! String,
                                                "address":dictData["address"] as! String,
                                                "lat":dictData["lat"] as! String,
                                                "lng":dictData["lng"] as! String,
                                                "city":dictData["city"] as! String,
                                                "state":dictData["state"] as! String,
                                                "zip_code":dictData["zip_code"] as! String,
                                                "country":dictData["country"] as! String,
                                                "start": dictData["start"] as! String,
                                                "end": dictData["end"] as! String]
        if isUpdating {
            if let images = dictData["remove_images"] as? String {
                params["remove_images"] = images
            }
            params["id"] = dictData["id"] as? String ?? ""
        }
        let url = !isUpdating ? AppConstant.API.addParty : AppConstant.API.editParty
        AppSingleton.shared.startLoading()
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            let arrImg = self.dictData["images"] as! Array<UIImage>
            for i in 0..<arrImg.count {
                multipartFormData.append((arrImg[i].jpegData(compressionQuality: 0.5))!, withName: "images", fileName: "Profile\(i).jpeg", mimeType: "image/jpeg")
            }
            
            for (key, value) in params {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: url)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                })
                
                upload.responseJSON { response in
                    //print response.result
                    print(response)
                    AppSingleton.shared.stopLoading()
                    if let value = response.result.value {
                        let responseObject = JSON(value).dictionaryValue
                        if responseObject["status"]?.intValue == 0 {
                            AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                        } else if responseObject["status"]?.intValue == 3 {
                            AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                        } else {
                            let msg = self.isUpdating ? "Your Party Updated Successfully" : nil
                            let obj = PartyAddedVC.getInstance(message: msg )
                            self.navigationController?.pushViewController(obj, animated: true)
                        }
                    }
                }
                
            case .failure( _):
                break
                //print encodingError.description
            }
        }
    }
}

extension PartyDescVC: UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        dictData["description"] = textView.text
    }
}
