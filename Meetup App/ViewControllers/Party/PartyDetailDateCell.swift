//
//  PartyDetailDateCell.swift
//  Meetup App
//
//  Created by Birju Bhatt on 24/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class PartyDetailDateCell: UITableViewCell {

    @IBOutlet weak var m_viewPartyStatus: UIView!
    @IBOutlet weak var m_btnJoin: DefaultButton!
    @IBOutlet weak var m_lblAmount: UILabel!
    @IBOutlet weak var m_viewContain: UIView!
    @IBOutlet weak var m_lblTime: UILabel!
    @IBOutlet weak var m_lblDate: UILabel!
    @IBOutlet weak var m_lblAvailable: UILabel!
    @IBOutlet weak var m_lblPartyName: UILabel!
    @IBOutlet weak var m_btnTheme: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        // Call basic set up
        //developer2
        //DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            //self.m_btnJoin.addGradient()
       // }
        m_viewContain.addCornerRadius(radius: 8.0)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
