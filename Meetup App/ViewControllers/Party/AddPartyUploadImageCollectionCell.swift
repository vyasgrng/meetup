//
//  AddPartyUploadImageCollectionCell.swift
//  Meetup App
//
//  Created by Birju Bhatt on 24/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class AddPartyUploadImageCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var m_imgUpload: UIImageView!
    @IBOutlet weak var m_btnRemove: UIButton!
}
