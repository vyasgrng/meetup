//
//  PartyPhotosVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 20/05/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class PartyPhotosVC: BaseVC {

    @IBOutlet weak var m_viewContain: UIView!
    @IBOutlet weak var m_btnNext: DefaultButton!
    @IBOutlet weak var m_collectionView: UICollectionView!
    @IBOutlet weak var m_imgUpload: UIImageView!
    @IBOutlet weak var m_constTop: NSLayoutConstraint!
    var dictData:Dictionary<String,Any> = ["":""]
    var removedImages: [String] = []
    var arrImages:Array<Any> = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
    }
    
    //MARK:- Set initial UI
    func basicSetUp() {
        addBackButton(isFirstHierarchy: false)
        addTopTitle(title: "Add Party")
        m_constTop.constant = topView.frame.height
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(openImagePicker))
        m_viewContain.addGestureRecognizer(tapGesture)
        m_viewContain.addShadowDefault(isOnlyRightSide: true)
        
        if (self.dictData["isEditing"] as? String) == "yes" {
            self.arrImages = (self.dictData["images"] as? String)?.split(separator: ",") ?? []
            self.m_collectionView.reloadData()
        }
    }
    
    @objc func openImagePicker() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func camera()
    {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.allowsEditing = true
        myPickerController.sourceType = UIImagePickerController.SourceType.camera
        self.present(myPickerController, animated: true, completion: nil)
    }

    func photoLibrary()
    {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.allowsEditing = true
        myPickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    func dataValidation() -> Bool {
        if arrImages.count == 0 {
            AppSingleton.shared.showErrorAlert(msg: "Please upload atleast one photo.")
            return false
        }
        return true
    }
    
    //MARK:- Action next
    @IBAction func actionNext(_ sender: UIButton) {
        if dataValidation() {
            dictData["images"] = arrImages.filter({$0 is UIImage})
            let partyDescObj = self.storyboard?.instantiateViewController(withIdentifier: "PartyDescVC") as? PartyDescVC
            if !removedImages.isEmpty {
                dictData["remove_images"] = self.removedImages.joined(separator: ",")
            }
            partyDescObj?.dictData = dictData
            self.navigationController?.pushViewController(partyDescObj!, animated: true)
        }
    }
    
}

extension PartyPhotosVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImages.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddPartyUploadImageCollectionCell", for: indexPath) as! AddPartyUploadImageCollectionCell
        if let image = arrImages[indexPath.item] as? UIImage {
            cell.m_imgUpload.image = image
        } else if let url = arrImages[indexPath.item] as? NSString {
            cell.m_imgUpload.kf.indicatorType = .activity
            cell.m_imgUpload.kf.setImage(with: URL(string: url as String))
        }
        
        cell.m_imgUpload.addCornerRadius(radius: 8.0)
        cell.m_btnRemove.addTapGestureRecognizer(action: {
            if let url = self.arrImages[indexPath.item] as? NSString {
                self.removedImages.append(url as String)
            }
            self.arrImages.remove(at: indexPath.item)
            collectionView.reloadData()
        })
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
        let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
        let size:CGFloat = (collectionView.frame.size.width - space) / 2.0
        return CGSize(width: size, height: size)
    }
}

extension PartyPhotosVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            arrImages.append(pickedImage)
            m_collectionView.reloadData()
        }
        self.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}
