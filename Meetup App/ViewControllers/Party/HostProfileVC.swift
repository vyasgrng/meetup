//
//  HostProfileVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 25/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class HostProfileVC: BaseVC {

    var imagesName = ["","ic_mobile","bday"]
    var hostId = ""
    var dictHostData:Dictionary<String,JSON>?
    
    @IBOutlet weak var m_constTop: NSLayoutConstraint!
    @IBOutlet weak var m_tblView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
    }
    

    //MARK:- Set initial UI
    func basicSetUp() {
        addBackButton()
        addTopTitle(title: "Profile")
        m_constTop.constant = topView.frame.height + 15
        getHostDetail()
    }
    
    func getHostDetail() {
        AppSingleton.shared.startLoading()
        let params = ["token":UserDefaults.standard.currentToken()!,"id":hostId]
        Alamofire.request(AppConstant.API.hostDetail, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        self.dictHostData = responseObject["data"]!["host"].dictionaryValue
                        self.m_tblView.dataSource = self
                        self.m_tblView.delegate = self
                        self.m_tblView.reloadData()
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
//    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
//        targetContentOffset.pointee = scrollView.contentOffset
//        let indexOfMajorCell = self.indexOfMajorCell()
//    }
//    
//    private func indexOfMajorCell() -> Int {
//        
//        let itemWidth = collectionViewLayout.itemSize.width
//        
//        let proportionalOffset = collectionViewLayout.collectionView!.contentOffset.x / itemWidth
//        
//        let index = Int(round(proportionalOffset))let safeIndex = max(0, min(dataSource.count - 1, index))
//        
//        return safeIndex
//        
//        let proportionalOffset = collectionViewLayout.collectionView!.contentOffset.x / itemWidth
//        
//        let index = Int(round(proportionalOffset))
//        
//        let safeIndex = max(0, min(dataSource.count - 1, index))
//        
//        return safeIndex
//        
//    }
}

extension HostProfileVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (dictHostData!["reviews"]?.arrayValue.count)! + 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShowProUserCell") as? ShowProUserCell
            cell?.m_btnUserImage.kf.setImage(with: dictHostData!["profile_pic"]?.url, for: .normal)
            cell?.m_btnUserImage.addCornerRadius(radius: 5)
            cell?.m_lblName.text = (dictHostData!["first_name"]?.stringValue)! + " " + (dictHostData!["last_name"]?.stringValue)!
            cell?.m_lblName.text = dictHostData!["email"]?.stringValue
            return cell!
        case 1,2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShowProfUserInfoCell") as? ShowProfUserInfoCell
            cell?.m_btnImg.setImage(UIImage(named: imagesName[indexPath.row]), for: .normal)
            if indexPath.row == 1 {
                cell!.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: UIScreen.main.bounds.width)
                cell?.m_lblValue.text = dictHostData!["mobile_no"]?.stringValue
            } else {
                cell?.m_lblValue.text = dictHostData!["dob"]?.stringValue
            }
            return cell!
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HostEventCell") as? HostEventCell
            cell!.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: UIScreen.main.bounds.width)
            cell?.m_collectionView.delegate = self
            cell?.m_collectionView.dataSource = self
            cell?.m_collectionView.reloadData()
            return cell!
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HostProfileReviewCaptionCell") as? HostProfileReviewCaptionCell
            cell?.m_lblReviewsCount.text = "Reviews (\((dictHostData!["reviews"]?.arrayValue.count) ?? 0))"
            cell!.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: UIScreen.main.bounds.width)
            return cell!
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HostProfileReviewCell") as? HostProfileReviewCell
            cell!.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: UIScreen.main.bounds.width)
            cell?.m_viewReviews.addCornerRadius(radius: 10)
            cell!.m_lblReview.text = dictHostData!["reviews"]?.arrayValue[indexPath.row - 5]["message"].stringValue
            return cell!
        }
    }
}

extension HostProfileVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (dictHostData!["events"]?.arrayValue.count)!
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HostEventCollectionCell", for: indexPath) as? HostEventCollectionCell
        let data = dictHostData!["events"]?.arrayValue[indexPath.item]
        cell?.m_btnTheme.addCornerRadius(radius: 8.0)
        cell?.m_viewDate.addCornerRadiusParticularSide(rectCorner: [.topLeft,.bottomLeft], radius: ((cell?.m_viewDate.frame.height)!/2))
        cell?.m_lblDate.text = data!["date"].stringValue
        cell?.m_lblName.text = data!["name"].stringValue
        cell?.m_btnTheme.setTitle(data!["theme"].stringValue, for: .normal)
        cell?.m_lblPrice.text = "$ \(data!["price"].stringValue)"
        cell?.m_imgView.kf.setImage(with: data!["image"].url)
        cell?.m_imgView.addCornerRadius(radius: 12.0)
        cell?.m_imgView.clipsToBounds = true
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width / 1.2, height: collectionView.frame.size.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 30)
    }
}
