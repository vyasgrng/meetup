//
//  PartyAddressVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 21/05/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import GooglePlaces
import Alamofire
import SwiftyJSON

struct addPartyAddress {
    var placeHolders:String?
    var key:String?
    var kbType:UIKeyboardType?
    var isDropDown:Bool?
}

class PartyAddressVC: BaseVC {

    @IBOutlet weak var m_btnNext: DefaultButton!
    @IBOutlet weak var m_tblView: UITableView!
    @IBOutlet weak var m_constTop: NSLayoutConstraint!
    
    var structAddPartyAddress:[addPartyAddress]?
    var dictData:Dictionary<String,String> = ["":""]


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //MARK:- Set initial UI
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
        
        
    }
    
    //MARK:- Set initial UI
    func basicSetUp() {
        addBackButton(isFirstHierarchy: false)
        addTopTitle(title: "Add Party")
        m_constTop.constant = topView.frame.height + 15
        
        structAddPartyAddress = [
            addPartyAddress(placeHolders: "Address",key:"address",kbType:.default,isDropDown:true),
            addPartyAddress(placeHolders: "City",key:"city",kbType:.default,isDropDown:true),
            addPartyAddress(placeHolders: "State",key:"state",kbType:.default,isDropDown:true),
            addPartyAddress(placeHolders: "ZipCode",key:"zip_code",kbType:.default,isDropDown:true),
            addPartyAddress(placeHolders: "Country",key:"country",kbType:.default,isDropDown:true),
        ]
        
        if self.dictData["isEditing"] != "yes" {
            dictData["address"] = ""
            dictData["city"] = ""
            dictData["state"] = ""
            dictData["zip_code"] = ""
            dictData["country"] = ""
        }
        
        m_tblView.delegate = self
        m_tblView.dataSource = self

    }
    
    //MARK:- Data validation
    func dataValidation() -> Bool {
        self.view.endEditing(true)
        if (dictData["address"]?.isEmpty)! {
            AppSingleton.shared.showErrorAlert(msg: "Address Required.")
            return false
        } else if (dictData["city"]?.isEmpty)! {
            AppSingleton.shared.showErrorAlert(msg: "City Required.")
            return false
        } else if (dictData["state"]?.isEmpty)! {
            AppSingleton.shared.showErrorAlert(msg: "State Required.")
            return false
        } else if (dictData["zip_code"]?.isEmpty)! {
            AppSingleton.shared.showErrorAlert(msg: "ZipCode Required.")
            return false
        } else if (dictData["country"]?.isEmpty)! {
            AppSingleton.shared.showErrorAlert(msg: "Country Required.")
            return false
        }
        return true
    }
    
    //MARK:- Action Next
    @IBAction func actionNext(_ sender: UIButton) {
        if dataValidation() {
            let partyImgObj = self.storyboard?.instantiateViewController(withIdentifier: "PartyPhotosVC") as? PartyPhotosVC
            partyImgObj?.dictData = dictData
            self.navigationController?.pushViewController(partyImgObj!, animated: true)
        }
    }
    
    func openGooglePlacePicker() {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.formattedAddress.rawValue) | UInt(GMSPlaceField.name.rawValue) | UInt(GMSPlaceField.coordinate.rawValue) |
            UInt(GMSPlaceField.placeID.rawValue))!
        autocompleteController.placeFields = fields
        
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .geocode
        autocompleteController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)
        
    }
}

extension PartyAddressVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return structAddPartyAddress!.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddPartyDetailsCell") as? AddPartyDetailsCell
//        cell?.m_viewContain.addShadowDefault(isOnlyRightSide: true)
        cell?.m_viewContain.addShadow(offset: .zero, color: .lightGray, opacity: 0.6, radius: 2)
        cell?.m_txtFld.placeholder = structAddPartyAddress![indexPath.row].placeHolders
        cell?.m_txtFld.tag = indexPath.row
        cell?.m_txtFld.delegate = self
        cell?.m_txtFld.keyboardType = structAddPartyAddress![indexPath.row].kbType!
        cell?.m_txtFld.text = dictData[structAddPartyAddress![indexPath.row].key!]
        return cell!
        
    }
}

extension PartyAddressVC: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        dictData[structAddPartyAddress![textField.tag].key!] = textField.text
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 0 {
            self.openGooglePlacePicker()
            return false
        }
        return true
    }
}

extension PartyAddressVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        dismiss(animated: true, completion: nil)
        
        // 1st way
        
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            
            // Address dictionary
//            print(placeMark.addressDictionary as Any)
//            print("Place address \(String(describing: place.formattedAddress))")
//            print("Place attributions \(String(describing: place.attributions))")
            
            
            self.dictData["address"] = placeMark.name ?? ""
            self.dictData["city"] = placeMark.locality ?? ""
            self.dictData["state"] = placeMark.administrativeArea ?? ""
            self.dictData["zip_code"] = placeMark.postalCode ?? ""
            self.dictData["country"] = placeMark.country ?? ""
            self.dictData["lat"] = "\(place.coordinate.latitude)"
            self.dictData["lng"] = "\(place.coordinate.latitude)"
            self.m_tblView.reloadData()
        })
        
        
        // 2nd way
        
        //                AppSingleton.shared.startLoading()
        //                let params = ["place_id":place.placeID!,"key":AppConstant.Google.mapKey]
        //
        //                Alamofire.request("https://maps.googleapis.com/maps/api/place/details/json", method: .get, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
        //                    switch response.result {
        //                    case .success:
        //                        if let value = response.result.value {
        //                            AppSingleton.shared.stopLoading()
        //                            let responseObject = JSON(value).dictionaryValue
        //                            if responseObject["status"]?.stringValue == "OK"  {
        //                                let arrAddress = responseObject["result"]!["address_components"].arrayValue
        //                                for i in arrAddress  {
        //                                    print(i)
        //                                }
        //                            }
        //                        }
        //                    case .failure(let error):
        //                        print(error)
        //                    }
        //                }
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
