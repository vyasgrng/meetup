//
//  PartyDetailVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 24/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON

enum EventStatus:Int {
    case requested = 0
    case approved = 1
    case paid = 2
    case owner = 3
    case nostatus = 4
}


class PartyDetailVC: BaseVC {
    
    var partyId = ""
    var dictPartyDetail:Dictionary<String,JSON> = [:]
    let pageViewPartyDetails = UIPageControl.init(frame: .zero)
    var pageMenu : CAPSPageMenu?

    @IBOutlet  var m_txtJoiningRequestMessage: UITextView!
    @IBOutlet  var m_viewJoinPartyTopConstraint: NSLayoutConstraint!
    @IBOutlet  var m_viewJoinPartyRequestMessage: UIView!
    @IBOutlet  var m_constTop: NSLayoutConstraint!
    @IBOutlet  var m_tblView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
        // Call basic set up
        
    }
    
    //developer2
    
    @IBAction func btnCancel_requestMesageJoinParty(_ sender: UIButton) {
        
        m_viewJoinPartyTopConstraint.constant = 1600
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func btnShare_requestMessageJoinParty(_ sender: UIButton) {
        m_txtJoiningRequestMessage.resignFirstResponder()
        AppSingleton.shared.startLoading()
        //developer2
        let params = ["token":UserDefaults.standard.currentToken()!,"id":partyId,"message" : m_txtJoiningRequestMessage.text! ]
        //"I would like to join"
        Alamofire.request(AppConstant.API.eventJoin, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    
                    self.m_viewJoinPartyTopConstraint.constant = 1600
                    UIView.animate(withDuration: 0.4) {
                        self.view.layoutIfNeeded()
                    }
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        self.m_txtJoiningRequestMessage.text = ""
                        self.dictPartyDetail["event_status"]?.int = 0
                        self.m_tblView.reloadData()
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    //MARK:- Set initial UI
    func basicSetUp() {
        addBackButton(isFirstHierarchy: false)
        addTopTitle(title: "Party Details")
        m_constTop.constant = topView.frame.height
        partyDetailAPI()
    }
    
    //MARK:- Party detail api call
    func partyDetailAPI() {
        AppSingleton.shared.startLoading()
        let params = ["token":UserDefaults.standard.currentToken()!,"id":partyId]
        Alamofire.request(AppConstant.API.partyDetail, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        self.dictPartyDetail = (responseObject["data"]?.dictionaryValue)!
                        self.m_tblView.delegate = self
                        self.m_tblView.dataSource = self
                        self.m_tblView.reloadData()
                        
                        //developer2
                        self.topTitleLabel.text = self.dictPartyDetail["name"]!.stringValue
                        self.setupPageView(numberOfPage: self.dictPartyDetail["images"]?.arrayValue.count ?? 0)
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    //MARK:- Setup page menu
    func setUpPageMenu(parentView:UIView) {
                
        var controllerArray : [UIViewController] = []
        
        let infoVC = self.storyboard?.instantiateViewController(withIdentifier: "PartyDetailInfoVC") as? PartyDetailInfoVC
        infoVC?.openViewController = { vc in
            self.navigationController?.pushViewController(vc, animated: true)
        }
        infoVC?.title = "INFO"
        infoVC?.dictPartyInfo = dictPartyDetail
        
        let updateVC = self.storyboard?.instantiateViewController(withIdentifier: "PartyDetailUpdateVC") as? PartyDetailUpdateVC
        updateVC?.title = "UPDATES"
        updateVC?.dictPartyUpdates = dictPartyDetail


        let photosVC = self.storyboard?.instantiateViewController(withIdentifier: "PartyDetailPhotosVC") as? PartyDetailPhotosVC
        photosVC?.title = "PHOTOS"
        photosVC?.dictPartyPhotos = dictPartyDetail
        
        controllerArray.append(infoVC!)
        controllerArray.append(updateVC!)
        controllerArray.append(photosVC!)
        
        let parameters: [CAPSPageMenuOption] = [
            .menuItemSeparatorWidth(0),
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorPercentageHeight(0.1),
            .scrollMenuBackgroundColor(UIColor().convertHexStringToColor(hexString: "F9F9F9")),
            .bottomMenuHairlineColor(UIColor().appThemeBlueColor()),
            .viewBackgroundColor(UIColor().convertHexStringToColor(hexString: "F9F9F9")),
            .selectionIndicatorColor(UIColor().appThemeBlueColor()),
            .menuItemFont(UIFont().RobotoMedium(size: 15.0)),
            .menuHeight(50.0),
            .selectedMenuItemLabelColor(UIColor().appThemeBlueColor()),
            .unselectedMenuItemLabelColor(UIColor().appThemeBlueColor().withAlphaComponent(0.5))
        ]
        
        // Initialize page menu with controller array, frame, and optional parameters
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: parameters)

        // Lastly add page menu as subview of base view controller view
        // or use pageMenu controller in you view hierachy as desired
        parentView.addSubview(pageMenu!.view)
        pageMenu?.view.snp.makeConstraints({ (make) in
            make.leading.top.equalToSuperview()
            make.bottom.trailing.equalToSuperview()
            make.height.equalTo(UIScreen.main.bounds.height)
        })
    }
    
    //MARK: setup Pageview control for party details photos
    func setupPageView(numberOfPage : Int){
        //if numberOfPage > 1{
        //add pageview control
        pageViewPartyDetails.currentPageIndicatorTintColor = UIColor().convertHexStringToColor(hexString: "461D8F")
        pageViewPartyDetails.pageIndicatorTintColor = .white
        pageViewPartyDetails.currentPage = 0
        pageViewPartyDetails.numberOfPages = numberOfPage
        //}
    }
    
    //MARK:- Join Party Action
    @objc func joinParty(_ sender:UIButton) {
        if sender.titleLabel?.text == "PAY"{
            let nib : NSArray = Bundle.main.loadNibNamed("PaymentSelectionVC", owner: self, options: nil)! as NSArray
            let myVC = nib.object(at: 0) as! PaymentSelectionVC
            myVC.delegate = self
            myVC.setInfo(eventId: partyId)
            myVC.addViewWithAnimation(onView:self.view)
            return
        }else if sender.titleLabel?.text == "JOIN"{
            //show msg popup to join party
            m_viewJoinPartyTopConstraint.constant = 0
            self.view.bringSubviewToFront(m_viewJoinPartyRequestMessage)
            UIView.animate(withDuration: 0.4) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    //MARK:- Favourite clicked
    @objc func actionFavourite(_ sender:UIButton) {
        
        var action = "add"
        if sender.isSelected {
            action = "remove"
        }
        sender.isSelected = !sender.isSelected
        AppSingleton.shared.startLoading()
        let params = ["token":UserDefaults.standard.currentToken()!,"id":partyId,"action":action]
        Alamofire.request(AppConstant.API.eventFavourite, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        var imgName = ""
                        sender.isSelected ? (imgName = "ic_favourite") : (imgName = "ic_favourite_lined")
                        sender.setImage(UIImage(named: imgName), for: .normal)
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}
extension PartyDetailVC: UITableViewDelegate, UITableViewDataSource {
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PartyImageTblCell") as! PartyImageTblCell
            cell.m_collectionPartyImages.delegate = self
            cell.m_collectionPartyImages.dataSource = self
            cell.m_collectionPartyImages.reloadData()
            return cell
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PartyDetailLikeReviewCell") as? PartyDetailLikeReviewCell
            cell?.m_btnFav.addTarget(self, action: #selector(actionFavourite(_:)), for: .touchUpInside)
            cell?.m_lblReviewCount.text = dictPartyDetail["host"]!["reviews_count"].stringValue
            if self.dictPartyDetail["hitlisted"]?.boolValue == true {
                cell?.m_btnFav.setImage(#imageLiteral(resourceName: "ic_favourite"), for: .normal)
                cell?.m_btnFav.isSelected = true
            } else {
                cell?.m_btnFav.setImage(#imageLiteral(resourceName: "ic_favourite_lined"), for: .normal)
                cell?.m_btnFav.isSelected = false
            }
            
            switch dictPartyDetail["event_status"]?.int {

            case EventStatus.requested.rawValue:
                cell?.m_btnStatus.isHidden = false
                cell?.m_btnStatus.setTitle("REQUESTED", for: .normal)
                break
            case EventStatus.approved.rawValue:
                //cell?.m_btnJoin.isHidden = true
                //developer2
                cell?.m_btnStatus.isHidden = false
                cell?.m_btnStatus.setTitle("PAY", for: .normal)
                cell?.m_btnStatus.addTarget(self, action: #selector(joinParty(_:)), for: .touchUpInside)
                break
            case EventStatus.paid.rawValue:
                cell?.m_btnStatus.isHidden = true
                break
            case EventStatus.owner.rawValue:
                cell?.m_btnStatus.isHidden = true
                break
            case EventStatus.nostatus.rawValue:
                cell?.m_btnStatus.isHidden = false
                cell?.m_btnStatus.setTitle("JOIN", for: .normal)
                cell?.m_btnStatus.addTarget(self, action: #selector(joinParty(_:)), for: .touchUpInside)
                break
            default:
                break
            }
            
            return cell!
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PageMenuCell", for: indexPath) as? PageMenuCell
            self.setUpPageMenu(parentView: cell!.contentView)
            return cell!
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PartyDetailDateCell") as? PartyDetailDateCell
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("Scroll")
    }
    
}

extension PartyDetailVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dictPartyDetail["images"]?.arrayValue.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PartyImageCollectionCell", for: indexPath) as? PartyImageCollectionCell
//        cell?.m_imgViewParty.image = #imageLiteral(resourceName: "holiday-party-entertainment-top-notch-talent")
        cell?.m_imgViewParty.kf.setImage(with: URL(string: ((dictPartyDetail["images"]?.arrayValue[indexPath.row].stringValue)!)), placeholder: UIImage(named: "image01"), options: [], progressBlock: nil, completionHandler: { (result) in
        })
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
}

//MARK: Delegate from payment selection
extension PartyDetailVC : ProtocolPaymentSelectionVC{
    func navigateToChangeDefualtCard() {
        let vcObj = UIStoryboard(name: "InnerProfileSection", bundle: Bundle.main).instantiateViewController(withIdentifier: "PaymentListVC") as? PaymentListVC
        self.navigationController?.pushViewController(vcObj!, animated: true)
    }
}
