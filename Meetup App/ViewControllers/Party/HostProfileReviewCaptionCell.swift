//
//  HostProfileReviewCaptionCell.swift
//  Meetup App
//
//  Created by Birju Bhatt on 25/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class HostProfileReviewCaptionCell: UITableViewCell {

    @IBOutlet weak var m_btnViewAll: UIButton!
    @IBOutlet weak var m_lblReviewsCount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
