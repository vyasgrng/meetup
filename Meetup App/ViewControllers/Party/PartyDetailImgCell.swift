//
//  PartyDetailImgCell.swift
//  Meetup App
//
//  Created by Birju Bhatt on 24/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class PartyDetailImgCell: UITableViewCell {

    @IBOutlet weak var m_collectionUploadImages: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


class PartyDetailImgsCell: UICollectionViewCell {
    var imgPartyDetails = UIImageView()
    
    func setupImage(urlPartyImg: URL?){
        imgPartyDetails.frame = self.bounds
        imgPartyDetails.contentMode = .scaleAspectFill
        self.addSubview(imgPartyDetails)
        if urlPartyImg != nil{
            self.imgPartyDetails.kf.setImage(with: urlPartyImg)
        }
    }
}
