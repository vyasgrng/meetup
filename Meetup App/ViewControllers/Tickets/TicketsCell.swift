//
//  TicketsCell.swift
//  Meetup App
//
//  Created by STL on 25/12/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class TicketsCell: UITableViewCell {

    @IBOutlet weak var m_wraperView: UIView!
    @IBOutlet weak var m_imgView: UIImageView!
    @IBOutlet weak var m_viewDate: UIView!
    @IBOutlet weak var m_lblDate: UILabel!
    @IBOutlet weak var m_lblName: UILabel!
    @IBOutlet weak var m_lblPrice: UILabel!
    @IBOutlet weak var m_btnJoin: UIButton!
    @IBOutlet weak var m_lblTicketNo: UILabel!
    @IBOutlet weak var m_imgViewHost: UIImageView!
    @IBOutlet weak var m_btnMore: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        m_imgViewHost.clipsToBounds = true
        m_imgViewHost.layer.cornerRadius = m_imgViewHost.frame.height / 2
        m_lblName.font = UIFont().RobotoMedium(size: 20.0)
        m_lblDate.font = UIFont().RobotoMedium(size: 16.0)
        m_lblPrice.font = UIFont().RobotoMedium(size: 18.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
