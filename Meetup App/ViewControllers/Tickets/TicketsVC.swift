//
//  TicketsVC.swift
//  Meetup App
//
//  Created by STL on 25/12/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class TicketsVC: BaseVC {

    var arrayTicketsList:Array<JSON> = []
    
    @IBOutlet weak var m_tblViewTicket: UITableView!
    @IBOutlet weak var m_constTop: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
    }
    
    //MARK:- Set initial UI
    func basicSetUp() {
        
        m_constTop.constant = topView.frame.height + 10
        addBackButton(withImage: #imageLiteral(resourceName: "ic_back").maskWithColor(color: .white)!)
        addTopTitle(title: "Tickets")
        
        //Get enjoted list
        ticketsListAPI()
        self.m_tblViewTicket.delegate = self
        self.m_tblViewTicket.dataSource = self
    }
    
    @objc func actionMore(_ sender:UIButton) {
        if let data = arrayTicketsList[sender.tag].dictionary,
            let id = data["id"]?.string {
            let view = ShareEditDelView.getInstance(partyId: id, owner: self, refresh: {
                self.ticketsListAPI()
            })
            view.isUpdatable = data["updatable"]?.bool ?? false
            view.show()
        }
    }
    
    //MARK:- Get enjoyed list
    func ticketsListAPI(isFilter:Bool = false) {
        AppSingleton.shared.startLoading()
        let  params = ["token":UserDefaults.standard.currentToken()!,"page":"0"]
                
        Alamofire.request(AppConstant.API.tickets, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        self.arrayTicketsList = responseObject["data"]!["events"].arrayValue
                        self.m_tblViewTicket.isHidden = false

                        if self.arrayTicketsList.count == 0 {
                            self.m_tblViewTicket.isHidden = true
                        }
                        self.m_tblViewTicket.reloadData()
                    }
                } else {
                    AppSingleton.shared.stopLoading()
                }
            case .failure(let error):
                print(error)
                AppSingleton.shared.stopLoading()
            }
        }
    }
}

extension TicketsVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayTicketsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data = arrayTicketsList[indexPath.row].dictionaryValue
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TicketsCell", for: indexPath) as? TicketsCell
        cell?.m_viewDate.addCornerRadiusParticularSide(rectCorner: [.topLeft,.bottomLeft], radius: ((cell?.m_viewDate.frame.height)!/2))
        cell?.m_lblPrice.text = "\(data["currency_readable"]?.stringValue ?? "USD") \(data["price"]?.stringValue ?? "0")"
        let partyTime = (data["start"]?.doubleValue.timeStampToDate(format: "hh:mm a"))! + " to " + (data["end"]?.doubleValue.timeStampToDate(format: "hh:mm a"))!
        cell?.m_lblDate.text = (data["date"]?.doubleValue.timeStampToDate(format: "dd MMM yyyy"))! + " / " + partyTime + " / " + data["city"]!.stringValue
        cell?.m_lblName.text = data["name"]?.stringValue
        cell?.m_btnJoin.addCornerRadius(radius: 3.0)
        cell?.m_imgView.kf.setImage(with: URL(string: (data["image"]?.stringValue)!), placeholder: UIImage(named: "image01"), options: [], progressBlock: nil, completionHandler: { (result) in
            cell?.m_imgView.addCornerRadius(radius: 10.0)
        })
        cell?.m_imgViewHost.kf.setImage(with: URL(string: (data["host_image"]?.stringValue) ?? ""), placeholder: #imageLiteral(resourceName: "profile.png"))
        cell?.m_lblTicketNo.text = data["ticket_no"]?.stringValue
        cell?.m_btnMore.tag = indexPath.row
        cell?.m_btnMore.addTarget(self, action: #selector(actionMore(_:)), for: .touchUpInside)
        cell?.m_wraperView.addShadowWithCorner(cornerRadius: 10.0, offset: .zero, color: .lightGray , radius: 4.0, opacity: 0.6)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = arrayTicketsList[indexPath.row].dictionaryValue
        let vcObj = UIStoryboard(name: "Party", bundle:Bundle.main).instantiateViewController(withIdentifier: "PartyDetailVC") as? PartyDetailVC
        vcObj?.partyId = (data["id"]?.stringValue)!
        self.navigationController?.pushViewController(vcObj!, animated: true)
    }
    
}
