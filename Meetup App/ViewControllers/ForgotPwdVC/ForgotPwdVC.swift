//
//  ForgotPwdVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 14/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ForgotPwdVC: UIViewController {

    @IBOutlet weak var m_viewUname: UIView!
    @IBOutlet weak var m_viewBottom: UIView!
    @IBOutlet weak var m_btnSend: UIButton!
    @IBOutlet weak var m_txtFldForgotPwd: UITextField!
    
    @IBOutlet weak var m_imgViewUname: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.popVC()
    }
    
    func basicSetUp() {
        m_viewBottom.addCornerRadiusParticularSide(rectCorner: [.topLeft,.topRight], radius: m_viewBottom.frame.size.width/5)
        m_viewUname.addCornerRadius(radius: 5.0)
        m_viewUname.addBorder(width: 1.0, color: .white)
        m_btnSend.addCornerRadius(radius: 5.0)
        let imgUname = m_imgViewUname.image?.maskWithColor(color: .white)
        m_imgViewUname.image = imgUname

    }
   
    //MARK:- Action forgot password
    @IBAction func actionForgotPwd(_ sender: UIButton) {
        if dataValidation() {
            AppSingleton.shared.startLoading()
            let params = ["email":m_txtFldForgotPwd.text!]
            Alamofire.request(AppConstant.API.forgotPassword, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
                switch response.result {
                case .success:
                    if let value = response.result.value {
                        AppSingleton.shared.stopLoading()
                        let responseObject = JSON(value).dictionaryValue
                        AppSingleton.shared.showSuccessAlert(msg: (responseObject["message"]?.stringValue)!)
                        self.navigationController?.popViewController(animated: true)
                    }
                case .failure(let error):
                    print(error)
                    AppSingleton.shared.stopLoading()
                }
            }
        }
    }
    
    func dataValidation() -> Bool {
        if (m_txtFldForgotPwd.text?.isEmpty)! || m_txtFldForgotPwd.text?.isValidEmail() == false {
            AppSingleton.shared.showErrorAlert(msg: "Please enter valid email address")
            return false
        }
        return true
    }
}
