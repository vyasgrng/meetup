//
//  ScanQRVC.swift
//  Meetup App
//
//  Created by STL on 07/01/20.
//  Copyright © 2020 Birju Bhatt. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire
import SwiftyJSON


class ScanUserListVC: BaseVC {

    @IBOutlet weak var m_tblView: UITableView!
    @IBOutlet weak var m_btnQR: UIButton!
    @IBOutlet weak var m_constTop: NSLayoutConstraint!
    
    var arrayCheckInUsers:Array<JSON> = []
    var qrCode = ""
    var eventId = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
    }

    //MARK:- Set initial UI
    func basicSetUp() {
        m_constTop.constant = topView.frame.height + 15
        addBackButton(isFirstHierarchy: false)
        addTopTitle(title: "QR List")
        m_btnQR.addShadowWithCorner(cornerRadius: m_btnQR.frame.size.width/2, offset: .zero, color: .black, radius: 4, opacity: 0.6)
        checkInListAPI()
        self.m_tblView.delegate = self
        self.m_tblView.dataSource = self
    }
    
    //MARK:- Check in list api
    func checkInListAPI() {
        AppSingleton.shared.startLoading()
        let params = ["token":UserDefaults.standard.currentToken()!,"event_id":eventId]
        Alamofire.request(AppConstant.API.checkInList, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        self.arrayCheckInUsers = (responseObject["data"]?["list"].arrayValue)!
                        self.m_tblView.reloadData()
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    //MARK:- Scan QR of User api
    func scanQRAPI() {
        AppSingleton.shared.startLoading()
        let params = ["token":UserDefaults.standard.currentToken()!,"event_id":eventId,"qr_code":qrCode]
        Alamofire.request(AppConstant.API.scanQRCode, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        AppSingleton.shared.showSuccessAlert(msg: (responseObject["message"]?.stringValue)!)
                        self.checkInListAPI()
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    //MARK:- Scan QR Code
    @IBAction func actionScanQR(_ sender: UIButton) {
        let vcObj = self.storyboard?.instantiateViewController(withIdentifier: "QRCamVC") as? QRCamVC
        vcObj?.delegate = self
        self.present(vcObj!, animated: true, completion: nil)
    }
}

extension ScanUserListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCheckInUsers.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = arrayCheckInUsers[indexPath.row].dictionaryValue
        let cell = tableView.dequeueReusableCell(withIdentifier: "BlockedUsersCell") as? BlockedUsersCell
        cell?.m_lblName.text = data["first_name"]!.stringValue + " " + data["last_name"]!.stringValue
        cell?.m_imgUser.kf.setImage(with: (data["profile_pic"]?.url)!)
        let timeinterval = data["check_in_time"]?.stringValue.dropLast(3)
        let date = Date(timeIntervalSince1970: (timeinterval! as NSString).doubleValue)
        cell?.m_lblTime.text = date.timeAgoSinceDate()
        return cell!
    }
}

extension ScanUserListVC: QRCamDelegate {
    func scannedQRCode(qr_Code: String) {
        qrCode = qr_Code
        scanQRAPI()
        
    }
}
