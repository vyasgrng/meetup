//
//  ChattingVC.swift
//  Meetup App
//
//  Created by Aditi Pancholi on 26/07/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import UITextView_Placeholder
import Alamofire
import SwiftyJSON
import DTPhotoViewerController
import SDWebImage

class ChattingVC: BaseVC {
    
    @IBOutlet weak var m_progressViewImageUpload: UIProgressView!
    @IBOutlet weak var m_constTop: NSLayoutConstraint!
    @IBOutlet weak var m_btnSendMessage: UIButton!
    @IBOutlet weak var m_btnAttachImage: UIButton!
    @IBOutlet weak var m_txtMessage: EmojiTextView!
    @IBOutlet weak var m_tblView: UITableView!
    
    var chatId : String = ""
    var arrChats:Array<JSON> = []
    var userName = ""
    var arrChatImgData:Array<Any> = []
    var isEmojiActive : Bool = false
    var senderId = ""
    var chatImage = UIImage()
    var pageCount = 0
    private var lastContentOffset: CGFloat = 0
    var manuallyScrollUp : Bool = false
    var fetchingInProgress : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getMessages()
        setupTxtViewMessage()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.setupUI()
        }
        m_progressViewImageUpload.isHidden = true
        observeIfAnyMessage()
        observeUserStatus()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnSendMessageClick(_ sender: UIButton) {
        
        m_txtMessage.isEmojiMode = false
        //chat message string
        let data = (m_txtMessage.text ?? "").data(using: .nonLossyASCII)
        let messageWithEmojisEncoded = String.init(data: data!, encoding: .utf8)!
        
        let dict:[String:String] = ["chat_id":chatId,"type":"text","content":messageWithEmojisEncoded,"thumbnail":""]
        let jsonStr = AppSingleton.shared.dictToJSONString(dict: dict)
        SocketIOManager.sharedInstance.manager.defaultSocket.emitWithAck("send_message",with: [jsonStr]).timingOut(after: 0) { data in
            print(data)
            let response = JSON(data)
            let newMessage = response[0]["data"]
            self.arrChats.append(newMessage)
            
            self.m_tblView.beginUpdates()
            self.m_tblView.insertRows(at: [IndexPath(row: self.arrChats.count-1, section: 0)], with: .automatic)
            self.m_tblView.endUpdates()
            
            //clear textview and hide keyboard
           // self.m_txtMessage.resignFirstResponder()
            self.m_txtMessage.text = ""
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
                let indexPath = IndexPath(row: self.arrChats.count-1, section: 0)
                self.m_tblView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
    }
    
    
    @IBAction func btnEmojiClick(_ sender: UIButton) {
        isEmojiActive = true
        m_txtMessage.isEmojiMode = true
        m_txtMessage.becomeFirstResponder()
        //self.m_txtMessage.textInputMode?.primaryLanguage = "emoji"
    }
    
    
    @IBAction func btnSendImageClick(_ sender: UIButton) {
        showActionSheetForGallery()
    }
    
    
    //MARK: Basic ui
    func setupUI(){
        addBackButton()
        addRightButton(withText: "", withImageName: "ic_dots",withImgColor:.white, vc: self, target: #selector(moreOptions(_:)))
        self.setupHeader(title: "User name", userImg: UIImage.init(named: "profile img")!)

        m_constTop.constant = topView.frame.height
        userImage.layer.cornerRadius = 20
        userImage.layer.masksToBounds = true
        userImage.clipsToBounds = true

    }

    //MARK: Configure txtmessage
    func setupTxtViewMessage(){
        m_txtMessage.translatesAutoresizingMaskIntoConstraints = false
//        m_txtMessage.placeholder = "Write message"
//        m_txtMessage.placeholderColor = .gray
        m_txtMessage.layer.cornerRadius = 10
        m_txtMessage.layer.borderColor = UIColor.gray.cgColor
        m_txtMessage.layer.borderWidth = 1
        m_txtMessage.textColor = .black
        m_txtMessage.isScrollEnabled = false
    }
    
}


//MARK: Nevigation actions
extension ChattingVC{
    //MARK: Navigation options
    @objc func moreOptions(_ sender:UIButton){
        showActionSheet(viewRect: sender)
    }
    
    func showActionSheet(viewRect:UIButton) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Review User", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.reviewUser()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Block", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.blockUser()
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        if let presenter = actionSheet.popoverPresentationController {
            presenter.sourceView = viewRect;
            presenter.sourceRect = viewRect.bounds;
        }
        //self.present(actionSheet, animated: true, completion: nil)
        actionSheet.show(self, sender: nil)
    }
    
    func reviewUser(){
        
    }
    
    func blockUser(){
        
    }
}

//MARK: Server communication
extension ChattingVC {
    func getMessages() {
        let dict:[String:String] = ["id":chatId,"page":"\(pageCount)"]
        let jsonStr = AppSingleton.shared.dictToJSONString(dict: dict)
        SocketIOManager.sharedInstance.manager.defaultSocket.emitWithAck("get_messages",with: [jsonStr]).timingOut(after: 0) { data in
            let response = JSON(data)
            self.arrChats = response[0]["data"]["messages"].arrayValue.sorted(by: { (msg1, msg2) -> Bool in
                return msg1["timestamp"].stringValue < msg2["timestamp"].stringValue
            })
                //updatde chat
            self.userName = response[0]["data"]["user"]["first_name"].stringValue + " " + response[0]["data"]["user"]["last_name"].stringValue
            self.topTitleLabel.text = self.userName
            
            //set last seen
        
            self.subTitleLabel.text = response[0]["data"]["user"]["last_seen"].doubleValue == -1.0 ? "Online" :  response[0]["data"]["user"]["last_seen"].doubleValue.timeStampToDate(format: "h:mm a 'on' MMMM dd, yyyy")
            
            self.senderId = response[0]["data"]["user"]["id"].stringValue
            
            //set profile photo of sender
            self.userImage.kf.setImage(with: response[0]["data"]["user"]["profile_pic"].url)
            self.m_tblView.reloadData()
            
            //after reload scroll to bottom
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.03) {
                let indexPath = IndexPath(row: self.arrChats.count-1, section: 0)
                self.m_tblView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
    }
    
    //MARK: newChatMessage

    func observeIfAnyMessage(){
        SocketIOManager.sharedInstance.manager.defaultSocket.on("new_message") { ( dataArray, ack) -> Void in
            print("New from server \(dataArray)")
            let response = JSON(dataArray)
            if self.chatId == response[0]["chat_id"].stringValue{
                let newMessage = response[0]["message"]
                self.arrChats.append(newMessage)
                self.m_tblView.beginUpdates()
                self.m_tblView.insertRows(at: [IndexPath(row: self.arrChats.count-1, section: 0)], with: .automatic)
                self.m_tblView.endUpdates()
          
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
                    let indexPath = IndexPath(row: self.arrChats.count-1, section: 0)
                    self.m_tblView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                }
            }
        }
    }
        //MARK: online status
    func observeUserStatus(){
        SocketIOManager.sharedInstance.manager.defaultSocket.on("status_update") { (timeUpdate, ack) in
            print("status update from server \(timeUpdate)")
            let opponentStatus = JSON(timeUpdate[0])
            if self.senderId == opponentStatus["id"].stringValue{
                //&& response["status"].intValue == 0
            if opponentStatus["last_seen"].doubleValue != -1 {
                self.subTitleLabel.text = opponentStatus["last_seen"].doubleValue.timeStampToDate(format: "h:mm a 'on' MMMM dd, yyyy")
            }else{
                //if time stamp same then online
                self.subTitleLabel.text = "Online"
            }
            }
        }
    }
}

//MARK: Textview delegates
extension ChattingVC : UITextViewDelegate{
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: self.arrChats.count-1, section: 0)
            self.m_tblView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: self.arrChats.count-1, section: 0)
            self.m_tblView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        isEmojiActive = false
        m_txtMessage.isEmojiMode = false
    }
}

//MARK: Tableview chat delegate and datasource
extension ChattingVC : UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrChats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var messageSentTimeString: String {
            let msgSentDate = Date(timeIntervalSince1970: self.arrChats[indexPath.row]["timestamp"].doubleValue / 1000)
            return msgSentDate.timeAgoSinceDate()
            //self.arrChats[indexPath.row]["timestamp"].doubleValue.timeStampToDate(format: "h:mm a 'on' MMMM dd, yyyy")
        }
        
        //image cell
        if self.arrChats[indexPath.row]["type"].stringValue != "text"{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatImgCell", for: indexPath) as! ChatImgCell
            cell.isSender = self.arrChats[indexPath.row]["mine"].boolValue
            cell.m_progressBarImgChatUpload.isHidden = false
            cell.imngChat.backgroundColor = .white
            
            //cell.imngChat.contentMode = .scaleAspectFill
            //cell.imngChat.kf.setImage(with: self.arrChats[indexPath.row]["thumbnail"].url)
            /*
            cell.imngChat.sd_setImage(with:self.arrChats[indexPath.row]["thumbnail"].url , placeholderImage: UIImage(named: "image01"), options: [], progress: { (complitedBites, totalBites, url) in
                print("total \(totalBites) and complited \(complitedBites)")
            }) { (image , error, chache, url) in
                cell.imngChat.backgroundColor = .white
                cell.imngChat.image = image
                for tempView in cell.subviews{
                    tempView.backgroundColor = .white
                }
                cell.updateImageSize(newSize: cell.imngChat.contentClippingRect.size)
            }
            */
            
            cell.imngChat.kf.setImage(with: self.arrChats[indexPath.row]["thumbnail"].url, placeholder: UIImage(named: "image01"), options: [.cacheOriginalImage], progressBlock: { (complitedBites, Totalbites) in
                let progress = (100*complitedBites)/Totalbites
                cell.updateProgress(progressInPercentage: CGFloat(progress))
            }) { (result) in

                cell.m_progressBarImgChatUpload.isHidden = true
                //change height width of the cell
                cell.updateImageSize(newSize: cell.imngChat.contentClippingRect.size)
                cell.imngChat.superview?.backgroundColor = .white
                cell.imngChat.backgroundColor = .white
                cell.backgroundColor = .white
                cell.viewBG.backgroundColor = .white
                for tempView in cell.subviews{
                    tempView.backgroundColor = .white
                }
            }
            
            cell.lblTime.text = messageSentTimeString
            cell.setupDirection()
            
            return cell
        }
            //mine
        else if self.arrChats[indexPath.row]["mine"].boolValue{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatRightCell", for: indexPath) as! ChatRightCell
            let data = self.arrChats[indexPath.row]["content"].stringValue.data(using: .utf8)
            let messageWithEmojiEncoding = String.init(data: data!, encoding: .nonLossyASCII)
            cell.lblMessage.text = messageWithEmojiEncoding
            cell.lblTime.text = messageSentTimeString
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatLeftCell", for: indexPath) as! ChatLeftCell
            let data = self.arrChats[indexPath.row]["content"].stringValue.data(using: .utf8)
            let messageWithEmojiEncoding = String.init(data: data!, encoding: .nonLossyASCII)
            cell.lblMessage.text = messageWithEmojiEncoding
            cell.lblTime.text = messageSentTimeString
            
           // cell.viewBg.layer.cornerRadius = cell.viewBg.frame.height * 0.2
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let rcell = cell as? ChatImgCell{
            rcell.leadingConstraint.isActive = !rcell.isSender
            rcell.traillingContraint.isActive = rcell.isSender
            rcell.viewBG.backgroundColor = !rcell.isSender ? UIColor.init(red: 239/256, green: 239/256, blue: 239/256, alpha: 1) : UIColor.init(red: 82/256, green: 153/256, blue: 242/256, alpha: 1)
            rcell.lblTime.textAlignment = !rcell.isSender ? .left : .right
        }
        
        if (indexPath.row == 0 || (m_tblView.indexPathsForVisibleRows?.contains(IndexPath.init(row: 0, section: 0)) ?? false)) && manuallyScrollUp && !fetchingInProgress{
            //reload data
            AppSingleton.shared.startLoading()

            fetchingInProgress = true
            pageCount = pageCount + 1
            let dict:[String:String] = ["id":chatId,"page":"\(pageCount)"]
            let jsonStr = AppSingleton.shared.dictToJSONString(dict: dict)
            let oldNumberOfData = self.arrChats.count-1
            
            
            SocketIOManager.sharedInstance.manager.defaultSocket.emitWithAck("get_messages",with: [jsonStr]).timingOut(after: 0) { data in
                
                self.fetchingInProgress = false

                let response = JSON(data)
                if response[0]["data"]["messages"].arrayValue.count == 0{
                    //no older records found - dont call api
                    AppSingleton.shared.stopLoading()
                    return
                }
                
                var arrNewChats = response[0]["data"]["messages"].arrayValue.sorted(by: { (msg1, msg2) -> Bool in
                    return msg1["timestamp"].stringValue < msg2["timestamp"].stringValue
                })
                
                arrNewChats.append(contentsOf: self.arrChats)
                self.arrChats = arrNewChats
                
                self.m_tblView.beginUpdates()
                self.m_tblView.insertRows(at: [IndexPath(row: oldNumberOfData-1, section: 0)], with: .automatic)
                self.m_tblView.endUpdates()
                
                //after reload scroll to bottom
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.03) {
                    let indexPath = IndexPath(row: oldNumberOfData, section: 0)
                    self.m_tblView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                    AppSingleton.shared.stopLoading()
                }
        }
    }
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.arrChats[indexPath.row]["type"].stringValue != "text" ? 290 : 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? ChatImgCell{
           self.showImageInFulScreen(cell: cell)
         }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == m_tblView{
            if (self.lastContentOffset > scrollView.contentOffset.y) && scrollView.isDragging{
                // move up
                print("Manually scrolled down")
                manuallyScrollUp = true
            }
            else if scrollView.isDecelerating{
                manuallyScrollUp = false
                print("system scrolled")
            }
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
        }
    }
}


//MARK: Select and upload document
extension ChattingVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func showActionSheetForGallery() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func camera()
    {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.allowsEditing = true
        myPickerController.sourceType = UIImagePickerController.SourceType.camera
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    func photoLibrary()
    {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.allowsEditing = true
        myPickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    //MARK:- Upload document
    func uploadDocument() {
        m_progressViewImageUpload.progress = 0.0
        m_progressViewImageUpload.isHidden = false
        
        let params:Dictionary<String,String> = ["token":UserDefaults.standard.currentToken()!,"type":"image"]
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            let img = self.arrChatImgData[self.arrChatImgData.count - 1] as? UIImage
            multipartFormData.append((img!.jpegData(compressionQuality: 0.5))!, withName: "file", fileName: "\(Int64(Date().timeIntervalSince1970)).jpeg", mimeType: "image/jpeg")
            for (key, value) in params {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to:AppConstant.API.mediaUpload)
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                    print("uploading Progress : \(progress.fractionCompleted) ")
                    self.m_progressViewImageUpload.progress = Float(progress.fractionCompleted)
                })
                
                upload.responseJSON { response in
                    print(response)
                    
                    if let value = response.result.value {
                        let responseObject = JSON(value)
                        let dict:[String:String] = ["chat_id":self.chatId,"type":"image","content":" ","thumbnail":responseObject["data"]["file"].stringValue]
                        let jsonStr = AppSingleton.shared.dictToJSONString(dict: dict)
                        SocketIOManager.sharedInstance.manager.defaultSocket.emitWithAck("send_message",with: [jsonStr]).timingOut(after: 0) { data in
                            print(data)
                            self.m_progressViewImageUpload.isHidden = true
                            self.m_progressViewImageUpload.progress = 0.0
                            
                            let newMessage = JSON(data)
                            self.arrChats.append(newMessage[0]["data"])
                            self.m_tblView.beginUpdates()
                            self.m_tblView.insertRows(at: [IndexPath(row: self.arrChats.count-1, section: 0)], with: .automatic)
                            self.m_tblView.endUpdates()
                            
                            //clear textview and hide keyboard
                            // self.m_txtMessage.resignFirstResponder()
                            self.m_txtMessage.text = ""
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
                                let indexPath = IndexPath(row: self.arrChats.count-1, section: 0)
                                self.m_tblView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                            }
                        }
                    }
                }
            case .failure( _):
                break
                //print encodingError.description
            }
        }
        
       
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            arrChatImgData.append(pickedImage)
            self.uploadDocument()
        }
        self.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

class EmojiTextView: UITextView {
    
    var isEmojiMode : Bool = false
    
    override var textInputMode: UITextInputMode? {
        for mode in UITextInputMode.activeInputModes {
            print("textbox modes \(mode)")
            if mode.primaryLanguage == "emoji" && isEmojiMode{
                return mode
            }
        }
        return nil
    }
}

//Mark: Show image in full screen
extension ChattingVC : SimplePhotoViewerControllerDelegate,DTPhotoViewerControllerDataSource {
    func numberOfItems(in photoViewerController: DTPhotoViewerController) -> Int {
        return 1
    }
    
    func photoViewerController(_ photoViewerController: DTPhotoViewerController, configurePhotoAt index: Int, withImageView imageView: UIImageView) {
        imageView.image = chatImage
    }
    
    func showImageInFulScreen(cell : ChatImgCell){
        let viewController = SimplePhotoViewerController(referencedView: cell.imngChat, image: cell.imngChat.image)
        viewController.dataSource = self
        viewController.delegate = self
        chatImage = cell.imngChat.image ?? UIImage()
        self.present(viewController, animated: true, completion: nil)
    }
    
        func photoViewerControllerDidEndPresentingAnimation(_ photoViewerController: DTPhotoViewerController) {
            //photoViewerController.scrollToPhoto(at: selectedImageIndex, animated: false)
        }
        
        func photoViewerController(_ photoViewerController: DTPhotoViewerController, didScrollToPhotoAt index: Int) {
           // selectedImageIndex = index
//            if let collectionView = collectionView {
//                let indexPath = IndexPath(item: selectedImageIndex, section: 0)
//
//                // If cell for selected index path is not visible
//                if !collectionView.indexPathsForVisibleItems.contains(indexPath) {
//                    // Scroll to make cell visible
//                    collectionView.scrollToItem(at: indexPath, at: UICollectionView.ScrollPosition.bottom, animated: false)
//                }
//            }
        }
        
        func simplePhotoViewerController(_ viewController: SimplePhotoViewerController, savePhotoAt index: Int) {
            //UIImageWriteToSavedPhotosAlbum(images[index], nil, nil, nil)
        }
    
    
    }


