//
//  ChatRightCell.swift
//  myFirstAppOnMacbookPro
//
//  Created by Aditi Pancholi on 26/07/19.
//  Copyright © 2019 Aditi Pancholi. All rights reserved.
//

import UIKit

class ChatRightCell: UITableViewCell {

    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewBg.layer.cornerRadius = 15
            //viewBg.frame.height/2

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
