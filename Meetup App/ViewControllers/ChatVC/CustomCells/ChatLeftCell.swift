//
//  ChatCell.swift
//  myFirstAppOnMacbookPro
//
//  Created by Aditi Pancholi on 25/07/19.
//  Copyright © 2019 Aditi Pancholi. All rights reserved.
//

import UIKit

enum enumChatCell{
    case host
    case client
    
    func returnColor()->UIColor{
        switch self {
        case .host:
            return .red
        case .client:
            return .blue
        }
    }
}

class ChatLeftCell: UITableViewCell {

    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var viewBg: UIView!
    var cellIsFor : enumChatCell = .host
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewBg.layer.cornerRadius = 15
            //viewBg.frame.height/2
        
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    
}
