//
//  ChatImgCell.swift
//  myFirstAppOnMacbookPro
//
//  Created by Aditi Pancholi on 26/07/19.
//  Copyright © 2019 Aditi Pancholi. All rights reserved.
//

import UIKit
import UICircularProgressRing

class ChatImgCell: UITableViewCell {

    
    @IBOutlet weak var m_progressBarImgChatUpload: UICircularProgressRing!
    @IBOutlet weak var m_imgWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var m_imgHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var imngChat: UIImageView!
    @IBOutlet weak var traillingContraint: NSLayoutConstraint!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    var isSender : Bool = true
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imngChat.layer.cornerRadius = 5
        imngChat.layer.masksToBounds = true
        m_progressBarImgChatUpload.isHidden = true
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        print("Prepare for reuse called")
        imngChat.image = UIImage.init(named: "image01")
        for innerViews in self.subviews{
            innerViews.backgroundColor = .white
        }
        //m_imgWidthConstraint.constant = 250
        //m_imgHeightConstraint.constant = 250
        //self.layoutIfNeeded()
    }
    
    func setupDirection(){
       leadingConstraint.isActive = !isSender
       traillingContraint.isActive = isSender
       self.layoutIfNeeded()
    }
    
    func updateProgress(progressInPercentage: CGFloat){
        m_progressBarImgChatUpload.value = progressInPercentage
        print(progressInPercentage)
    }
    
    func updateImageSize(newSize:CGSize) {
        print("New sale size \(newSize)")
//        m_imgWidthConstraint.constant = newSize.width
//        m_imgHeightConstraint.constant = newSize.height
        imngChat.layer.cornerRadius = 5
        imngChat.backgroundColor = .white
        for innerViews in self.subviews{
            innerViews.backgroundColor = .white
        }
        self.setNeedsLayout()
    }
}

