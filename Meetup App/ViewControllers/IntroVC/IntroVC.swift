//
//  IntroVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 20/11/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class IntroVC: UIViewController {

    @IBOutlet weak var m_collectionIntro: UICollectionView!
    
    @IBOutlet weak var m_pageControl: UIPageControl!
    @IBOutlet weak var m_btnNext: UIButton!
    let arrImages = [#imageLiteral(resourceName: "intro_1"),#imageLiteral(resourceName: "intro_2"),#imageLiteral(resourceName: "intro_3"),#imageLiteral(resourceName: "intro_4")]
    let arrTitles = ["Browse And Discover","Invites Friends","Organize With Friends","Earn Money"]
    let arrColorStr = ["Discover","Invites","Organize","Earn"]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func changeColorOfSring(wholeStr:String, particuarStr:[String]) -> NSAttributedString {
        let main_string = wholeStr
        //        let string_to_color = "World"
        
        let attributedString = NSMutableAttributedString(string:main_string)
        
        for i in particuarStr {
            let range = (main_string as NSString).range(of: i)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red , range: range)
        }
        
        return attributedString
    }
    
    @IBAction func actionNext(_ sender: UIButton) {
        if (arrTitles.count - 1) != (self.m_pageControl.currentPage) {
            m_collectionIntro.scrollToItem(at: IndexPath(item: self.m_pageControl.currentPage + 1, section: 0), at: UICollectionView.ScrollPosition.right, animated: true)
        } else {
            
            UserDefaults().introVisit()
            let vcObj = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WelcomeVC") as? WelcomeVC
            self.navigationController?.pushViewController(vcObj!, animated: true)
        }
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        if (self.m_pageControl.currentPage) != 0 {
            m_collectionIntro.scrollToItem(at: IndexPath(item: self.m_pageControl.currentPage - 1, section: 0), at: UICollectionView.ScrollPosition.left, animated: true)
        }
    }
}

extension IntroVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImages.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IntroCell", for: indexPath) as? IntroCell
        cell?.m_imgViewIntro.image = arrImages[indexPath.row]
        cell?.m_lblTitle.attributedText = changeColorOfSring(wholeStr: arrTitles[indexPath.row], particuarStr: [arrColorStr[indexPath.row]])
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.m_pageControl.currentPage = indexPath.item
    }
}

