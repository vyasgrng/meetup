//
//  IntroCell.swift
//  Meetup App
//
//  Created by Birju Bhatt on 20/11/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class IntroCell: UICollectionViewCell {
    
    @IBOutlet weak var m_imgViewIntro: UIImageView!
    @IBOutlet weak var m_lblTitle: UILabel!
    @IBOutlet weak var m_lblDesc: UILabel!
}
