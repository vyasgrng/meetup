//
//  WelcomeVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 13/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import GoogleSignIn
import SwiftyJSON
import Alamofire
import FBSDKLoginKit
import TOWebViewController

class WelcomeVC: UIViewController {

    @IBOutlet weak var m_imgUserProfilePhoto: UIImageView!
    
    @IBOutlet weak var m_btnLogIn: UIButton!
    @IBOutlet weak var m_viewBottom: UIView!
    @IBOutlet weak var m_btnGoogle: UIButton!
    @IBOutlet weak var m_btnCreateAcc: UIButton!
    @IBOutlet weak var m_btnPolicy: UIButton!
    @IBOutlet weak var m_btnTerms: UIButton!
    var isSignin = Bool()
    
    @IBOutlet weak var m_btnFb: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
        
//        UIFont.familyNames.forEach({ familyName in
//            let fontNames = UIFont.fontNames(forFamilyName: familyName)
//            print(familyName, fontNames)
//        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isSignin {
            AppSingleton.shared.startLoading()
        }
    }
    
    //MARK:- Set initial UI
    func basicSetUp() {
        GIDSignIn.sharedInstance().clientID = AppConstant.Google.clientID
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        m_viewBottom.addCornerRadiusParticularSide(rectCorner: [.topLeft,.topRight], radius: m_viewBottom.frame.size.width/5)
        m_btnFb.addBorder(width: 1, color: .white)
        m_btnFb.addCornerRadius(radius: 5.0)
        m_btnGoogle.addBorder(width: 1, color: .white)
        m_btnGoogle.addCornerRadius(radius: 5.0)
        m_btnCreateAcc.addCornerRadius(radius: 5.0)
    }
    
    
    @IBAction func btnNextTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        let loginVCObj = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
        self.pushVC(destinationVC: loginVCObj!)
    }
    
    
    @IBAction func btnGoogleLoginTapped(_ sender: UIButton) {
        self.isSignin = true
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    @IBAction func btnFacebookLoginTapped(_ sender: UIButton) {
        let fbLoginManager : LoginManager = LoginManager()
        fbLoginManager.logIn(permissions: ["email"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : LoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions.count > 0  {
                    AppSingleton.shared.startLoading()
                    if(fbloginresult.grantedPermissions.contains("email")) {
                        if((AccessToken.current) != nil){
                            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                                if (error == nil){
                                    let dict = result as! [String : Any]
                                    print(result!)
                                    print(dict)
                                    
                                    guard let imageURL = ((dict["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String else {
                                        return
                                    }
                                    let params:[String:String] = ["login_type":"1","token":dict["id"]! as! String,"firebase_token":"123","device_type":"0","app_version":self.getAppVersion(),"first_name":dict["first_name"]! as! String ,"last_name":dict["last_name"]! as! String,"email":dict["email"]! as! String,"profile_url":imageURL]
                                    self.socialSignIn(params: params)
                                }
                            })
                        }
                    }
                } else {
                    AppSingleton.shared.stopLoading()
                }
            } else {
                
            }
        }
    }
    
    
    @IBAction func btnSignupTapped(_ sender: UIButton) {
        let signUpVCObj = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as? SignUpVC
        self.pushVC(destinationVC: signUpVCObj!)
    }
    
    
    @IBAction func btnPrivacyTapped(_ sender: UIButton) {
        let webVC = TOWebViewController(url: URL(string: BaseUrl + "terms")!)
        webVC.showPageTitles = false
        webVC.title = "Privacy Policy"
        let navi = UINavigationController(rootViewController: webVC)
        self.present(navi, animated: true, completion: nil)
    }
    
    @IBAction func btnTermsTapped(_ sender: UIButton) {
        let webVC = TOWebViewController(url: URL(string: BaseUrl + "terms")!)
        webVC.showPageTitles = false
        webVC.title = "Terms & Conditions"
        let navi = UINavigationController(rootViewController: webVC)
        self.present(navi, animated: true, completion: nil)
    }
    
    //MARK:- Get app version number
    func getAppVersion() -> String {
        return (Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String)!
    }
    
    //MARK:- Social Sign In
    func socialSignIn(params:[String:String]) {
        Alamofire.request(AppConstant.API.socialLogin, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        UserDefaults.standard.set(encodable: UserModal(data: JSON(value)), forKey: "userData")
                        UserDefaults.standard.storeToken(token: responseObject["data"]!["token"].stringValue)
                        UserDefaults.standard.setUserType(type: UserType.Joinee.rawValue)
                        appDelegate.setRootAfterLogin()
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
}

extension WelcomeVC: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        AppSingleton.shared.stopLoading()
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            
            AppSingleton.shared.startLoading()
            let imageUrl = "\(String(describing: user!.profile.imageURL(withDimension: 200)))"
            let params:[String:String] = ["login_type":"0","token":user!.authentication.idToken,"firebase_token":"123","device_type":"0","app_version":getAppVersion(),"first_name":user!.profile.givenName,"last_name":user!.profile.familyName,"email":user!.profile.email,"profile_url":imageUrl]
            
            self.socialSignIn(params: params)
            
        }
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        AppSingleton.shared.stopLoading()
    }
}

extension WelcomeVC: GIDSignInUIDelegate {
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        
    }
}
