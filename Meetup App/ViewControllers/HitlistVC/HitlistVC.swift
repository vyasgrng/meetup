//
//  HitlistVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 18/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import GooglePlaces



class HitlistVC: BaseVC{
    
    // Variables
    var arrayPartyList:Array<JSON> = []
    var latitude = ""
    var longitude = ""
    var searchDate = ""
    var partyId = ""
    var objBottomView: ShareEditDelView?
    
    // Outlets
    @IBOutlet weak var m_tblViewEnjoy: UITableView!
    @IBOutlet weak var m_viewSearchBar: UIView!
    @IBOutlet weak var m_txtFldSearchBar: UITextField!
    @IBOutlet weak var m_btnFilter: UIButton!
    @IBOutlet weak var m_viewBtnFilter: UIView!
    @IBOutlet weak var m_viewJoinPartyTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var m_viewJoinPartyRequestMessage: UIView!
    @IBOutlet weak var m_txtJoiningRequestMessage: UITextView!
    @IBOutlet weak var m_btnCancel: UIButton!
    @IBOutlet weak var m_btnShare: DefaultButton!
    
    @IBOutlet weak var m_constTop: NSLayoutConstraint!
    @IBOutlet weak var m_imgViewEmptyEvent: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //Get enjoted list
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.02) {
            self.m_constTop.constant = self.topView.frame.height + 15
            self.hitListAPI()
        }
    }
    
    //MARK:- Set initial UI
    func basicSetUp() {
        
        m_viewSearchBar.addCornerRadius(radius: 5.0)
        m_viewSearchBar.addShadow(offset: CGSize(width: 1, height: 2), color: .lightGray, opacity: 0.6, radius: 4)
        m_viewBtnFilter.addCornerRadius(radius: 5.0)
        m_viewBtnFilter.addShadow(offset: CGSize(width: 1, height: 2), color: .lightGray, opacity: 0.6, radius: 4)
        
        addBackButton(withImage: #imageLiteral(resourceName: "Burger Button"),isFirstHierarchy: false)
        addRightButton(withText: "", withImageName: "Group 135", vc: self, target: #selector(goToNotification))
        addTopTitle(title: "Events")
        self.m_constTop.constant = self.topView.frame.height + 15
        self.m_tblViewEnjoy.delegate = self
        self.m_tblViewEnjoy.dataSource = self
    }
    
    @objc func goToNotification() {
        let notificationVCObj = self.storyboard?.instantiateViewController(withIdentifier: "NotificationsVC") as? NotificationsVC
        self.navigationController?.pushViewController(notificationVCObj!, animated: true)
    }
    
    override func backClicked(_ sender: UIButton) {
        self.slideMenuController()?.openLeft()
    }
    
    //MARK:- Get enjoyed list
    func hitListAPI(isFilter:Bool = false) {
        AppSingleton.shared.startLoading()
        var params = ["":""]
        if isFilter {
            if (searchDate.isEmpty) {
                params = ["token":UserDefaults.standard.currentToken()!,"page":"0","date":"","lat":latitude,"lng":longitude,"keyword":m_txtFldSearchBar.text!]
            } else {
                params = ["token":UserDefaults.standard.currentToken()!,"page":"0","date":searchDate.convertToDateFormate(currentDateFormate: "dd/MM/yyyy",dateFormate: "yyyy-MM-dd"),"lat":latitude,"lng":longitude,"keyword":m_txtFldSearchBar.text!]
            }
        } else {
            params = ["token":UserDefaults.standard.currentToken()!,"page":"0","keyword":m_txtFldSearchBar.text!]
        }
        
        Alamofire.request(AppConstant.API.hitList, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        self.arrayPartyList = responseObject["data"]!["events"].arrayValue
                        self.m_imgViewEmptyEvent.isHidden = true
                        self.m_tblViewEnjoy.isHidden = false
                        self.m_viewSearchBar.isHidden = false
                        self.m_viewBtnFilter.isHidden = false
                        if self.arrayPartyList.count == 0 {
                            self.m_imgViewEmptyEvent.isHidden = false
                            self.m_tblViewEnjoy.isHidden = true
                            self.m_viewSearchBar.isHidden = true
                            self.m_viewBtnFilter.isHidden = true
                        }
                        self.m_tblViewEnjoy.reloadData()
                        
                    }
                } else {
                    AppSingleton.shared.stopLoading()
                }
            case .failure(let error):
                print(error)
                AppSingleton.shared.stopLoading()
            }
        }
    }
    
    //MARK:- Action filter view open
    @IBAction func actionFilterOpen(_ sender: UIButton) {
        
        let searchPartyVCObj = self.storyboard?.instantiateViewController(withIdentifier: "SearchPartyVC") as? SearchPartyVC
        searchPartyVCObj!.delegate = self
//        self.navigationController?.present(searchPartyVCObj!, animated: true, completion: nil)
        self.present(searchPartyVCObj!, animated: true, completion: nil)
    }
    
    @objc func actionMore(_ sender:UIButton) {
        if let data = arrayPartyList[sender.tag].dictionary,
            let id = data["id"]?.string {
            let view = ShareEditDelView.getInstance(partyId: id, owner: self, refresh: {
                self.hitListAPI()
            })
            view.show()
        }
    }
}


extension HitlistVC: searchPartyDelegate {
    func searchParties(lat:String,long:String, dt:String) {
        latitude = lat
        longitude = long
        searchDate = dt
        hitListAPI(isFilter: true)
    }
}

extension HitlistVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayPartyList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data = arrayPartyList[indexPath.row].dictionaryValue
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "EnjoyedCell", for: indexPath) as? EnjoyedCell
        cell?.m_btnTheme.addCornerRadius(radius: 5.0)
        cell?.m_btnTheme.addBorder(width: 1, color: UIColor().convertHexStringToColor(hexString: "202564"))
        cell?.m_btnTheme.setTitle(data["theme"]?.stringValue, for: .normal)
        cell?.m_btnTheme.contentEdgeInsets = UIEdgeInsets(top: 5, left: 8, bottom: 5, right: 8)
        let priceValue = String(format: "%.3f", data["price"]!.floatValue)
        cell?.m_lblPrice.text = "\(data["currency_readable"]?.stringValue ?? "USD") \(priceValue)"
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            cell?.m_viewDate.addCornerRadiusParticularSide(rectCorner: [.topLeft,.bottomLeft], radius: ((cell?.m_viewDate.frame.height)!/2))
        }
        let partyTime = (data["start"]?.doubleValue.timeStampToDate(format: "hh:mm a"))! + " to " + (data["end"]?.doubleValue.timeStampToDate(format: "hh:mm a"))!
        cell?.m_lblDate.text = (data["date"]?.doubleValue.timeStampToDate(format: "dd MMM yyyy"))! + " / " + partyTime + " / " + data["city"]!.stringValue
        cell?.m_lblName.text = data["name"]?.stringValue
        cell?.m_btnJoin.addCornerRadius(radius: 3.0)
        cell?.m_imgView.kf.setImage(with: URL(string: (data["image"]?.stringValue)!), placeholder: UIImage(named: "image01"), options: [], progressBlock: nil, completionHandler: { (result) in
            cell?.m_imgView.addCornerRadius(radius: 10.0)
        })
        cell?.m_imgViewHost.kf.setImage(with: URL(string: (data["host_image"]?.stringValue)!), placeholder: UIImage(named: "profile"), options: [], progressBlock: nil, completionHandler: { (result) in
            cell?.m_imgViewHost.addCornerRadius(radius: (cell?.m_imgViewHost.frame.size.width)!/2)
        })

        cell?.m_wraperView.addShadowWithCorner(cornerRadius: 10.0, offset: .zero, color: .lightGray , radius: 4.0, opacity: 0.6)
        cell?.m_btnJoin.tag = indexPath.row
        cell?.m_lblLikeCount.text = data["like_count"]?.stringValue
        cell?.m_lblCommentCount.text = data["review_count"]?.stringValue
       // cell?.m_btnMore.addTarget(self, action: #selector(actionMore(_:)), for: .touchUpInside)
        switch data["status"]?.int {
            
        case EventStatus.requested.rawValue:
            cell?.m_btnJoin.isHidden = false
            cell?.m_btnJoin.setTitle("REQUESTED", for: .normal)
            break
        case EventStatus.approved.rawValue:
            //cell?.m_btnJoin.isHidden = true
            cell?.m_btnJoin.isHidden = false
            cell?.m_btnJoin.setTitle("PAY", for: .normal)
            cell?.m_btnJoin.addTarget(self, action: #selector(joinParty(_:)), for: .touchUpInside)
            break
        case EventStatus.paid.rawValue:
            cell?.m_btnJoin.isHidden = true
            break
        case EventStatus.owner.rawValue:
            cell?.m_btnJoin.isHidden = true
            break
        case EventStatus.nostatus.rawValue:
            cell?.m_btnJoin.isHidden = false
            cell?.m_btnJoin.setTitle("JOIN", for: .normal)
            cell?.m_btnJoin.addTarget(self, action: #selector(joinParty(_:)), for: .touchUpInside)
            break
        default:
            break
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = arrayPartyList[indexPath.row].dictionaryValue
        let vcObj = UIStoryboard(name: "Party", bundle:Bundle.main).instantiateViewController(withIdentifier: "PartyDetailVC") as? PartyDetailVC
        vcObj?.partyId = (data["id"]?.stringValue)!
        self.navigationController?.pushViewController(vcObj!, animated: true)
    }
    
    func openGooglePlacePicker() {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.formattedAddress.rawValue) | UInt(GMSPlaceField.name.rawValue) | UInt(GMSPlaceField.coordinate.rawValue) |
            UInt(GMSPlaceField.placeID.rawValue))!
        autocompleteController.placeFields = fields
        
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .geocode
        autocompleteController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)
        
    }
    
    //MARK:- Join Party Action
    @objc func joinParty(_ sender:UIButton) {
        
        m_txtJoiningRequestMessage.addBorder(width: 1, color: .lightGray)
        m_btnCancel.addBorder(width: 1, color: UIColor().convertHexStringToColor(hexString: "E94A47"))
        m_btnCancel.addCornerRadius(radius: 8)
        
        if sender.titleLabel?.text == "PAY"{
            
            let nib : NSArray = Bundle.main.loadNibNamed("PaymentSelectionVC", owner: self, options: nil)! as NSArray
            let myVC = nib.object(at: 0) as! PaymentSelectionVC
            myVC.delegate = self
            myVC.addViewWithAnimation(onView:self.view)
            return
        }else if sender.titleLabel?.text == "JOIN"{
            //show msg popup to join party
            m_viewJoinPartyTopConstraint.constant = 0
            self.view.bringSubviewToFront(m_viewJoinPartyRequestMessage)
            UIView.animate(withDuration: 0.4) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func btnShare_requestMessageJoinParty(_ sender: UIButton) {
        
        let data = arrayPartyList[sender.tag].dictionaryValue
        partyId = data["id"]!.stringValue

        m_txtJoiningRequestMessage.resignFirstResponder()
        AppSingleton.shared.startLoading()
        //developer2
        let params = ["token":UserDefaults.standard.currentToken()!,"id":partyId,"message" : m_txtJoiningRequestMessage.text! ]
        //"I would like to join"
        Alamofire.request(AppConstant.API.eventJoin, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    
                    self.m_viewJoinPartyTopConstraint.constant = 1600
                    UIView.animate(withDuration: 0.4) {
                        self.view.layoutIfNeeded()
                    }
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        self.m_tblViewEnjoy.reloadData()
                        self.hitListAPI()
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    @IBAction func btnCancel_requestMesageJoinParty(_ sender: UIButton) {
        
        m_viewJoinPartyTopConstraint.constant = 1600
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
}

//MARK: Delegate from payment selection
extension HitlistVC : ProtocolPaymentSelectionVC{
    func navigateToChangeDefualtCard() {
        let vcObj = UIStoryboard(name: "InnerProfileSection", bundle: Bundle.main).instantiateViewController(withIdentifier: "PaymentListVC") as? PaymentListVC
        self.navigationController?.pushViewController(vcObj!, animated: true)
    }
}

extension HitlistVC: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.openGooglePlacePicker()
        return false
    }
}

extension HitlistVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        //        print("Place name: \(place.name)")
        //        print("Place ID: \(place.placeID)")
        //        print("Place attributions: \(place.attributions)")
        //        print(place.coordinate.latitude,place.coordinate.longitude)
        dismiss(animated: true, completion: nil)
        
        latitude = "\(place.coordinate.latitude)"
        longitude = "\(place.coordinate.longitude)"
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}


