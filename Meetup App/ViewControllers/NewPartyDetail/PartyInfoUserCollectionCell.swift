//
//  PartyInfoUserCollectionCell.swift
//  Meetup App
//
//  Created by STL on 16/01/20.
//  Copyright © 2020 Birju Bhatt. All rights reserved.
//

import UIKit

class PartyInfoUserCollectionCell: UICollectionViewCell {
    @IBOutlet weak var m_imgViewUser: UIImageView!
    @IBOutlet weak var m_lblUserName: UILabel!
    
}
