//
//  PartyDetailPhotosVC.swift
//  Meetup App
//
//  Created by STL on 11/01/20.
//  Copyright © 2020 Birju Bhatt. All rights reserved.
//

import UIKit
import SwiftyJSON
class PartyDetailPhotosVC: UIViewController {

    @IBOutlet weak var m_collectionPhotos: UICollectionView!
    var dictPartyPhotos:Dictionary<String,JSON> = [:]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}

extension PartyDetailPhotosVC: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dictPartyPhotos["images"]?.arrayValue.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PartyPhotosCollectionCell", for: indexPath) as? PartyPhotosCollectionCell
        cell?.m_imgViewPartyPhoto.kf.setImage(with: URL(string: ((dictPartyPhotos["images"]?.arrayValue[indexPath.row].stringValue)!)), placeholder: UIImage(named: "image01"), options: [], progressBlock: nil, completionHandler: { (result) in
        })
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let numberOfItemsPerRow = 3
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(numberOfItemsPerRow - 1))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(numberOfItemsPerRow))
        return CGSize(width: size, height: size)
    }
}
