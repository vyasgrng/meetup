//
//  PartyDetailLikeReviewCell.swift
//  Meetup App
//
//  Created by STL on 12/01/20.
//  Copyright © 2020 Birju Bhatt. All rights reserved.
//

import UIKit

class PartyDetailLikeReviewCell: UITableViewCell {

    @IBOutlet weak var m_btnFav: UIButton!
    @IBOutlet weak var m_lblReviewCount: UILabel!
    @IBOutlet weak var m_btnStatus: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
