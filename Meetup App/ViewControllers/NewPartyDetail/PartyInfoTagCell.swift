//
//  PartyInfoTagCell.swift
//  Meetup App
//
//  Created by STL on 16/01/20.
//  Copyright © 2020 Birju Bhatt. All rights reserved.
//

import UIKit

class PartyInfoTagCell: UITableViewCell {

    @IBOutlet weak var m_lblTitle: UILabel!
    @IBOutlet weak var m_btnTag: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
