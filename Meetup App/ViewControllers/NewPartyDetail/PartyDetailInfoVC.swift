//
//  PartyDetailInfoVC.swift
//  Meetup App
//
//  Created by STL on 11/01/20.
//  Copyright © 2020 Birju Bhatt. All rights reserved.
//

import UIKit
import SwiftyJSON

enum collectionViewTag:Int {
    case collectionViewHost = 1000
    case collectionViewMember = 1001
}

class PartyDetailInfoVC: UIViewController {
    
    let arrCaptions = ["Description","When","Location","Price","Tag","Host","Members"]
    var dictPartyInfo:Dictionary<String,JSON> = [:]
    @IBOutlet var m_tblView: UITableView!
    
    var openViewController: ((_ viewController: UIViewController)->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

}

extension PartyDetailInfoVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCaptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if arrCaptions[indexPath.row] == "Tag" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PartyInfoTagCell", for: indexPath) as? PartyInfoTagCell
            cell?.m_lblTitle.text = arrCaptions[indexPath.row]
            cell?.m_btnTag.addCornerRadius(radius: 5.0)
            cell?.m_btnTag.addBorder(width: 1, color: UIColor().convertHexStringToColor(hexString: "202564"))
            cell?.m_btnTag.setTitle(dictPartyInfo["theme"]?.stringValue, for: .normal)
            cell?.m_btnTag.contentEdgeInsets = UIEdgeInsets(top: 8, left: 5, bottom: 8, right: 5)
            return cell!
        } else if arrCaptions[indexPath.row] == "Host" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PartyInfoUserCell", for: indexPath) as? PartyInfoUserCell
            cell?.m_collectionViewUsers.tag = collectionViewTag.collectionViewHost.rawValue
            cell?.m_lblTitle.text = arrCaptions[indexPath.row]
            return cell!
        } else if arrCaptions[indexPath.row] == "Members" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PartyInfoUserCell", for: indexPath) as? PartyInfoUserCell
            cell?.m_collectionViewUsers.tag = collectionViewTag.collectionViewMember.rawValue
            cell?.m_lblTitle.text = arrCaptions[indexPath.row]
            return cell!
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PartyInfoCell", for: indexPath) as? PartyInfoCell
            cell?.m_lblTitle.text = arrCaptions[indexPath.row]
            
            switch arrCaptions[indexPath.row] {
            case "Description":
                cell?.m_lblSubtitle.text = dictPartyInfo["description"]?.stringValue
                break
            case "When":
                let partyTime = (dictPartyInfo["start"]?.doubleValue.timeStampToDate(format: "hh:mm a"))! + " to " + (dictPartyInfo["end"]?.doubleValue.timeStampToDate(format: "hh:mm a"))!
                cell?.m_lblSubtitle.text = (dictPartyInfo["date"]?.doubleValue.timeStampToDate(format: "dd MMM yyyy"))! + " / " + partyTime + " / " + dictPartyInfo["city"]!.stringValue
                break
            case "Location":
                cell?.m_lblSubtitle.text = dictPartyInfo["address"]?.stringValue
                break
            case "Price":
                cell?.m_lblSubtitle.text = dictPartyInfo["currency_readable"]!.stringValue + " " + dictPartyInfo["price"]!.stringValue
                break
            default:
                break
            }
            return cell!
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension PartyDetailInfoVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == collectionViewTag.collectionViewHost.rawValue {
            return 1
        }
        return (dictPartyInfo["members"]?.arrayValue.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PartyInfoUserCollectionCell", for: indexPath) as? PartyInfoUserCollectionCell
        
        if collectionView.tag == collectionViewTag.collectionViewHost.rawValue {
            cell?.m_imgViewUser.kf.setImage(with: dictPartyInfo["host"]!["profile_pic"].url, placeholder: UIImage(named: "profile"), options: [], progressBlock: nil, completionHandler: { (result) in
            })
            cell?.m_lblUserName.text = dictPartyInfo["host"]!["first_name"].stringValue + " " + dictPartyInfo["host"]!["last_name"].stringValue
        } else {
            cell?.m_imgViewUser.kf.setImage(with: dictPartyInfo["members"]!.arrayValue[indexPath.row].dictionaryValue["profile_pic"]!.url, placeholder: UIImage(named: "profile"), options: [], progressBlock: nil, completionHandler: { (result) in
            })
            let userName = dictPartyInfo["members"]!.arrayValue[indexPath.row].dictionaryValue["first_name"]!.stringValue + " " + dictPartyInfo["members"]!.arrayValue[indexPath.row].dictionaryValue["last_name"]!.stringValue
            cell?.m_lblUserName.text = userName
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            cell?.m_imgViewUser.addCornerRadius(radius: (cell?.m_imgViewUser.frame.width)! / 2)
        }

        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        if let hostId = dictPartyInfo["host"]?["id"].stringValue {
            let vc = ProfileDetailsViewController.getInstance(hostId: hostId)
            self.openViewController?(vc)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 4, height: collectionView.frame.height)
    }
}
