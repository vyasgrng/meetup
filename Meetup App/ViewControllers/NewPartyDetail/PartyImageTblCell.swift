//
//  PartyImageTblCell.swift
//  Meetup App
//
//  Created by STL on 12/01/20.
//  Copyright © 2020 Birju Bhatt. All rights reserved.
//

import UIKit

class PartyImageTblCell: UITableViewCell {

    @IBOutlet weak var m_collectionPartyImages: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
