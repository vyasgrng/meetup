//
//  PartyUpdateCell.swift
//  Meetup App
//
//  Created by STL on 16/01/20.
//  Copyright © 2020 Birju Bhatt. All rights reserved.
//

import UIKit

class PartyUpdateCell: UITableViewCell {

    @IBOutlet weak var m_viewContain: UIView!
    @IBOutlet weak var m_lblTitle: UILabel!
    @IBOutlet weak var m_lblTimeAgo: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
        m_viewContain.addShadowWithCorner(cornerRadius: 5, offset: .zero, color: .lightGray, radius: 4, opacity: 0.6)
    }

}
