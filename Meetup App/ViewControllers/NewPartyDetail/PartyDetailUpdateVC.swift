//
//  PartyDetailUpdateVC.swift
//  Meetup App
//
//  Created by STL on 11/01/20.
//  Copyright © 2020 Birju Bhatt. All rights reserved.
//

import UIKit
import SwiftyJSON

class PartyDetailUpdateVC: UIViewController {

    @IBOutlet weak var m_tblViewUpdates: UITableView!
    var dictPartyUpdates:Dictionary<String,JSON> = [:]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}

extension PartyDetailUpdateVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (dictPartyUpdates["updates"]?.arrayValue.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PartyUpdateCell", for: indexPath) as? PartyUpdateCell
        return cell!
    }
}
