//
//  OnGoingCell.swift
//  Meetup App
//
//  Created by Birju Bhatt on 23/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class OnGoingCell: UITableViewCell {

    @IBOutlet weak var m_imgView: UIImageView!
    @IBOutlet weak var m_lblName: UILabel!
    @IBOutlet weak var m_btnTheme: UIButton!
    @IBOutlet weak var m_lblPrice: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        m_btnTheme.addCornerRadius(radius: 5.0)
        m_imgView.addCornerRadius(radius: 5.0)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
