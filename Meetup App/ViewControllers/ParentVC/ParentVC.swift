//
//  ParentVC.swift
//  Meetup App
//
//  Created by STL on 26/12/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialBottomAppBar
import MaterialComponents.MaterialThemes

class ParentVC: UIViewController {

    @IBOutlet weak var m_viewContainer: UIView!
    @IBOutlet weak var m_constBottom: NSLayoutConstraint!
    
    var enjoyedVCObj = UIViewController()
    var inboxVCObj = UIViewController()
    var hitListVCObj = UIViewController()
    var invitationVCObj = UIViewController()
    var onGoingVCObj = UIViewController()
    let bottomBarView = MDCBottomAppBarView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setUpBottomView()
        enjoyedVCObj = (self.storyboard?.instantiateViewController(withIdentifier: "EnjoyedVC") as? EnjoyedVC)!
        inboxVCObj = (self.storyboard?.instantiateViewController(withIdentifier: "InboxVC") as? InboxVC)!
        hitListVCObj = (self.storyboard?.instantiateViewController(withIdentifier: "HitlistVC") as? HitlistVC)!
        invitationVCObj = (self.storyboard?.instantiateViewController(withIdentifier: "InvitationVC") as? InvitationVC)!
        onGoingVCObj = (self.storyboard?.instantiateViewController(withIdentifier: "OnGoingVC") as? OnGoingVC)!
        centerBtnTapped()
}
    
    func layoutBottomAppBar() {
        let size = bottomBarView.sizeThatFits(view.bounds.size)
        let bottomBarViewFrame = CGRect(x: 0,
                                        y: view.bounds.size.height - size.height,
                                        width: size.width,
                                        height: size.height)
        bottomBarView.frame = bottomBarViewFrame
        m_constBottom.constant = bottomBarViewFrame.size.height - 38
        if #available(iOS 11.0, *) {
            if UIDevice.current.hasNotch {
                m_constBottom.constant = bottomBarViewFrame.size.height - (38*2)
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      self.view.backgroundColor = .white

      self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillLayoutSubviews() {
      super.viewWillLayoutSubviews()
      layoutBottomAppBar()
    }

    @available(iOS 11, *)
    override func viewSafeAreaInsetsDidChange() {
      super.viewSafeAreaInsetsDidChange()
      layoutBottomAppBar()
    }
    
    //MARK:- Set up bottom bar
    func setUpBottomView() {
        
        bottomBarView.autoresizingMask = [ .flexibleWidth, .flexibleTopMargin ]
        view.addSubview(bottomBarView)
        
        // Bottom bar set up
        bottomBarView.floatingButton.setImage(#imageLiteral(resourceName: "ic_niddle"), for: .normal)
        bottomBarView.floatingButton.setBackgroundColor(UIColor.init(red: 250, green: 50, blue: 51, alpha: 1.0))
        bottomBarView.floatingButton.addTarget(self, action: #selector(centerBtnTapped), for: .touchUpInside)
        
        
        var imgNameLeft = "noun_Mail_8984"
        var leftBtnTitle = "Invitation"
        if UserDefaults.standard.currentUserType() == UserType.Joinee.rawValue {
            imgNameLeft = "noun_wish list_1956813"
            leftBtnTitle = "Wish List"
        }
        
        let buttonLeading = UIButton(type: .system)
        buttonLeading.setTitleColor(UIColor().appThemeBlueColor(), for: .normal)
        buttonLeading.setImage(UIImage(named: imgNameLeft), for: .normal)
        buttonLeading.setTitle(leftBtnTitle, for: .normal)
        buttonLeading.titleLabel?.font = UIFont().RobotoRegular(size: 13.0)
        buttonLeading.sizeToFit()
        buttonLeading.centerVertically(contentLeftPadding: 20)
        buttonLeading.addTarget(self, action: #selector(leftBtnTapped), for: .touchUpInside)
        let barButtonLeadingItem = UIBarButtonItem(customView: buttonLeading)
        
        let buttonTrailing = UIButton(type: .system)
        buttonTrailing.setTitleColor(UIColor().appThemeBlueColor(), for: .normal)
        buttonTrailing.setImage(UIImage(named: "Group 1275"), for: .normal)
        buttonTrailing.setTitle("Chat", for: .normal)
        buttonTrailing.titleLabel?.font = UIFont().RobotoRegular(size: 13.0)
        buttonTrailing.sizeToFit()
        buttonTrailing.centerVertically(contentRightPadding: 25)
        buttonTrailing.addTarget(self, action: #selector(rightBtnTapped), for: .touchUpInside)
        let barButtonTrailingItem = UIBarButtonItem(customView: buttonTrailing)
        
        bottomBarView.leadingBarButtonItems = [ barButtonLeadingItem ]
        bottomBarView.trailingBarButtonItems = [ barButtonTrailingItem ]
        
        let btnCenterDashboardTitle = UIButton(type: .custom)
        btnCenterDashboardTitle.frame = CGRect(x: 0, y: 0, width: 20, height: 30)
        btnCenterDashboardTitle.titleLabel?.font = UIFont().RobotoRegular(size: 13.0)
        btnCenterDashboardTitle.setTitle("Dashboard", for: .normal)
        btnCenterDashboardTitle.contentEdgeInsets = UIEdgeInsets(top: 5, left: 8, bottom: 5, right: 8)
        btnCenterDashboardTitle.setTitleColor(UIColor().appThemeBlueColor(), for: .normal)
        view.addSubview(btnCenterDashboardTitle)
        
        btnCenterDashboardTitle.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            if #available(iOS 11.0, *) {
                if UIDevice.current.hasNotch {
                    make.bottom.equalTo(self.view.snp.bottomMargin)
                } else {
                    make.bottom.equalToSuperview().offset(0)
                }
            }
        }
    }
    
    @objc func leftBtnTapped() {
        remove(asChildViewController: inboxVCObj)
        if UserDefaults.standard.currentUserType() == UserType.Joinee.rawValue {
            remove(asChildViewController: enjoyedVCObj)
            add(asChildViewController: hitListVCObj)
        } else {
            remove(asChildViewController: onGoingVCObj)
            add(asChildViewController: invitationVCObj)
        }
    }
    
    @objc func centerBtnTapped() {
        remove(asChildViewController: inboxVCObj)
        if UserDefaults.standard.currentUserType() == UserType.Joinee.rawValue {
            remove(asChildViewController: hitListVCObj)
            add(asChildViewController: enjoyedVCObj)
        } else {
            remove(asChildViewController: invitationVCObj)
            add(asChildViewController: onGoingVCObj)
        }
    }
    
    @objc func rightBtnTapped() {
        if UserDefaults.standard.currentUserType() == UserType.Joinee.rawValue {
            remove(asChildViewController: enjoyedVCObj)
            remove(asChildViewController: hitListVCObj)
        } else {
            remove(asChildViewController: invitationVCObj)
            remove(asChildViewController: onGoingVCObj)
        }
        add(asChildViewController: inboxVCObj)
    }
    
    private func add(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        addChild(viewController)

        // Add Child View as Subview
        m_viewContainer.addSubview(viewController.view)
        // Configure Child View
        viewController.view.snp.makeConstraints { (make) in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        // Notify Child View Controller
        viewController.didMove(toParent: self)
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParent: nil)

        // Remove Child View From Superview
        viewController.view.removeFromSuperview()

        // Notify Child View Controller
        viewController.removeFromParent()
    }
}

extension UIDevice {
    @available(iOS 11.0, *)
    var hasNotch: Bool {
        let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        return bottom > 0
    }
}

extension UIButton {

    func centerVertically(padding: CGFloat = 5.0, contentLeftPadding:CGFloat = 0.0, contentRightPadding:CGFloat = 0.0) {
        guard
            let imageViewSize = self.imageView?.frame.size,
            let titleLabelSize = self.titleLabel?.frame.size else {
            return
        }

        let totalHeight = imageViewSize.height + titleLabelSize.height + padding

        self.imageEdgeInsets = UIEdgeInsets(
            top: -(totalHeight - imageViewSize.height),
            left: 0.0,
            bottom: 0.0,
            right: -titleLabelSize.width
        )

        self.titleEdgeInsets = UIEdgeInsets(
            top: 0.0,
            left: -imageViewSize.width,
            bottom: -(totalHeight - titleLabelSize.height),
            right: 0.0
        )

        let inset = (self.frame.size.height - totalHeight) / 2
        self.contentEdgeInsets = UIEdgeInsets(
            top: inset + 5,
            left: contentLeftPadding,
            bottom: inset,
            right: contentRightPadding
        )
    }

}
