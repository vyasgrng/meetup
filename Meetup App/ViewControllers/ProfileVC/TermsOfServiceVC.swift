//
//  TermsOfServiceVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 23/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class TermsOfServiceVC: BaseVC {

    @IBOutlet weak var m_txtView: UITextView!
    @IBOutlet weak var m_constTop: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
        
        AppSingleton.shared.startLoading()
        Alamofire.request(AppConstant.API.terms, method: .get, parameters: nil, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        let data = responseObject["data"]!.dictionaryValue
                        self.m_txtView.text = (data["terms"]?.stringValue)!
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    //MARK:- Set initial UI
    func basicSetUp() {
        addBackButton(isFirstHierarchy: false)
        addTopTitle(title: "Terms of Service")
        m_txtView.font = UIFont().RobotoRegular(size: 18.0)
        m_constTop.constant = topView.frame.height
    }
}
