//
//  EditProfileAddressVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 30/05/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import GooglePlaces

struct editProfileAddressData {
    var placeHolders:String?
    var key:String?
    var kbType:UIKeyboardType?
}

class EditProfileAddressVC: BaseVC {

    @IBOutlet weak var m_lblTitle: UILabel!
    @IBOutlet weak var m_btnNext: DefaultButton!
    @IBOutlet weak var m_constTop: NSLayoutConstraint!
    @IBOutlet weak var m_tblView: UITableView!

    var dictData:Dictionary<String,Any>?
    var structEditPro:[editProfileAddressData]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Call basic set up
        
        self.basicSetUp()
        
    }
    
    //MARK:- Set initial UI
    func basicSetUp() {
        
        DispatchQueue.main.async {
            self.m_constTop.constant = self.topView.frame.height + 20
//            self.addBackButton()
//            self.addTopTitle(title: "Edit Profile")
//            self.topView.addShadow(offset: CGSize(width: 0, height: 2), color: .lightGray, opacity: 0.8, radius: 2)
//            self.topView.backgroundColor = .white
//            self.m_btnNext.addCornerRadius(radius: 8)
//            self.m_btnNext.addBorder(width: 1.0, color: UIColor(red: 189, green: 211, blue: 229, alpha: 1.0))
           // self.m_btnNext.gradientBackground(from: UIColor(red: 93, green: 73, blue: 230, alpha: 1.0), to: UIColor(red: 40, green: 117, blue: 208, alpha: 1.0), direction: .rightToLeft)
        }
//        m_lblTitle.layoutIfNeeded()
        
        structEditPro = [
            editProfileAddressData(placeHolders: "Address",key:"address",kbType:.default),
            editProfileAddressData(placeHolders: "City",key:"city",kbType:.default),
            editProfileAddressData(placeHolders: "State",key:"state",kbType:.default),
            editProfileAddressData(placeHolders: "ZipCode",key:"zip_code",kbType:.default),
            editProfileAddressData(placeHolders: "Country",key:"country",kbType:.default),
        ]
        
        dictData!["address"] = AppSingleton.shared.userData!.address!
        dictData!["city"] = AppSingleton.shared.userData!.city!
        dictData!["state"] = AppSingleton.shared.userData!.state!
        dictData!["zip_code"] = AppSingleton.shared.userData!.zip_code!
        dictData!["country"] = AppSingleton.shared.userData!.country!
        
        
//        addBackButton()
//        topView.backgroundColor = .white
//        topView.addShadowDefault()
        self.addBackButton(isFirstHierarchy:false)
        self.addTopTitle(title: "Edit Profile")
        m_tblView.delegate = self
        m_tblView.dataSource = self
    }
    
    //MARK:- Action Next
    @IBAction func actionNext(_ sender: UIButton) {
        if dataValidation() {
            let educationDetailObj = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileOtherDetailVC") as? EditProfileOtherDetailVC
            educationDetailObj?.dictData = dictData!
            self.navigationController?.pushViewController(educationDetailObj!, animated: true)
        }
    }
    
    func dataValidation() -> Bool {
        self.view.endEditing(true)
        if (dictData!["address"] as? String)!.isEmpty {
            AppSingleton.shared.showErrorAlert(msg: "Address Required.")
            return false
        } else if (dictData!["city"] as? String)!.isEmpty {
            AppSingleton.shared.showErrorAlert(msg: "City Required.")
            return false
        } else if (dictData!["state"] as? String)!.isEmpty {
            AppSingleton.shared.showErrorAlert(msg: "State Required.")
            return false
        } else if (dictData!["zip_code"] as? String)!.isEmpty {
            AppSingleton.shared.showErrorAlert(msg: "ZipCode Required.")
            return false
        } else if (dictData!["country"] as? String)!.isEmpty {
            AppSingleton.shared.showErrorAlert(msg: "Country Required.")
            return false
        }
        return true
    }
    
    func openGooglePlacePicker() {
           let autocompleteController = GMSAutocompleteViewController()
           autocompleteController.delegate = self
           
           // Specify the place data types to return.
           let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.formattedAddress.rawValue) | UInt(GMSPlaceField.name.rawValue) | UInt(GMSPlaceField.coordinate.rawValue) |
               UInt(GMSPlaceField.placeID.rawValue))!
           autocompleteController.placeFields = fields
           
           // Specify a filter.
           let filter = GMSAutocompleteFilter()
           filter.type = .geocode
           autocompleteController.autocompleteFilter = filter
           
           // Display the autocomplete view controller.
           present(autocompleteController, animated: true, completion: nil)
           
       }
}
extension EditProfileAddressVC: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        dictData![structEditPro![textField.tag].key!] = textField.text
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 0 {
            self.openGooglePlacePicker()
            return false
        }
        return true
    }
}

extension EditProfileAddressVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (structEditPro?.count)!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SignUpCell") as? SignUpCell
//        cell?.m_viewContain.addCornerRadius(radius: (cell?.m_viewContain.frame.height)! / 5)
//        cell?.m_viewContain.addShadowDefault()
//        cell?.m_viewContain.addShadowDefault(isOnlyRightSide: true)
        cell?.m_viewContain.addShadow(offset: .zero, color: .lightGray, opacity: 0.6, radius: 2)
        cell?.m_txtFld.delegate = self
        cell?.m_txtFld.tag = indexPath.row
        cell?.m_txtFld.placeholder = structEditPro![indexPath.row].placeHolders
        cell?.m_txtFld.text = (dictData![structEditPro![indexPath.row].key!] as! String)
        return cell!
    }
}


extension EditProfileAddressVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        dismiss(animated: true, completion: nil)
        
        // 1st way
        
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            
            // Address dictionary
//            print(placeMark.addressDictionary as Any)
//            print("Place address \(String(describing: place.formattedAddress))")
//            print("Place attributions \(String(describing: place.attributions))")
            
            
            self.dictData?["address"] = placeMark.name ?? ""
            self.dictData?["city"] = placeMark.locality ?? ""
            self.dictData?["state"] = placeMark.administrativeArea ?? ""
            self.dictData?["zip_code"] = placeMark.postalCode ?? ""
            self.dictData?["country"] = placeMark.country ?? ""
            self.dictData?["lat"] = "\(place.coordinate.latitude)"
            self.dictData?["lng"] = "\(place.coordinate.latitude)"
            self.m_tblView.reloadData()
        })
        
        
        // 2nd way
        
        //                AppSingleton.shared.startLoading()
        //                let params = ["place_id":place.placeID!,"key":AppConstant.Google.mapKey]
        //
        //                Alamofire.request("https://maps.googleapis.com/maps/api/place/details/json", method: .get, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
        //                    switch response.result {
        //                    case .success:
        //                        if let value = response.result.value {
        //                            AppSingleton.shared.stopLoading()
        //                            let responseObject = JSON(value).dictionaryValue
        //                            if responseObject["status"]?.stringValue == "OK"  {
        //                                let arrAddress = responseObject["result"]!["address_components"].arrayValue
        //                                for i in arrAddress  {
        //                                    print(i)
        //                                }
        //                            }
        //                        }
        //                    case .failure(let error):
        //                        print(error)
        //                    }
        //                }
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
