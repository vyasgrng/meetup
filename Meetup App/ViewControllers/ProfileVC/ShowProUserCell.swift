//
//  ShowProUserCell.swift
//  Meetup App
//
//  Created by Birju Bhatt on 23/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class ShowProUserCell: UITableViewCell {

    @IBOutlet weak var m_btnUserImage: UIButton!
    @IBOutlet weak var m_lblName: UILabel!
    @IBOutlet weak var m_lblEmail: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
