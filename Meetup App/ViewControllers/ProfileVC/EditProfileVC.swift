//
//  EditProfileVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 26/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import DropDown
import Kingfisher
import MICountryPicker

struct editProfileData {
    var images:UIImage?
    var placeHolders:String?
    var key:String?
    var kbType:UIKeyboardType?
    var isDropDown:Bool?
}

class EditProfileVC: BaseVC {

    @IBOutlet weak var m_lblTitle: UILabel!
    @IBOutlet weak var m_btnSave: DefaultButton!
    @IBOutlet weak var m_constTop: NSLayoutConstraint!
    @IBOutlet weak var m_tblView: UITableView!
    
    var dictData:Dictionary<String,Any>?
    var structEditPro:[editProfileData]?
    let dropDown = DropDown()
    let datePicker = UIDatePicker()
    var txtFldDOB = UITextField()
    var imgViewProfile = UIImageView()
    var dialCode = ""


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Call basic set up
        
        dialCode = (AppSingleton.shared.userData?.mobile_cc)!
        
        DispatchQueue.main.async {
            self.m_constTop.constant = self.topView.frame.height + 20
            self.addBackButton(isFirstHierarchy: false)
            self.addTopTitle(title: "Edit Profile")
            self.basicSetUp()
        }
        m_lblTitle.layoutIfNeeded()
    }
    
    
    
    //MARK:- Set initial UI
    func basicSetUp() {
        
        structEditPro = [
            editProfileData(images: nil, placeHolders: "",key:"",kbType:.default,isDropDown:true),
            editProfileData(images: #imageLiteral(resourceName: "ic_person_outline_editProf_24px"), placeHolders: "First Name",key:"firstName",kbType:.default,isDropDown:true),
            editProfileData(images: #imageLiteral(resourceName: "ic_person_outline_editProf_24px"), placeHolders: "Last Name",key:"lastName",kbType:.default,isDropDown:true),
            editProfileData(images: #imageLiteral(resourceName: "ic_mail_editProf"), placeHolders: "Email ID",key:"email",kbType:.emailAddress,isDropDown:true),
            editProfileData(images: #imageLiteral(resourceName: "ic_mobile no"), placeHolders: "Mobile No",key:"mobile",kbType:.phonePad,isDropDown:true),
            editProfileData(images: #imageLiteral(resourceName: "noun_gender_973031"), placeHolders: "Gender",key:"gender",kbType:.default,isDropDown:false),
            editProfileData(images: #imageLiteral(resourceName: "noun_Birthday_2072092"), placeHolders: "DOB",key:"dob",kbType:.default,isDropDown:true),
        ]
        
        dictData = ["":"","firstName":AppSingleton.shared.userData!.first_name!,"lastName":AppSingleton.shared.userData!.last_name!,"email":AppSingleton.shared.userData!.email!,"mobile":AppSingleton.shared.userData!.mobile_no!,"gender":AppSingleton.shared.userData!.gender!,"dob":AppSingleton.shared.userData!.dob!]
        
        m_tblView.delegate = self
        m_tblView.dataSource = self
        
        DropDown.startListeningToKeyboard()
        DropDown.appearance().textColor = UIColor.black
        DropDown.appearance().selectedTextColor = UIColor.black
        DropDown.appearance().textFont = UIFont().RobotoLight(size: 14)
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
        DropDown.appearance().cellHeight = 60
    }
    
    //MARK:- Action update profile
    @IBAction func actionDone(_ sender: UIButton) {
        if dataValidation() {
            let editProAddVC = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileAddressVC") as? EditProfileAddressVC
            dictData!["mobile_cc"] = self.dialCode
            editProAddVC?.dictData = dictData
            self.navigationController?.pushViewController(editProAddVC!, animated: true)
        }

    }
    func dataValidation() -> Bool {
        self.view.endEditing(true)
        if (dictData!["firstName"] as? String)!.isEmpty {
            AppSingleton.shared.showErrorAlert(msg: "First Name Required.")
            return false
        } else if (dictData!["lastName"] as? String)!.isEmpty {
            AppSingleton.shared.showErrorAlert(msg: "Last Name Required.")
            return false
        } else if (dictData!["mobile"] as? String)!.isEmpty {
            AppSingleton.shared.showErrorAlert(msg: "Mobile Required.")
            return false
        } else if ((dictData!["mobile"] as? String)!.count < 7) || ((dictData!["mobile"] as? String)!.count > 10) {
            AppSingleton.shared.showErrorAlert(msg: "Mobile Number Invalid.")
            return false
        } else if (dictData!["gender"] as? String)!.isEmpty {
            AppSingleton.shared.showErrorAlert(msg: "Gender Required.")
            return false
        }
        return true
    }
    
    //MARK: Show Date Picker
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        // add toolbar to textField
        txtFldDOB.inputAccessoryView = toolbar
        // add datepicker to textField
        txtFldDOB.inputView = datePicker
    }
    @objc func donedatePicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        txtFldDOB.text! = formatter.string(from: datePicker.date)
        //dismiss date picker dialog
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
    }
    
    @objc func showActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func camera()
    {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.allowsEditing = true
        myPickerController.sourceType = UIImagePickerController.SourceType.camera
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    func photoLibrary()
    {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.allowsEditing = true
        myPickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    @objc func openCountryPicker(_ sender:UIButton) {
        let picker = MICountryPicker()
        picker.delegate = self
        picker.title = "Select Country Code"
        picker.showCallingCodes = true
        let navigation = UINavigationController(rootViewController: picker)
        picker.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(dismissCuntryPicker))
        self.present(navigation, animated: true)
        
    }
    
    @objc func dismissCuntryPicker() {
        dismiss(animated: true)
    }
}
extension EditProfileVC: MICountryPickerDelegate {
    func countryPicker(_ picker: MICountryPicker, didSelectCountryWithName name: String, code: String) {
        print(code)
    }
    
    func countryPicker(_ picker: MICountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        picker.popVC()
        print(dialCode)
        self.dialCode = dialCode
        m_tblView.reloadData()
    }
}
extension EditProfileVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (structEditPro?.count)!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
          let cell = tableView.dequeueReusableCell(withIdentifier: "EditProfileImageCell") as? EditProfileImageCell
            cell?.m_viewContain.addShadow(offset: .zero, color: .lightGray, opacity: 0.6, radius: 2)
            cell?.m_btnEdit.addTarget(self, action: #selector(showActionSheet), for: .touchUpInside)
            imgViewProfile = (cell?.m_imgView)!
            cell?.m_viewContain.clipsToBounds = true
            cell?.m_imgView.kf.setImage(with: URL(string: AppSingleton.shared.userData!.profile_pic!))
            return cell!
        } else  if indexPath.row == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SignUpPhoneCell", for: indexPath) as? SignUpPhoneCell
            
            cell?.m_btnCode.setTitle(dialCode, for: .normal)
            cell?.m_btnCode.addTarget(self, action: #selector(openCountryPicker(_:)), for: .touchUpInside)
            cell?.m_viewContain.addShadow(offset: .zero, color: .lightGray, opacity: 0.6, radius: 2)            
            cell?.m_txtFldMobile.placeholder = structEditPro![indexPath.row].placeHolders
            cell?.m_txtFldMobile.delegate = self
            cell?.m_txtFldMobile.tag = indexPath.row
            cell?.m_txtFldMobile.text = (dictData![structEditPro![indexPath.row].key!] as! String)
            cell?.m_txtFldMobile.keyboardType = structEditPro![indexPath.row].kbType!
            
            cell?.m_btnCode.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            cell?.m_btnCode.titleLabel?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            cell?.m_btnCode.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            
            return cell!
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SignUpCell") as? SignUpCell
//            cell?.m_viewContain.addShadowDefault(isOnlyRightSide: true)
            cell?.m_viewContain.addShadow(offset: .zero, color: .lightGray, opacity: 0.6, radius: 2)


            cell?.m_imgView.image = structEditPro![indexPath.row].images
            cell?.m_txtFld.delegate = self
            cell?.m_txtFld.tag = indexPath.row
            cell?.m_txtFld.placeholder = structEditPro![indexPath.row].placeHolders
            cell?.m_txtFld.text = (dictData![structEditPro![indexPath.row].key!] as! String)
            cell?.m_imgDropdown.isHidden = structEditPro![indexPath.row].isDropDown!
            
            if indexPath.row == 5 {
                dropDown.anchorView = cell?.m_txtFld
                dropDown.dataSource = ["Male", "Female"]
                dropDown.width = cell?.m_viewContain.frame.width
                dropDown.bottomOffset = CGPoint(x: -((cell?.m_viewContain.frame.origin.x)!), y:(cell?.m_viewContain.frame.height)!)
                dropDown.selectionAction = { (index:Int, item:String) in
                    cell?.m_txtFld.text = item
                    self.dictData![self.structEditPro![indexPath.row].key!] = "\(index)"
                }
            } else if indexPath.row == 6 {
                txtFldDOB = (cell?.m_txtFld)!
            }
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            showActionSheet()
        }
    }
}

extension EditProfileVC: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        dictData![structEditPro![textField.tag].key!] = textField.text
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 5 {
            dropDown.show()
            return false
        } else if textField.tag == 6 {
            showDatePicker()
        } else if textField.tag == 3 {
            return false
        }
        return true
    }
}

extension EditProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            imgViewProfile.contentMode = .scaleToFill
            imgViewProfile.image = pickedImage
            dictData!["profile_pic"] = pickedImage
        }
        self.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}
