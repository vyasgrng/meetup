//
//  AddCardVCVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 23/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol ProtocolPaymentMethodVC{
    func refreshDetails()
}


class AddCardVC: BaseVC {

    @IBOutlet weak var m_constTop: NSLayoutConstraint!
    @IBOutlet weak var m_viewCardNumber: UIView!
    @IBOutlet weak var m_viewHolderName: UIView!
    @IBOutlet weak var m_viewMonthYear: UIView!
    @IBOutlet weak var m_viewCVV: UIView!
    @IBOutlet weak var m_btnSave: DefaultButton!
    @IBOutlet weak var m_txtHolderName: UITextField!
    @IBOutlet weak var m_txtCardNumber: UITextField!
    @IBOutlet weak var m_txtExpieryDate: UITextField!
    @IBOutlet weak var m_txtCardCVV: UITextField!
    
    
    var delegate : ProtocolPaymentMethodVC?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
    }
    
    //MARK:- Set initial UI
    func basicSetUp() {
        m_constTop.constant = topView.frame.height + 5
        m_txtExpieryDate.delegate = self
        addBackButton(isFirstHierarchy:false)
        addTopTitle(title: "Add New Card")
    }
    
    func setPropertyOfElement(view:UIView) {
        let radius = view.frame.height / 5
        view.addBorder(width: 1.0, color: UIColor(red: 189, green: 211, blue: 229, alpha: 1.0))
        view.addCornerRadius(radius: radius)
        
    }
   
    @IBAction func btnSaveCardDetails(_ sender: DefaultButton) {
        let expieryDetails = m_txtExpieryDate.text?.components(separatedBy: "/")

        if m_txtHolderName.text?.isEmpty ?? false{
            AppSingleton.shared.showErrorAlert(msg: "Card Holder Name can not Blank.")
            return
        }else if m_txtCardNumber.text!.count != 12  {
            AppSingleton.shared.showErrorAlert(msg: "Card Number Invalid.")
            return
        }else if m_txtCardCVV.text?.isEmpty ?? false{
            AppSingleton.shared.showErrorAlert(msg: "Card CVC can not Blank.")
            return
        }else if !(m_txtExpieryDate.text?.contains("/") ?? true){
            AppSingleton.shared.showErrorAlert(msg: "Card Expiery date is not in correct formate.")
            return
        }else if expieryDetails?.count != 2 {
            AppSingleton.shared.showErrorAlert(msg: "Card Expiery date is not in correct formate.")
            return
        }else if expieryDetails![0].count != 2 || expieryDetails![1].count != 4{
            AppSingleton.shared.showErrorAlert(msg: "Card Expiery date is not in correct formate.")
            return
        }
        saveCardDetails(expieryMonth: expieryDetails?.first?.replacingOccurrences(of: "/", with: "") ?? "", ExpeiryYear: expieryDetails?[1] ?? "")
    }
    
    //MARK: //call api and save details to server
    func saveCardDetails(expieryMonth:String,ExpeiryYear:String){
        AppSingleton.shared.startLoading()
        
        let params = ["token":UserDefaults.standard.currentToken()!,"name":m_txtHolderName.text! ,"card_no":m_txtCardNumber.text!,"cvc":m_txtCardCVV.text!,"exp_month":expieryMonth,"exp_year":ExpeiryYear]
        
        Alamofire.request(AppConstant.API.addCard, method: .post, parameters: params as Parameters,encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        self.delegate?.refreshDetails()
                        AppSingleton.shared.showSuccessAlert(msg: "Card details added successfully.")
                        self.popVC()
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}

//MARK: UITextField delegate
extension AddCardVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard textField == m_viewMonthYear else {
            return true
        }
        
        if !string.isEmpty {
            
            if range.location == 7 {
                return false
            }
            guard let text = textField.text else { return true }
            textField.text = text.applyPatternOnNumbers(pattern: "##/####", filler: "/")
        }
        return true
    }
}

extension String {
    func applyPatternOnNumbers(pattern: String, filler: Character) -> String {
        var pureNumber = self.replacingOccurrences( of: "[^0-9]", with: "", options: .regularExpression)
        // Insert filler for all occurances of filler in pattern
        for index in pattern.indices {
//            print(pattern[index])
            let patternChar = pattern[index]
            if patternChar == filler {
                if index <= pureNumber.endIndex {
                    pureNumber.insert(filler, at: index)
                }
            }
        }
        return pureNumber
    }
}
