//
//  BlockedUsersVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 24/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import SwiftyJSON

class BlockedUsersVC: BaseVC {

    @IBOutlet weak var m_constTop: NSLayoutConstraint!
    @IBOutlet weak var m_tblView: UITableView!
    var topTitle:String?
    var rightBtnTitle:String?
    var rightBtnColor:UIColor?
    var arrBlockedList:Array<JSON> = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
        blockedList()
        m_tblView.tableFooterView = UIView()
    }
    

    //MARK:- Set initial UI
    func basicSetUp() {
        addBackButton()
        addTopTitle(title: topTitle!)
        m_constTop.constant = topView.frame.height + 15
    }
    
    //MARK:- Get blocked user
    func blockedList() {
        
        let jsonStr = AppSingleton.shared.dictToJSONString(dict: [:])
        SocketIOManager.sharedInstance.manager.defaultSocket.emitWithAck("block_list", with: [jsonStr]).timingOut(after: 0) { (data) in
            let response = JSON(data)
            self.arrBlockedList = response["data"].arrayValue
            self.m_tblView.reloadData()
        }
    }
}

extension BlockedUsersVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrBlockedList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BlockedUsersCell") as? BlockedUsersCell
        let data = self.arrBlockedList[indexPath.row].dictionaryValue
        cell?.m_btnUnblock.setTitleColor(rightBtnColor, for: .normal)
        cell?.m_btnUnblock.setTitle(rightBtnTitle, for: .normal)
        cell?.m_lblName.text = (data["first_name"]?.stringValue)! + " " + (data["last_name"]?.stringValue)!
        cell?.m_imgUser.kf.setImage(with: (data["profile_pic"]?.url)!)
        return cell!
    }
}
