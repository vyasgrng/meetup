//
//  ReferralCodeVC.swift
//  Meetup App
//
//  Created by Aditi Pancholi on 09/08/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class ReferralCodeVC: BaseVC {

    @IBOutlet weak var m_btnDone: DefaultButton!
    @IBOutlet weak var m_txtReferalCode: UITextField!
    @IBOutlet weak var m_constTop: NSLayoutConstraint!
    @IBOutlet weak var m_viewTxtCodeWrapper: UIView!
    @IBOutlet weak var m_lblShareTitle: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
    }
    
    //MARK:- Set initial UI
    func basicSetUp() {
        addBackButton(isFirstHierarchy: false)
        addTopTitle(title: "Referral Code")
        m_constTop.constant = topView.frame.height
        setPropertyOfElement(view: m_viewTxtCodeWrapper)
    }
    @IBAction func btnVerifyReferralCodeAction(_ sender: Any) {
        if m_txtReferalCode.text!.isEmpty {
            AppSingleton.shared.showErrorAlert(msg: "Referral code Invalid.")
            return
        }
        applyInviteCode()
    }
    
    func setPropertyOfElement(view:UIView) {
        view.addShadow(offset: .zero, color: .lightGray, opacity: 0.6, radius: 2)
    }
    
    //MARK: Server interaction
    func applyInviteCode(){
        //token,code
        AppSingleton.shared.startLoading()
        let params = ["token":UserDefaults.standard.currentToken()!,"code" : m_txtReferalCode.text!]
        Alamofire.request(AppConstant.API.redeemInviteCode, method: .post, parameters: params as Parameters,encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                        self.m_txtReferalCode.text = ""
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                        self.m_txtReferalCode.text = ""
                    } else {
                        let referralDetails = responseObject["data"]!.dictionaryValue
                        print("referral amount \(referralDetails["amount"]!.stringValue)")
                        UserDefaults.standard.setReferralCurrency(currency: referralDetails["amount"]!.stringValue)
                        AppSingleton.shared.showSuccessAlert(msg: " $ \(referralDetails["amount"]!.stringValue) added.")
                        self.m_txtReferalCode.text = ""
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
   

}
