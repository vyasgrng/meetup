//
//  ChangePasswordVC.swift
//  Meetup App
//
//  Created by Aditi Pancholi on 09/08/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import Kingfisher
import GoogleSignIn
import FBSDKLoginKit


class ChangePasswordVC: BaseVC {

    @IBOutlet weak var m_viewConfirmPassword: UIView!
    @IBOutlet weak var m_viewNewPasswordWrapper: UIView!
    @IBOutlet weak var m_viewOldPsswordWrapper: UIView!
    @IBOutlet weak var m_txtNewPassword: UITextView!
    @IBOutlet weak var m_txtOldPassword: UITextView!
    @IBOutlet weak var m_txtConfirmPassword: UITextField!
    @IBOutlet weak var m_constTop: NSLayoutConstraint!
    
    @IBOutlet weak var m_btnChangePwd: DefaultButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
    }
    
    //MARK:- Set initial UI
    func basicSetUp() {
        m_constTop.constant = topView.frame.height + 20
        addBackButton(isFirstHierarchy: false)
        addTopTitle(title: "Change Password")
        self.setPropertyOfElement(view: m_viewOldPsswordWrapper)
        self.setPropertyOfElement(view: m_viewNewPasswordWrapper)
        self.setPropertyOfElement(view: m_viewConfirmPassword)
        m_txtOldPassword.font = UIFont().RobotoRegular(size: 16.0)
        m_txtNewPassword.font = UIFont().RobotoRegular(size: 16.0)
        m_txtConfirmPassword.font = UIFont().RobotoRegular(size: 16.0)
        m_btnChangePwd.titleLabel?.font = UIFont().RobotoMedium(size: 16.0)
        m_btnChangePwd.removeCorners()
    }
    
    func setPropertyOfElement(view:UIView) {
//        let radius = view.frame.height / 5
//        view.addBorder(width: 1.0, color: UIColor(red: 189, green: 211, blue: 229, alpha: 1.0))
        view.addCornerRadius(radius: 3)
        view.addShadow(offset: CGSize(width: 0, height: 2), color: .lightGray, opacity: 0.6, radius: 2)
        
    }

    //MARK: Change password
    @IBAction func btnChangePasswordAction(_ sender: Any) {
        if m_txtOldPassword.text!.isEmpty{
            AppSingleton.shared.showErrorAlert(msg: "Old password required.")
        }else if m_txtNewPassword.text!.isEmpty{
            AppSingleton.shared.showErrorAlert(msg: "New password required.")
        }else if m_txtNewPassword.text!.count < 6 || m_txtNewPassword.text!.count > 12{
            AppSingleton.shared.showErrorAlert(msg: "Password must be between 6 to 12 characters.")
            m_txtNewPassword.text = ""
            m_txtConfirmPassword.text = ""
        }else if m_txtNewPassword.text! != m_txtConfirmPassword.text! {
            AppSingleton.shared.showErrorAlert(msg: "Confirm password dose not match with Password.")
            m_txtNewPassword.text = ""
            m_txtConfirmPassword.text = ""
        }else{
            changePassword()
        }
    }
    
    //MARK: Server communication
    func changePassword(){
        AppSingleton.shared.startLoading()
        let params = ["token":UserDefaults.standard.currentToken()!,"old_password" : m_txtOldPassword.text!, "new_password":m_txtNewPassword.text!]
        Alamofire.request(AppConstant.API.changePassword, method: .post, parameters: params as Parameters,encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                        self.m_txtOldPassword.text = ""
                        self.m_txtNewPassword.text = ""

                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                        self.m_txtOldPassword.text = ""
                        self.m_txtNewPassword.text = ""
                    } else {
                        AppSingleton.shared.showSuccessAlert(msg: "Please login again.")
                        self.logoutUser()
                        self.m_txtOldPassword.text = ""
                        self.m_txtNewPassword.text = ""
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    func logoutUser(){
        AppSingleton.shared.startLoading()
        let params = ["token":UserDefaults.standard.currentToken()!]
        Alamofire.request(AppConstant.API.logOut, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        
                        GIDSignIn.sharedInstance()?.signOut()
                        LoginManager().logOut()
                        
                        //disconnect socket
                        SocketIOManager.sharedInstance.disconnectSocket()
                        //
                        let domain = Bundle.main.bundleIdentifier!
                        UserDefaults.standard.removePersistentDomain(forName: domain)
                        UserDefaults.standard.synchronize()
                        var isVCFound = Bool()
                        if self.navigationController?.viewControllers.count != nil {
                            for vcs in (self.navigationController?.viewControllers)! {
                                if vcs is WelcomeVC {
                                    isVCFound = true
                                    self.navigationController?.popToViewController(vcs, animated: true)
                                }
                            }
                        }
                        
                        if !isVCFound {
                            let vcObj = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WelcomeVC") as? WelcomeVC
                            let navi = UINavigationController(rootViewController: vcObj!)
                            navi.navigationBar.isHidden = true
                            appDelegate.window?.rootViewController = navi
                            appDelegate.window?.makeKeyAndVisible()
                        }
                        UserDefaults.standard.removeToken()
                        UserDefaults.standard.setUserType(type: UserType.Joinee.rawValue)
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}
