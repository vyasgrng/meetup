//
//  VerifyDocumentVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 16/05/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class VerifyDocumentVC: BaseVC {

    let captions = ["Driver's license","Passport","Identity card"]
    var arrDocsData:Array<Any> = []

    @IBOutlet weak var m_lblSubtitle: UILabel!
    @IBOutlet weak var m_lblTitle: UILabel!
    @IBOutlet weak var m_tblView: UITableView!
    @IBOutlet weak var m_constTop: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
    }

    //MARK:- Set initial UI
    func basicSetUp() {
        m_constTop.constant = topView.frame.height + 30
        addBackButton()
        addTopTitle(title: "Verify Document")
        m_tblView.tableFooterView = UIView()
        m_lblTitle.font = UIFont().RobotoMedium(size: 25.0)
        m_lblSubtitle.font = UIFont().RobotoRegular(size: 15.0)
    }
}

extension VerifyDocumentVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VerificationNormalCell") as? VerificationNormalCell
        cell?.m_lblCaption.text = captions[indexPath.row]
        cell?.m_lblCaption.font = UIFont().RobotoMedium(size: 20.0)
        cell?.m_viewContain.addShadow(offset: .zero, color: .lightGray, opacity: 0.6, radius: 4)
        cell?.m_btnAdd.removeCorners()
        cell?.m_btnAdd.titleLabel?.font = UIFont().RobotoMedium(size: 20.0)
        cell?.m_btnAdd.addTarget(self, action: #selector(showActionSheet), for: .touchUpInside)
        return cell!
    }
}

//MARK: Select and upload document
extension VerifyDocumentVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    @objc func showActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func camera()
    {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.allowsEditing = true
        myPickerController.sourceType = UIImagePickerController.SourceType.camera
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    func photoLibrary()
    {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.allowsEditing = true
        myPickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            arrDocsData.append(pickedImage)
            self.uploadDocument()
        }
        self.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Upload document
    func uploadDocument() {
        let params:Dictionary<String,String> = ["token":UserDefaults.standard.currentToken()!]
        
        AppSingleton.shared.startLoading()
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            let img = self.arrDocsData[self.arrDocsData.count - 1] as? UIImage
            multipartFormData.append((img!.jpegData(compressionQuality: 0.5))!, withName: "documents", fileName: "Document.jpeg", mimeType: "image/jpeg")
            
            for (key, value) in params {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to:AppConstant.API.uploadDocument)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                })
                
                upload.responseJSON { response in
                    //print response.result
                    print(response)
                    AppSingleton.shared.stopLoading()
                    if let value = response.result.value {
                        let responseObject = JSON(value).dictionaryValue
                        if responseObject["status"]?.intValue == 0 {
                            AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                        } else if responseObject["status"]?.intValue == 3 {
                            AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                        } else {
                            UserDefaults.standard.storeToken(token: responseObject["data"]!["token"].stringValue)
                            UserDefaults.standard.set(encodable: UserModal(data: JSON(value)), forKey: "userData")
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }
                
            case .failure( _):
                break
                //print encodingError.description
            }
        }
    }
}
