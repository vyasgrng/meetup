//
//  CurrencyVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 22/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


protocol ProtocolCurrencyVC{
    func refreshDetails()
}

class CurrencyVC: BaseVC {

    var arrCurrencies:Array<JSON> = []
    
    @IBOutlet weak var m_tblView: UITableView!
    @IBOutlet weak var m_constTblTop: NSLayoutConstraint!
    
    var delegate : ProtocolCurrencyVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
    }
    
    //MARK:- Set initial UI
    func basicSetUp() {
        addBackButton(isFirstHierarchy: false)
        addTopTitle(title: "Currency")
        m_constTblTop.constant = topView.frame.height + 10
        m_tblView.tableFooterView = UIView()
        
        //Get currency
        getCurrencies()
    }
        
    //MARK:- Get currencies
    func getCurrencies() {
        AppSingleton.shared.startLoading()
        Alamofire.request(AppConstant.API.getCurrency, method: .get, parameters: nil, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        self.arrCurrencies = responseObject["data"]!["currencies"].arrayValue
                        self.m_tblView.reloadData()
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    //MARK:- Set currency
    func setCurrency(currencyID:String) {
        AppSingleton.shared.startLoading()
        let params = ["token":UserDefaults.standard.currentToken()!,"currency":currencyID]
        Alamofire.request(AppConstant.API.setCurrency, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        UserDefaults.standard.storeToken(token: responseObject["data"]!["token"].stringValue)
                        UserDefaults.standard.set(encodable: UserModal(data: JSON(value)), forKey: "userData")
                        self.m_tblView.reloadData()
                        //developer 2
                    UserDefaults.standard.setDefaultCurrency(currency: responseObject["data"]!["currency_readable"].stringValue)
                       self.delegate.refreshDetails()
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}

extension CurrencyVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCurrencies.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CurrencyCell") as? CurrencyCell

        cell?.m_lblCurrencyName?.text = self.arrCurrencies[indexPath.row]["name"].stringValue
        cell?.m_viewBg.addCornerRadius(radius: 3.0)
        cell?.m_viewBg.addShadow(offset: CGSize(width: 1, height: 2), color: .lightGray, opacity: 0.6, radius: 3)
        
        if AppSingleton.shared.userData?.currency == self.arrCurrencies[indexPath.row]["id"].stringValue {
            cell?.m_btnDefault.isHidden = false
            cell?.m_btnMakeDefault.isHidden = true
        } else {
            cell?.m_btnDefault.isHidden = true
            cell?.m_btnMakeDefault.isHidden = false
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        setCurrency(currencyID: arrCurrencies[indexPath.row]["id"].stringValue)
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
