//
//  HelpVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 23/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class HelpVC: BaseVC {

    @IBOutlet weak var m_constTop: NSLayoutConstraint!
    @IBOutlet weak var m_viewContactUs: UIView!
    @IBOutlet weak var m_viewEmailUs: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
    }
    
    //MARK:- Set initial UI
    func basicSetUp() {
        addBackButton()
        addTopTitle(title: "Help")
        m_constTop.constant = topView.frame.height + 20
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(goToContactUs))
        tapGesture.numberOfTapsRequired = 1
        m_viewContactUs.addGestureRecognizer(tapGesture)
        
        m_viewContactUs.addShadow(offset: CGSize(width: 2, height: 2), color: .lightGray, opacity: 0.6, radius: 2)
        m_viewEmailUs.addShadow(offset: CGSize(width: 2, height: 2), color: .lightGray, opacity: 0.6, radius: 2)
        
        

        
    }
    
    //MARK:- Action goto contact us screen
    @objc func goToContactUs() {
        let vcObj = self.storyboard?.instantiateViewController(withIdentifier: "ContactUSVC") as? ContactUSVC
        vcObj?.topTitle = "Contact US"
        self.navigationController?.pushViewController(vcObj!, animated: true)
    }
}
