//
//  ProfileVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 18/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher
import GoogleSignIn
import FBSDKLoginKit

class ProfileVC: BaseVC {

    var captions:Array<String>?
    var imageName:Array<String>?
    var imgViewProfile = UIImageView()
    
    @IBOutlet weak var m_tblViewProfile: UITableView!
    
    @IBOutlet weak var m_constTblTop: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //MARK:- Set initial UI
    func basicSetUp() {
        addTopTitle(title: "Profile")
        m_constTblTop.constant = topView.frame.height + 15
        m_tblViewProfile.tableFooterView = UIView()
        if UserDefaults.standard.currentUserType() == UserType.Host.rawValue {
            captions = ["","","Revenue (530$)","Blocked Users","Invite Friends","Verification","Account Settings","Switch to Joining","Help","Feedback","Logout"]
            imageName = ["","","revenue","blocked users","invite friends","verification","Settings","switch","help","feedback","Logout"]

        } else {
            captions = ["","Invite Friends","Verification","Account Settings","Switch to Host","Help","Feedback","Logout"]
            imageName = ["","invite friends","verification","Settings","switch","help","feedback","Logout"]
        }
        
        self.m_tblViewProfile.delegate = self
        self.m_tblViewProfile.dataSource = self
        
    }
    
    func showLogoutAlert() {
        
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Meet Up", message: "You can always access your content by signing back in",preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { _ in
                //Cancel Action
            }))
            alert.addAction(UIAlertAction(title: "Log out",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            //Sign out action
                                            
                    AppSingleton.shared.startLoading()
                    let params = ["token":UserDefaults.standard.currentToken()!]
                    Alamofire.request(AppConstant.API.logOut, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
                        switch response.result {
                        case .success:
                            if let value = response.result.value {
                                AppSingleton.shared.stopLoading()
                                let responseObject = JSON(value).dictionaryValue
                                if responseObject["status"]?.intValue == 0 {
                                    AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                                } else if responseObject["status"]?.intValue == 3 {
                                    AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                                } else {
                                    
                                    //developer 2
                                    SocketIOManager.sharedInstance.disconnectSocket()
                                    
                                    GIDSignIn.sharedInstance()?.signOut()
                                    LoginManager().logOut()
                                    let domain = Bundle.main.bundleIdentifier!
                                    UserDefaults.standard.removePersistentDomain(forName: domain)
                                    UserDefaults.standard.synchronize()
                                    var isVCFound = Bool()
                                    if self.navigationController?.viewControllers.count != nil {
                                        for vcs in (self.navigationController?.viewControllers)! {
                                            if vcs is WelcomeVC {
                                                isVCFound = true
                                                self.navigationController?.popToViewController(vcs, animated: true)
                                            }
                                        }
                                    }
                                    
                                    if !isVCFound {
                                        let vcObj = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WelcomeVC") as? WelcomeVC
                                        let navi = UINavigationController(rootViewController: vcObj!)
                                        navi.navigationBar.isHidden = true
                                        appDelegate.window?.rootViewController = navi
                                        appDelegate.window?.makeKeyAndVisible()
                                    }
                                    UserDefaults.standard.removeToken()
                                    UserDefaults.standard.setUserType(type: UserType.Joinee.rawValue)
                                }
                            }
                        case .failure(let error):
                            print(error)
                        }
                    }
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- Edit pic api call
    @objc func showActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func camera()
    {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.allowsEditing = true
        myPickerController.sourceType = UIImagePickerController.SourceType.camera
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    func photoLibrary()
    {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.allowsEditing = true
        myPickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    func editProfileApi() {
        
        
        let params:Dictionary<String,String> = ["token":UserDefaults.standard.currentToken()!,
                                                "first_name":(AppSingleton.shared.userData?.first_name)!,
                                                "last_name":(AppSingleton.shared.userData?.last_name)!,
                                                "mobile_no":(AppSingleton.shared.userData?.mobile_no)!,
                                                "gender":AppSingleton.shared.getGender(gender:(AppSingleton.shared.userData?.gender)!),
                                                "dob":(AppSingleton.shared.userData?.dob)!,
                                                "school":(AppSingleton.shared.userData?.school)!,
                                                "university":(AppSingleton.shared.userData?.university)!,
                                                "work":(AppSingleton.shared.userData?.work)!,
                                                "hobby":(AppSingleton.shared.userData?.hobby)!,
                                                "language":(AppSingleton.shared.userData?.language)!,
                                                "mobile_cc":(AppSingleton.shared.userData?.mobile_cc)!,
                                                "address":(AppSingleton.shared.userData?.address)!,
                                                "city":(AppSingleton.shared.userData?.city)!,
                                                "state":(AppSingleton.shared.userData?.state)!,
                                                "zip_code":(AppSingleton.shared.userData?.zip_code)!,
                                                "country":(AppSingleton.shared.userData?.country)!]
        
        let imgProfile = imgViewProfile.image
        
        AppSingleton.shared.startLoading()
        
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                multipartFormData.append((imgProfile!.jpegData(compressionQuality: 0.5))!, withName: "profile_pic", fileName: "Profile.jpeg", mimeType: "image/jpeg")
                
                for (key, value) in params {
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
            }, to:AppConstant.API.updateProfile)
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        //Print progress
                    })
                    
                    upload.responseJSON { response in
                        //print response.result
                        print(response)
                        AppSingleton.shared.stopLoading()
                        if let value = response.result.value {
                            let responseObject = JSON(value).dictionaryValue
                            if responseObject["status"]?.intValue == 0 {
                                AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                            } else if responseObject["status"]?.intValue == 3 {
                                AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                            } else {
                                UserDefaults.standard.set(encodable: UserModal(data: JSON(value)), forKey: "userData")
                                self.m_tblViewProfile.reloadData()
                            }
                        }
                    }
                    
                case .failure( _):
                    break
                    //print encodingError.description
                }
            }
    }
}

extension ProfileVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return captions!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell1") as? ProfileCell1
            let radius = cell!.m_imgViewProfile.frame.height / 5
            cell!.m_imgViewProfile.addCornerRadius(radius:radius)
            cell!.m_btnEditPic.addTarget(self, action: #selector(showActionSheet), for: .touchUpInside)
            cell!.m_lblUserName.text = (AppSingleton.shared.userData!.first_name)! + " " + (AppSingleton.shared.userData!.last_name)!
            cell!.m_lblUserEmail.text = AppSingleton.shared.userData!.email
            cell?.m_imgViewProfile.kf.setImage(with: URL(string: (AppSingleton.shared.userData!.profile_pic)!), for: .normal)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
                cell?.m_imgViewProfile.addCornerRadius(radius: 5.0)
            }
            
            if UserDefaults.standard.currentUserType() == UserType.Host.rawValue {
                cell!.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: UIScreen.main.bounds.width)
            }
            return cell!
        } else {
            if UserDefaults.standard.currentUserType() == UserType.Host.rawValue {
                if indexPath.row == 1 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell2") as? ProfileCell2
                    return cell!
                }
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell") as? ProfileCell
            cell?.m_lblCaption.text = captions![indexPath.row]
            cell?.m_imgView.image = UIImage(named: imageName![indexPath.row])
            cell?.m_lblCaption.textColor = .black
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch captions![indexPath.row] {
        case "Invite Friends":
            let vcObj = UIStoryboard(name: "InnerProfileSection", bundle: Bundle.main).instantiateViewController(withIdentifier: "InviteFriendVC") as? InviteFriendVC
            //developer2
            //self.navigationController?.pushViewController(vcObj!, animated: true)
            self.navigationController?.present(vcObj!, animated: true, completion: nil)
            break
        case "Verification":
            let vcObj = UIStoryboard(name: "InnerProfileSection", bundle: Bundle.main).instantiateViewController(withIdentifier: "VerificationVC") as? VerificationVC
            self.navigationController?.pushViewController(vcObj!, animated: true)
            break
        case "Account Settings":
            let vcObj = UIStoryboard(name: "InnerProfileSection", bundle: Bundle.main).instantiateViewController(withIdentifier: "AccountSettingVC") as? AccountSettingVC
            self.navigationController?.pushViewController(vcObj!, animated: true)            
            break
        case "Switch to Joining","Switch to Host":
            if UserDefaults.standard.currentUserType() == UserType.Joinee.rawValue {
                UserDefaults.standard.setUserType(type: UserType.Host.rawValue)
            } else {
                UserDefaults.standard.setUserType(type: UserType.Joinee.rawValue)
            }
            let navi = UINavigationController(rootViewController: TabbarVC())
            navi.navigationBar.isHidden = true
            appDelegate.window?.rootViewController = navi
            appDelegate.window?.makeKeyAndVisible()
            
            break
        case "Help":
            let vcObj = UIStoryboard(name: "InnerProfileSection", bundle: Bundle.main).instantiateViewController(withIdentifier: "HelpVC") as? HelpVC
            self.navigationController?.pushViewController(vcObj!, animated: true)
            break
        case "Feedback":
            let vcObj = UIStoryboard(name: "InnerProfileSection", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactUSVC") as? ContactUSVC
            vcObj?.topTitle = "Feedback"
            self.navigationController?.pushViewController(vcObj!, animated: true)
            break
        case "Logout":
            showLogoutAlert()
            break
        case "Blocked Users":
            let vcObj = UIStoryboard(name: "InnerProfileSection", bundle: Bundle.main).instantiateViewController(withIdentifier: "BlockedUsersVC") as? BlockedUsersVC
            vcObj?.topTitle = "Blocked Users"
            vcObj?.rightBtnTitle = "Unblock"
            vcObj?.rightBtnColor = UIColor().convertHexStringToColor(hexString: "455CE4")
            self.navigationController?.pushViewController(vcObj!, animated: true)
            break
        case "Revenue (530$)":
            break
        default:
            let vcObj = UIStoryboard(name: "InnerProfileSection", bundle: Bundle.main).instantiateViewController(withIdentifier: "ShowProfileVC") as? ShowProfileVC
            self.navigationController?.pushViewController(vcObj!, animated: true)
            break
        }
    }
}

extension ProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            imgViewProfile.contentMode = .scaleToFill
            imgViewProfile.image = pickedImage
        }
        self.dismiss(animated: true, completion: nil)
        editProfileApi()
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}
