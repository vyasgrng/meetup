//
//  AccountSettingVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 22/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AccountSettingVC: BaseVC {

    @IBOutlet weak var m_tblView: UITableView!
    @IBOutlet weak var m_constTblTop: NSLayoutConstraint!
    var captions = ["Notifications","Currency","Payment Methods","Change Password","Referral code","Terms of Service"]
    var dictData = ["chat":"","reminder":"","event":"","support":""]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
    }
    
    //MARK:- Set initial UI
    func basicSetUp() {
        addBackButton()
        addTopTitle(title: "Account Settings")
        m_constTblTop.constant = topView.frame.height
        m_tblView.tableFooterView = UIView()
    }
    
    @objc func setNotification(_ customSwitch:UISwitch) {
        
        switch customSwitch.tag {
        case 30:
            dictData["chat"] = "\(customSwitch.isOn)"
            break
        case 31:
            dictData["event"] = "\(customSwitch.isOn)"
            break
        case 32:
            dictData["support"] = "\(customSwitch.isOn)"
            break
        case 33:
            dictData["reminder"] = "\(customSwitch.isOn)"
            break

        default:
            break
        }
        
        dictData["token"] = UserDefaults.standard.currentToken()!
        setNotiApi(params: dictData)
    }
    
    func setNotiApi(params:[String:String]) {
        AppSingleton.shared.startLoading()
        Alamofire.request(AppConstant.API.setNotification, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        UserDefaults.standard.storeToken(token: responseObject["data"]!["token"].stringValue)
                        UserDefaults.standard.set(encodable: UserModal(data: JSON(value)), forKey: "userData")
                        self.m_tblView.reloadData()
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }

}

extension AccountSettingVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AccSettingCell") as? AccSettingCell
            cell?.m_switchChat.isOn = (AppSingleton.shared.userData!.notification_chat?.boolValue)!
            cell?.m_switchChat.tag = 30
            dictData["chat"] = "\(cell?.m_switchChat.isOn ?? false)"
            cell?.m_switchChat.addTarget(self, action: #selector(setNotification(_:)), for: .valueChanged)
            
            cell?.m_switchEvent.isOn = (AppSingleton.shared.userData!.notification_event?.boolValue)!
            cell?.m_switchEvent.tag = 31
            dictData["event"] = "\(cell?.m_switchEvent.isOn ?? false)"
            cell?.m_switchEvent.addTarget(self, action: #selector(setNotification(_:)), for: .valueChanged)
            
            cell?.m_switchSupport.isOn = (AppSingleton.shared.userData!.notification_support?.boolValue)!
            cell?.m_switchSupport.tag = 32
            dictData["support"] = "\(cell?.m_switchSupport.isOn ?? false)"
            cell?.m_switchSupport.addTarget(self, action: #selector(setNotification(_:)), for: .valueChanged)
            
            cell?.m_switchReminder.isOn = (AppSingleton.shared.userData!.notification_reminder?.boolValue)!
            cell?.m_switchReminder.tag = 33
            dictData["reminder"] = "\(cell?.m_switchReminder.isOn ?? false)"
            cell?.m_switchReminder.addTarget(self, action: #selector(setNotification(_:)), for: .valueChanged)
            
            cell?.m_viewBg.addCornerRadius(radius: 3.0)
            cell?.m_viewBg.addShadow(offset: CGSize(width: 1, height: 2), color: .lightGray, opacity: 0.6, radius: 3)

            return cell!
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell") as? ProfileCell
            cell?.m_lblCaption.text = captions[indexPath.row]
            cell?.m_btnArrow.setImage(#imageLiteral(resourceName: "right_arrow").maskWithColor(color: UIColor().convertHexStringToColor(hexString: "202564")), for: .normal)
            cell?.m_lblSubTitle.text = captions[indexPath.row] == "Currency" ? "( " + UserDefaults.standard.getDefaultCurrency() + " ) " : ""
            
            cell?.m_viewBg.addCornerRadius(radius: 3.0)
            cell?.m_viewBg.addShadow(offset: CGSize(width: 1, height: 2), color: .lightGray, opacity: 0.6, radius: 3)

            cell?.layoutIfNeeded()
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 1:
            let vcObj = UIStoryboard(name: "InnerProfileSection", bundle: Bundle.main).instantiateViewController(withIdentifier: "CurrencyVC") as? CurrencyVC
            vcObj?.delegate = self
            self.navigationController?.pushViewController(vcObj!, animated: true)
            break
        case 2:
//            let vcObj = UIStoryboard(name: "InnerProfileSection", bundle: Bundle.main).instantiateViewController(withIdentifier: "PaymentListVC") as? PaymentListVC
            let vcObj = UIStoryboard(name: "InnerProfileSection", bundle: Bundle.main).instantiateViewController(withIdentifier: "PaymentMethodsVC") as? PaymentMethodsVC
            self.navigationController?.pushViewController(vcObj!, animated: true)
            break
        case 3:
            let vcObj = UIStoryboard(name: "InnerProfileSection", bundle: Bundle.main).instantiateViewController(withIdentifier: "ChangePasswordVC") as? ChangePasswordVC
            self.navigationController?.pushViewController(vcObj!, animated: true)
            break
        case 4:
            let vcObj = UIStoryboard(name: "InnerProfileSection", bundle: Bundle.main).instantiateViewController(withIdentifier: "ReferralCodeVC") as? ReferralCodeVC
            self.navigationController?.pushViewController(vcObj!, animated: true)
            break
        case 5:
            let vcObj = UIStoryboard(name: "InnerProfileSection", bundle: Bundle.main).instantiateViewController(withIdentifier: "TermsOfServiceVC") as? TermsOfServiceVC
            self.navigationController?.pushViewController(vcObj!, animated: true)
            break
        default:
            break
        }
    }
}


//MARK: Currency delegate
extension AccountSettingVC : ProtocolCurrencyVC{
    func refreshDetails() {
        self.m_tblView.reloadData()
    }
    
}

extension String {
    var boolValue: Bool {
        return NSString(string: self).boolValue
    }}
