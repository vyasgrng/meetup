//
//  ShowProfileVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 23/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import Kingfisher

class ShowProfileVC: BaseVC {
    
    //    let sectionTitles = ["","Personal Detail","Education And Other Detail"]
    let sectionTitles = ["Personal Detail","Education And Other Detail"]
    
    //    let rowImages = [[],[UIImage(named: "noun_Phone_2904960"),UIImage(named: "noun_gender_973031"),UIImage(named: "noun_Birthday_2072092"),UIImage(named: "noun_Address_2048875")],[UIImage(named: "noun_School_108315"),UIImage(named: "noun_Mortar Board_239008"),UIImage(named: "noun_Work_1730635"),UIImage(named: "noun_hobby_2367921"),UIImage(named: "noun_Language_1824073")]]
    
    let rowImages = [[UIImage(named: "noun_Phone_2904960"),UIImage(named: "noun_gender_973031"),UIImage(named: "noun_Birthday_2072092"),UIImage(named: "noun_Address_2048875")],[UIImage(named: "noun_School_108315"),UIImage(named: "noun_Mortar Board_239008"),UIImage(named: "noun_Work_1730635"),UIImage(named: "noun_hobby_2367921"),UIImage(named: "noun_Language_1824073")]]
    
    var rowValues:Array<Array<String>>?
    
    @IBOutlet weak var m_lblName: UILabel!
    @IBOutlet weak var m_lblEmail: UILabel!
    @IBOutlet weak var m_imgViewUser: UIImageView!
    @IBOutlet weak var m_constTop: NSLayoutConstraint!
    @IBOutlet weak var m_tblView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        // Call basic set up
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
    }
    
    //MARK:- Set initial UI
    func basicSetUp() {
        addBackButton()
        addTopTitle(title: "Profile")
        m_constTop.constant = topView.frame.height
        let dummyViewHeight = CGFloat(50)
        m_tblView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: m_tblView.bounds.size.width, height: dummyViewHeight))
        m_tblView.contentInset = UIEdgeInsets(top: -dummyViewHeight, left: 0, bottom: 0, right: 0)
        
        rowValues = [[AppSingleton.shared.userData!.mobile_cc! + " " + AppSingleton.shared.userData!.mobile_no!,AppSingleton.shared.userData!.gender!,AppSingleton.shared.userData!.dob,AppSingleton.shared.userData!.address],[AppSingleton.shared.userData!.school,AppSingleton.shared.userData!.university,AppSingleton.shared.userData!.work,AppSingleton.shared.userData!.hobby,AppSingleton.shared.userData!.language]] as? Array<Array<String>>
        
        
        m_lblName.text = (AppSingleton.shared.userData!.first_name)! + " " + (AppSingleton.shared.userData!.last_name)!
        m_lblEmail.text = AppSingleton.shared.userData!.email
        m_imgViewUser.kf.setImage(with: URL(string: (AppSingleton.shared.userData!.profile_pic)!), placeholder: #imageLiteral(resourceName: "profile"), options: [], progressBlock: nil, completionHandler: { (result) in
        })
        m_imgViewUser.layer.cornerRadius = 8
        
        m_tblView.delegate = self
        m_tblView.dataSource = self
        m_tblView.reloadData()
    }
    
    //MARK:- Edit profile clicked
    @IBAction func rightButtonClicked(_ sender: UIButton) {
        let vcObj = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as? EditProfileVC
        self.navigationController?.pushViewController(vcObj!, animated: true)
    }
}

extension ShowProfileVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewMain = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
        let lbl = UILabel(frame: CGRect(x: 30, y: 5, width: view.frame.size.width - 60, height: 21))
        lbl.center = CGPoint(x: lbl.center.x, y: viewMain.center.y)
        lbl.text = sectionTitles[section]
        lbl.textColor = .black
        lbl.font = UIFont().RobotoMedium(size: 20.0)
        viewMain.addSubview(lbl)
        return viewMain
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 4
        case 1:
            return 5
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShowProfUserInfoCell") as? ShowProfUserInfoCell
        cell?.m_btnImg.setImage(rowImages[indexPath.section][indexPath.row], for: .normal)
        cell?.m_lblValue.text = rowValues![indexPath.section][indexPath.row].isEmpty ? " - " : rowValues![indexPath.section][indexPath.row]
        return cell!
    }
}
