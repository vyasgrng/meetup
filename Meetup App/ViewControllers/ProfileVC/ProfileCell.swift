//
//  ProfileCell.swift
//  Meetup App
//
//  Created by Birju Bhatt on 21/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell {

    @IBOutlet weak var m_lblSubTitle: UILabel!
    @IBOutlet weak var m_btnArrow: UIButton!
    @IBOutlet weak var m_lblCaption: UILabel!
    @IBOutlet weak var m_imgView: UIImageView!
    @IBOutlet weak var m_viewBg: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
