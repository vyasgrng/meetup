//
//  PaymentListVC.swift
//  Meetup App
//
//  Created by Aditi Pancholi on 31/07/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class PaymentListVC: BaseVC {

    @IBOutlet weak var m_tblTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var m_tblPaymentList: UITableView!
    
    var arrCardDetails : Array<JSON> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.getListOfSavedCards()
            self.basicSetUp()
        }
    }
    
    //MARK:- Set initial UI
    func basicSetUp() {
        addBackButton(isFirstHierarchy: false)
        addTopTitle(title: "Payment Methods")
        m_tblTopConstraint.constant = topView.frame.height
        m_tblPaymentList.tableFooterView = UIView()
        m_tblPaymentList.sectionHeaderHeight = 54
        addRightButton(withText: "Add", withImageName: "", vc: self, target: #selector(addDetails(_:)))
    }

    @objc func addDetails(_ sender:UIButton){
         let vcObj = UIStoryboard(name: "InnerProfileSection", bundle: Bundle.main).instantiateViewController(withIdentifier: "PaymentMethodOptionsVC") as? PaymentMethodOptionsVC
        vcObj?.delegate = self
        self.navigationController?.pushViewController(vcObj!, animated: true)
        
    }
}


//MARK: Server interaction
extension PaymentListVC {
    func getListOfSavedCards(){
        AppSingleton.shared.startLoading()
        let params = ["token":UserDefaults.standard.currentToken()!]
        Alamofire.request(AppConstant.API.cardList, method: .post, parameters: params as Parameters,encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        let cardDetails = responseObject["data"]!.dictionaryValue
                        self.arrCardDetails = cardDetails["list"]!.arrayValue
                        self.arrCardDetails.insert(cardDetails["default_card"]!, at: 0)
                        self.m_tblPaymentList.reloadData()
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    func removeCardFRomServer(cardID : String){
        AppSingleton.shared.startLoading()
        let params = ["token":UserDefaults.standard.currentToken()!,"card_id":cardID]
        Alamofire.request(AppConstant.API.removeCard, method: .post, parameters: params as Parameters,encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        AppSingleton.shared.showSuccessAlert(msg: "Card deleted successfully.")
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func changeDefaultCardto(cardId : String,indexTapped : Int){
        AppSingleton.shared.startLoading()
        let params = ["token":UserDefaults.standard.currentToken()!,"card_id":cardId]
        Alamofire.request(AppConstant.API.changeDefaultCard, method: .post, parameters: params as Parameters,encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        AppSingleton.shared.showSuccessAlert(msg: "Default card changed successfully.")
                        //change indexes
                        let temp = self.arrCardDetails[indexTapped]
                        self.arrCardDetails.remove(at: indexTapped)
                        self.arrCardDetails.insert(temp, at: 0)
                        self.m_tblPaymentList.reloadRows(at: self.m_tblPaymentList.indexPathsForVisibleRows ?? [], with: .automatic)
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}

//MARK: Tableview delegate data source
extension PaymentListVC : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCardDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : PaymentListCell = tableView.dequeueReusableCell(withIdentifier: "PaymentListCell", for: indexPath) as! PaymentListCell
        cell.lblCardName.text = "Card ending in \(arrCardDetails[indexPath.row]["last4"].intValue)"
        cell.lblCardHolderName.text = arrCardDetails[indexPath.row]["name"].stringValue
        cell.m_imgDefaultMethod.isHidden = indexPath.row != 0
        return cell
    }
   
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //change card to defualt
        if indexPath.row != 0 {
            self.changeDefaultCardto(cardId: arrCardDetails[indexPath.row]["id"].stringValue, indexTapped: indexPath.row)
        }
    }
    
     func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.removeCardFRomServer(cardID: arrCardDetails[indexPath.row]["id"].stringValue)
            arrCardDetails.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
}


//MARK: add Payment method delegate
extension PaymentListVC : ProtocolPaymentMethodOptionsVC{
    func refreshDetails() {
        //call api and reload
        getListOfSavedCards()
    }
}
