//
//  CurrencyCell.swift
//  Meetup App
//
//  Created by Birju Bhatt on 06/12/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class CurrencyCell: UITableViewCell {

    @IBOutlet weak var m_lblCurrencyName: UILabel!
    @IBOutlet weak var m_viewBg: UIView!
    @IBOutlet weak var m_btnDefault: UIButton!
    @IBOutlet weak var m_btnMakeDefault: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        m_btnDefault.titleLabel?.font = UIFont().RobotoRegular(size: 12.0)
        m_btnMakeDefault.titleLabel?.font = UIFont().RobotoRegular(size: 12.0)
        // Configure the view for the selected state
    }

}
