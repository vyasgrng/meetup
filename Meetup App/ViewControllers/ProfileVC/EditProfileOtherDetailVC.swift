//
//  EditProfileOtherDetailVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 30/05/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

struct editProfileOtherData {
    var images:UIImage?
    var placeHolders:String?
    var key:String?
    var kbType:UIKeyboardType?
}

class EditProfileOtherDetailVC: BaseVC {

    
    @IBOutlet weak var m_lblTitle: UILabel!
    @IBOutlet weak var m_btnSave: DefaultButton!
    @IBOutlet weak var m_constTop: NSLayoutConstraint!
    @IBOutlet weak var m_tblView: UITableView!
    
    var dictData:Dictionary<String,Any> = ["":""]
    var structEditPro:[editProfileOtherData]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.basicSetUp()
    }
    

    //MARK:- Set initial UI
    func basicSetUp() {
        
        DispatchQueue.main.async {
            self.m_constTop.constant = self.topView.frame.height + 20
        }
        m_lblTitle.layoutIfNeeded()
        
        structEditPro = [
            editProfileOtherData(images:#imageLiteral(resourceName: "noun_School_108315"),placeHolders: "School",key:"school",kbType:.default),
            editProfileOtherData(images:#imageLiteral(resourceName: "noun_Mortar Board_239008"),placeHolders: "University",key:"university",kbType:.default),
            editProfileOtherData(images:#imageLiteral(resourceName: "noun_Work_1730635"),placeHolders: "Work",key:"work",kbType:.default),
            editProfileOtherData(images:#imageLiteral(resourceName: "noun_hobby_2367921"),placeHolders: "Hobbies",key:"hobby",kbType:.default),
            editProfileOtherData(images:#imageLiteral(resourceName: "noun_Language_1824073"),placeHolders: "Language",key:"language",kbType:.default),
        ]
        
        dictData["school"] = AppSingleton.shared.userData!.school!
        dictData["university"] = AppSingleton.shared.userData!.university!
        dictData["work"] = AppSingleton.shared.userData!.work!
        dictData["hobby"] = AppSingleton.shared.userData!.hobby!
        dictData["language"] = AppSingleton.shared.userData!.language!
        
        addBackButton(isFirstHierarchy: false)
        self.addTopTitle(title: "Edit Profile")
        m_tblView.delegate = self
        m_tblView.dataSource = self
    }
    
    func dataValidation() -> Bool {
        self.view.endEditing(true)
        if (dictData["school"] as? String)!.isEmpty {
            AppSingleton.shared.showErrorAlert(msg: "School Required.")
            return false
        } else if (dictData["university"] as? String)!.isEmpty {
            AppSingleton.shared.showErrorAlert(msg: "University Required.")
            return false
        } else if (dictData["work"] as? String)!.isEmpty {
            AppSingleton.shared.showErrorAlert(msg: "Work Required.")
            return false
        } else if (dictData["hobby"] as? String)!.isEmpty {
            AppSingleton.shared.showErrorAlert(msg: "Hobby Required.")
            return false
        } else if (dictData["language"] as? String)!.isEmpty {
            AppSingleton.shared.showErrorAlert(msg: "Language Required.")
            return false
        }
        return true
    }
    
    
    @IBAction func actionSave(_ sender: UIButton) {
        
        if dataValidation() {
            let params:Dictionary<String,String> = ["token":UserDefaults.standard.currentToken()!,
                                                    "first_name":dictData["firstName"] as! String,
                                                    "last_name":dictData["lastName"]! as! String,
                                                    "mobile_no":dictData["mobile"] as! String,
                                                    "gender":AppSingleton.shared.getGender(gender:dictData["gender"]! as! String),
                                                    "dob":dictData["dob"] as! String,
                                                    "school":dictData["school"] as! String,
                                                    "university":dictData["university"] as! String,
                                                    "work":dictData["work"] as! String,
                                                    "hobby":dictData["hobby"] as! String,
                                                    "language":dictData["language"] as! String,
                                                    "mobile_cc":dictData["mobile_cc"] as! String,
                                                    "address":dictData["address"] as! String,
                                                    "city":dictData["city"] as! String,
                                                    "state":dictData["state"] as! String,
                                                    "zip_code":dictData["zip_code"] as! String,
                                                    "country":dictData["country"] as! String]

            let imgProfile = dictData["profile_pic"] as? UIImage
            
            AppSingleton.shared.startLoading()

            if imgProfile == nil {
                Alamofire.request(AppConstant.API.updateProfile, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
                    switch response.result {
                    case .success:
                        if let value = response.result.value {
                            AppSingleton.shared.stopLoading()
                            let responseObject = JSON(value).dictionaryValue
                            if responseObject["status"]?.intValue == 0 {
                                AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                            } else if responseObject["status"]?.intValue == 3 {
                                AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                            } else {
                                UserDefaults.standard.set(encodable: UserModal(data: JSON(value)), forKey: "userData")
                                for vc in (self.navigationController?.viewControllers)! {
                                    if vc is ShowProfileVC {
                                        self.navigationController?.popToViewController(vc, animated: true)
                                        break
                                    }
                                }
                            }
                        }
                    case .failure(let error):
                        print(error)
                    }
                }
            } else {
                Alamofire.upload(multipartFormData: { (multipartFormData) in
                    multipartFormData.append((imgProfile!.jpegData(compressionQuality: 0.5))!, withName: "profile_pic", fileName: "Profile.jpeg", mimeType: "image/jpeg")
                    
                    for (key, value) in params {
                        multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                    }
                }, to:AppConstant.API.updateProfile)
                { (result) in
                    switch result {
                    case .success(let upload, _, _):
                        
                        upload.uploadProgress(closure: { (progress) in
                            //Print progress
                        })
                        
                        upload.responseJSON { response in
                            //print response.result
                            print(response)
                            AppSingleton.shared.stopLoading()
                            if let value = response.result.value {
                                let responseObject = JSON(value).dictionaryValue
                                if responseObject["status"]?.intValue == 0 {
                                    AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                                } else if responseObject["status"]?.intValue == 3 {
                                    AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                                } else {
                                    UserDefaults.standard.set(encodable: UserModal(data: JSON(value)), forKey: "userData")
                                    for vc in (self.navigationController?.viewControllers)! {
                                        if vc is ShowProfileVC {
                                            self.navigationController?.popToViewController(vc, animated: true)
                                            break
                                        }
                                    }
                                }
                            }
                        }
                        
                    case .failure( _):
                        break
                        //print encodingError.description
                    }
                }
            }

        }
        
    }
    
}

extension EditProfileOtherDetailVC: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        dictData[structEditPro![textField.tag].key!] = textField.text
    }
}

extension EditProfileOtherDetailVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (structEditPro?.count)!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SignUpCell") as? SignUpCell
//        cell?.m_viewContain.addShadowDefault(isOnlyRightSide: true)
        cell?.m_viewContain.addShadow(offset: .zero, color: .lightGray, opacity: 0.6, radius: 2)
        cell?.m_txtFld.delegate = self
        cell?.m_txtFld.tag = indexPath.row
        cell?.m_txtFld.placeholder = structEditPro![indexPath.row].placeHolders
        cell?.m_txtFld.text = (dictData[structEditPro![indexPath.row].key!] as! String)
        cell?.m_imgView.image = structEditPro![indexPath.row].images
        
        return cell!
    }
}
