//
//  BlockedUsersCell.swift
//  Meetup App
//
//  Created by Birju Bhatt on 24/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class BlockedUsersCell: UITableViewCell {

    @IBOutlet weak var m_lblTime: UILabel!
    @IBOutlet weak var m_viewContain: UIView!
    @IBOutlet weak var m_imgUser: UIImageView!
    @IBOutlet weak var m_lblName: UILabel!
    @IBOutlet weak var m_btnUnblock: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        m_imgUser.addCornerRadius(radius: m_imgUser.frame.height / 2)
        m_imgUser.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        m_viewContain.addShadowWithCorner(cornerRadius: 5, offset: .zero, color: .lightGray, radius: 4, opacity: 0.6)
        
        // Configure the view for the selected state
    }

}
