//
//  PaymentListCell.swift
//  Meetup App
//
//  Created by Aditi Pancholi on 31/07/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class PaymentListCell: UITableViewCell {

    @IBOutlet weak var m_imgDefaultMethod: UIImageView!
    @IBOutlet weak var lblCardHolderName: UILabel!
    @IBOutlet weak var lblCardName: UILabel!
    override func awakeFromNib() {
        
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
