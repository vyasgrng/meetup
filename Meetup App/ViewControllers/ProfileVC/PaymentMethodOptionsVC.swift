//
//  PaymentMethodOptionsVC.swift
//  Meetup App
//
//  Created by Aditi Pancholi on 26/07/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

protocol ProtocolPaymentMethodOptionsVC{
    func refreshDetails()
}


class PaymentMethodOptionsVC: BaseVC {

    @IBOutlet weak var m_tblPaymentOptions: UITableView!
    @IBOutlet weak var m_constTblTop: NSLayoutConstraint!
    var arrPaymentOptions = ["Credit/Debit Card","Paypal"]
    
    var delegate : ProtocolPaymentMethodOptionsVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
    }
    
    //MARK:- Set initial UI
    func basicSetUp() {
        addBackButton()
        addTopTitle(title: "")
        m_constTblTop.constant = topView.frame.height
        m_tblPaymentOptions.tableFooterView = UIView()
        m_tblPaymentOptions.sectionHeaderHeight = 54
    }
}

//MARK: Tableview delegate datasources
extension PaymentMethodOptionsVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPaymentOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentCell", for: indexPath) as! ProfileCell
        cell.m_lblCaption.text = arrPaymentOptions[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            let vcObj = UIStoryboard(name: "InnerProfileSection", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddCardVC") as? AddCardVC
            vcObj?.delegate = self
            self.navigationController?.pushViewController(vcObj!, animated: true)
        }
    }
}

//MARK: add Payment method delegate		
extension PaymentMethodOptionsVC : ProtocolPaymentMethodVC{
    func refreshDetails() {
        delegate.refreshDetails()
    }
}
