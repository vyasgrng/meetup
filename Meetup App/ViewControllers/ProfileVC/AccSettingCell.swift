//
//  AccSettingCell.swift
//  Meetup App
//
//  Created by Birju Bhatt on 22/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class AccSettingCell: UITableViewCell {

    @IBOutlet weak var m_switchChat: UISwitch!
    @IBOutlet weak var m_switchReminder: UISwitch!
    @IBOutlet weak var m_switchEvent: UISwitch!
    @IBOutlet weak var m_switchSupport: UISwitch!
    @IBOutlet weak var m_viewBg: UIView!
    @IBOutlet weak var m_lblNotification: UILabel!
    @IBOutlet weak var m_lblChat: UILabel!
    @IBOutlet weak var m_lblReminder: UILabel!
    @IBOutlet weak var m_lblUpdateEvents: UILabel!
    @IBOutlet weak var m_lblSupport: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
