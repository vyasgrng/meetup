//
//  VerificationDocumetCell.swift
//  Meetup App
//
//  Created by Birju Bhatt on 17/04/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class VerificationDocumetCell: UITableViewCell {

    @IBOutlet weak var m_collectionView: UICollectionView!
    @IBOutlet weak var m_btnUpload: UIButton!
    
    @IBOutlet weak var m_imgViewVerified: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
