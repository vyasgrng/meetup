//
//  ShowProfUserInfoCell.swift
//  Meetup App
//
//  Created by Birju Bhatt on 23/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class ShowProfUserInfoCell: UITableViewCell {

    @IBOutlet weak var m_viewContain: UIView!
    @IBOutlet weak var m_btnImg: UIButton!
    @IBOutlet weak var m_lblValue: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
//        m_viewContain.addShadowDefault(isOnlyRightSide: true)
        // Configure the view for the selected state
    }

}
