//
//  VerificationVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 22/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import FBSDKLoginKit


class VerificationVC: BaseVC {

    @IBOutlet weak var m_imgViewVerified: UIImageView!
    @IBOutlet weak var m_constTblTop: NSLayoutConstraint!
    @IBOutlet weak var m_tblView: UITableView!
//    var captions = [["title":"Facebook","btnTitle":"Link"],["title":"Email ID","btnTitle":"Send"]]
    var captions = ["Facebook","Email ID","Document"]
    var arrDocsData:Array<Any> = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.m_tblView.reloadData()
    }
    
    //MARK:- Set initial UI
    func basicSetUp() {
        addBackButton()
        addTopTitle(title: "Verification")
        m_constTblTop.constant = topView.frame.height + 10
        m_tblView.tableFooterView = UIView()
        
        arrDocsData = (AppSingleton.shared.userData?.documents)!
        m_tblView.delegate = self
        m_tblView.dataSource = self
        
    }
    
    //MARK:- Email send api
    @objc func actionSend(_ sender:UIButton) {
        self.view.endEditing(true)
        let returnValue = dataValidation()
        if returnValue.0 {
            AppSingleton.shared.startLoading()
            let params = ["token":UserDefaults.standard.currentToken()!,"email":returnValue.1]
            Alamofire.request(AppConstant.API.verifyEmail, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
                switch response.result {
                case .success:
                    if let value = response.result.value {
                        AppSingleton.shared.stopLoading()
                        let responseObject = JSON(value).dictionaryValue
                        if responseObject["status"]?.intValue == 0 {
                            AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                        } else if responseObject["status"]?.intValue == 3 {
                            AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                        } else {
                            AppSingleton.shared.showSuccessAlert(msg: (responseObject["message"]?.stringValue)!)
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    
    func dataValidation() -> (Bool,String) {
        let cell = m_tblView.cellForRow(at: IndexPath(row: 1, section: 0)) as? VerificationTextCell
        if (cell?.m_txtFld.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty)! {
            AppSingleton.shared.showErrorAlert(msg: "Please enter email for verification")
            return (false,"")
        }
        return (true,(cell?.m_txtFld.text)!)
    }
    
    //MARK:- Social Sign In
    func socialSignIn(params:[String:String]) {
        Alamofire.request(AppConstant.API.socialLogin, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        UserDefaults.standard.set(encodable: UserModal(data: JSON(value)), forKey: "userData")
                        UserDefaults.standard.storeToken(token: responseObject["data"]!["token"].stringValue)
                        self.m_tblView.reloadData()
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    @objc func showActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func camera()
    {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.allowsEditing = true
        myPickerController.sourceType = UIImagePickerController.SourceType.camera
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    func photoLibrary()
    {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.allowsEditing = true
        myPickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    //MARK:- Upload document
    func uploadDocument() {
        let params:Dictionary<String,String> = ["token":UserDefaults.standard.currentToken()!]
        
        AppSingleton.shared.startLoading()
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            let img = self.arrDocsData[self.arrDocsData.count - 1] as? UIImage
            multipartFormData.append((img!.jpegData(compressionQuality: 0.5))!, withName: "documents", fileName: "Document.jpeg", mimeType: "image/jpeg")
            
            for (key, value) in params {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to:AppConstant.API.uploadDocument)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                })
                
                upload.responseJSON { response in
                    //print response.result
                    print(response)
                    AppSingleton.shared.stopLoading()
                    if let value = response.result.value {
                        let responseObject = JSON(value).dictionaryValue
                        if responseObject["status"]?.intValue == 0 {
                            AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                        } else if responseObject["status"]?.intValue == 3 {
                            AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                        } else {
                            UserDefaults.standard.storeToken(token: responseObject["data"]!["token"].stringValue)
                            UserDefaults.standard.set(encodable: UserModal(data: JSON(value)), forKey: "userData")
                            self.arrDocsData.removeAll()
                            self.arrDocsData = (AppSingleton.shared.userData?.documents)!
                            self.m_tblView.reloadData()
                        }
                    }
                }
                
            case .failure( _):
                break
                //print encodingError.description
            }
        }
    }
    
    //MARK:- Delete document
    @objc func deleteDocument(_ sender:UIButton) {
        AppSingleton.shared.startLoading()
        let params = ["token":UserDefaults.standard.currentToken()!,"url":arrDocsData[sender.tag]]
        Alamofire.request(AppConstant.API.deleteDocument, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        UserDefaults.standard.storeToken(token: responseObject["data"]!["token"].stringValue)
                        UserDefaults.standard.set(encodable: UserModal(data: JSON(value)), forKey: "userData")
                        self.arrDocsData.remove(at: sender.tag)
                        let cell = self.m_tblView.cellForRow(at: IndexPath(item: 2, section: 0)) as? VerificationDocumetCell
                        cell?.m_collectionView.reloadData()
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
     func facebookLoginTapped() {
        let fbLoginManager : LoginManager = LoginManager()
        fbLoginManager.logIn(permissions: ["email"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : LoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions.count > 0 {
                    AppSingleton.shared.startLoading()
                    if(fbloginresult.grantedPermissions.contains("email")) {
                        if((AccessToken.current) != nil){
                            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                                if (error == nil){
                                    let dict = result as! [String : Any]
                                    print(result!)
                                    print(dict)
                                    
                                    guard let imageURL = ((dict["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String else {
                                        return
                                    }
                                    let params:[String:String] = ["login_type":"1","token":dict["id"]! as! String,"firebase_token":"123","device_type":"0","app_version":AppSingleton().getAppVersion(),"first_name":dict["first_name"]! as! String ,"last_name":dict["last_name"]! as! String,"email":dict["email"]! as! String,"profile_url":imageURL]
                                    self.socialSignIn(params: params)
                                }
                            })
                        }
                    }
                } else {
                    AppSingleton.shared.stopLoading()
                }
            } else {
                
            }
        }
    }
}

extension VerificationVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            arrDocsData.append(pickedImage)
            let cell = m_tblView.cellForRow(at: IndexPath(item: 2, section: 0)) as? VerificationDocumetCell
            cell?.m_collectionView.reloadData()
            self.uploadDocument()
        }
        self.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension VerificationVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            facebookLoginTapped()
            break
        case 1:
            let verifyEmailVCObj = self.storyboard?.instantiateViewController(withIdentifier: "VerifyEmailVC") as? VerifyEmailVC
            self.navigationController?.pushViewController(verifyEmailVCObj!, animated: true)
            break
        case 2:
            let verifyDocVCObj = self.storyboard?.instantiateViewController(withIdentifier: "VerifyDocumentVC") as? VerifyDocumentVC
            self.navigationController?.pushViewController(verifyDocVCObj!, animated: true)
            break
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "VerificationNormalCell") as? VerificationNormalCell
        cell?.m_lblCaption.text = captions[indexPath.row]
        cell?.m_lblCaption.font = UIFont().RobotoRegular(size: 20.0)
        cell?.m_btnLink.titleLabel?.font = UIFont().RobotoRegular(size: 20.0)
        switch indexPath.row {
        case 0:
            cell?.m_imgRightArrow.isHidden = true
            cell?.m_btnLink.isUserInteractionEnabled = false
            if AppSingleton.shared.userData!.facebook_linked == "true" {
                cell?.m_imgViewVerified.isHidden = false
                cell?.m_btnLink.setTitle("Verified", for: .normal)
                cell?.isUserInteractionEnabled = false
            } else {
                cell?.m_btnLink.setTitle("Link", for: .normal)
                cell?.isUserInteractionEnabled = true
            }
            break
        case 1:
            if AppSingleton.shared.userData!.email_verified == "true" {
                cell?.m_imgViewVerified.isHidden = false
                cell?.m_btnLink.setTitle("Verified", for: .normal)
                cell?.m_btnLink.isUserInteractionEnabled = false
                cell?.m_imgRightArrow.isHidden = true
                cell?.isUserInteractionEnabled = false

            } else {
                cell?.m_imgViewVerified.isHidden = true
                cell?.m_btnLink.isHidden = true
                cell?.m_imgRightArrow.isHidden = false
                cell?.isUserInteractionEnabled = true
            }
            break
        default:
            if AppSingleton.shared.userData!.document_verified == "true" {
                cell?.m_imgViewVerified.isHidden = false
                cell?.m_btnLink.setTitle("Verified", for: .normal)
                cell?.m_btnLink.isUserInteractionEnabled = false
                cell?.m_imgRightArrow.isHidden = true
                cell?.isUserInteractionEnabled = false

            } else {
                cell?.m_imgViewVerified.isHidden = true
                cell?.m_btnLink.isHidden = true
                cell?.m_imgRightArrow.isHidden = false
                cell?.isUserInteractionEnabled = true
            }
            break
        }
        
            return cell!
    }
}

extension VerificationVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrDocsData.count//(AppSingleton.shared.userData?.documents?.count)! + arrayDocs.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PartyDetailCollectionMemberCell", for: indexPath) as? PartyDetailCollectionMemberCell

        if arrDocsData[indexPath.item] is UIImage {
            cell!.m_imgView.image = arrDocsData[indexPath.item] as? UIImage
        } else {
            let imgUrl: String = (arrDocsData[indexPath.item] as! JSON).string!
            cell?.m_imgView.kf.setImage(with: URL(string:imgUrl))            
        }
        cell?.m_imgView.contentMode = .scaleAspectFill
        cell?.m_imgView.addCornerRadius(radius: 5.0)
        cell?.m_btnRemove.tag = indexPath.item
        cell?.m_btnRemove.addTarget(self, action: #selector(deleteDocument(_:)), for: .touchUpInside)
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width / 4, height: collectionView.frame.size.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right:  0)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
    }
}
