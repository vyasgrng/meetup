//
//  EditProfileImageCell.swift
//  Meetup App
//
//  Created by Birju Bhatt on 26/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class EditProfileImageCell: UITableViewCell {

    @IBOutlet weak var m_btnEdit: UIButton!
    @IBOutlet weak var m_viewContain: UIView!
    @IBOutlet weak var m_imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
