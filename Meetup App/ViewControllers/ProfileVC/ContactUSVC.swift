//
//  ContactUSVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 23/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ContactUSVC: BaseVC {

    @IBOutlet weak var m_constTop: NSLayoutConstraint!
    
    @IBOutlet weak var m_viewName: UIView!
    @IBOutlet weak var m_txtFldName: UITextField!
        
    @IBOutlet weak var m_viewEmail: UIView!
    @IBOutlet weak var m_txtFldEmail: UITextField!
    
    @IBOutlet weak var m_viewMsg: UIView!
    @IBOutlet weak var m_txtViewMessage: UITextView!
    
    @IBOutlet weak var m_btnSend: DefaultButton!
    var topTitle:String?
    var isFirstHierarchy = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
    }
    
    //MARK:- Set initial UI
    func basicSetUp() {
        addBackButton(isFirstHierarchy:isFirstHierarchy)
        addTopTitle(title: topTitle!)
        m_constTop.constant = topView.frame.height
        m_viewName.addShadowWithCorner(cornerRadius: 5.0, offset: .zero, color: .lightGray, radius: 2.0, opacity: 0.6)
        m_viewEmail.addShadowWithCorner(cornerRadius: 5.0, offset: .zero, color: .lightGray, radius: 2.0, opacity: 0.6)
        m_viewMsg.addShadowWithCorner(cornerRadius: 5.0, offset: .zero, color: .lightGray, radius: 2.0, opacity: 0.6)

        m_txtFldEmail.text = AppSingleton.shared.userData!.email
    }
       
    //MARK:- Action Send
    @IBAction func actionSend(_ sender: UIButton) {
        view.endEditing(true)
        AppSingleton.shared.startLoading()
        let type = (UserDefaults.standard.currentUserType() == UserType.Joinee.rawValue) ? "join" : "host"
        let params = ["name":m_txtFldName.text ?? "",
                      "email": m_txtFldEmail.text ?? "",
                      "message": m_txtViewMessage.text ?? "",
                      "type": type
                      ]
        Alamofire.request(AppConstant.API.contactUs, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
            AppSingleton.shared.stopLoading()
            switch response.result {
            case .success:
                if let value = response.result.value {
                    
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        AppSingleton.shared.showSuccessAlert(msg: (responseObject["message"]?.stringValue)!)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
}

//MARK:- Textview stuff
extension ContactUSVC: UITextViewDelegate {
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if textView.text == "Message"
        {
            // move cursor to start
            moveCursorToStart(aTextView: textView)
        }
        return true
    }
    
    func moveCursorToStart(aTextView: UITextView)
    {
        DispatchQueue.main.async {
            aTextView.selectedRange = NSMakeRange(0, 0)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        // remove the placeholder text when they start typing
        // first, see if the field is empty
        // if it's not empty, then the text should be black and not italic
        // BUT, we also need to remove the placeholder text if that's the only text
        let newLength = textView.text.utf16.count + text.utf16.count - range.length
        if newLength > 0 // have text, so don't show the placeholder
        {
            // check if the only text is the placeholder and remove it if needed
            if textView.text == "Message"
            {
                applyNonPlaceholderStyle(aTextview: textView)
                textView.text = ""
            }
            return true
        } else  // no text, so show the placeholder
        {
            applyPlaceholderStyle(aTextview: textView, placeholderText: "Message")
            moveCursorToStart(aTextView: textView)
            return false
        }
    }
    
    func applyPlaceholderStyle(aTextview: UITextView, placeholderText: String)
    {
        // make it look (initially) like a placeholder
        aTextview.textColor = UIColor.lightGray
        aTextview.text = placeholderText
    }

    
    func applyNonPlaceholderStyle(aTextview: UITextView) {
        // make it look like normal text instead of a placeholder
        aTextview.textColor = .black
    }
}
