//
//  InviteFriendVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 22/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class InviteFriendVC: BaseVC {
    
    @IBOutlet weak var lblReferralCode: UILabel!
    @IBOutlet weak var m_constTop: NSLayoutConstraint!    
    @IBOutlet weak var m_btnShare: DefaultButton!
    @IBOutlet weak var m_lblOr: UILabel!
    @IBOutlet weak var m_lblCode: UILabel!
    @IBOutlet weak var m_btnCopy: UIButton!
    @IBOutlet weak var m_lblCoins: UILabel!
    @IBOutlet weak var m_lblCaption: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        addBackButton()
        self.addTopTitle(title: "Invite Friends")
        m_lblCaption.font = UIFont().RobotoMedium(size: 20.0)
        m_lblCoins.font = UIFont().RobotoMedium(size: 20.0)
        m_lblCode.font = UIFont().RobotoMedium(size: 20.0)
        m_btnCopy.titleLabel?.font = UIFont().RobotoMedium(size: 14.0)
        m_btnShare.titleLabel?.font = UIFont().RobotoMedium(size: 13.0)
        m_btnCopy.addShadowWithCorner(cornerRadius: (m_btnCopy.frame.height/2), offset: .zero, color: .lightGray, radius: 4, opacity: 0.6)
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnInviteFriendsAction(_ sender: DefaultButton) {
        
    }
    
}
