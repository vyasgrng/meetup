//
//  ProfileCell1.swift
//  Meetup App
//
//  Created by Birju Bhatt on 21/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class ProfileCell1: UITableViewCell {

    @IBOutlet weak var m_imgViewProfile: UIButton!
    @IBOutlet weak var m_lblUserName: UILabel!
    @IBOutlet weak var m_lblUserEmail: UILabel!
    @IBOutlet weak var m_btnEditPic: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
