//
//  VerificationNormalCell.swift
//  Meetup App
//
//  Created by Birju Bhatt on 22/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class VerificationNormalCell: UITableViewCell {

    @IBOutlet weak var m_viewContain: UIView!
    @IBOutlet weak var m_btnAdd: DefaultButton!
    @IBOutlet weak var m_lblCaption: UILabel!
    @IBOutlet weak var m_btnLink: UIButton!
    @IBOutlet weak var m_imgViewVerified: UIImageView!
    @IBOutlet weak var m_imgRightArrow: UIImageView!
    @IBOutlet weak var m_collectionViewDocument: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
