//
//  VerifyEmailVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 16/05/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class VerifyEmailVC: BaseVC {

    @IBOutlet weak var m_lblSubtitle: UILabel!
    @IBOutlet weak var m_lblHeader: UILabel!
    @IBOutlet weak var m_constTop: NSLayoutConstraint!
    @IBOutlet weak var m_btnVerifyEmail: DefaultButton!
    @IBOutlet weak var m_viewEmail: UIView!
    @IBOutlet weak var m_txtFld: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
    }
    
    //MARK:- Set initial UI
    func basicSetUp() {
        addBackButton()
        addTopTitle(title: "Verify Email")
        m_constTop.constant = topView.frame.height
        m_viewEmail.addShadowDefault(isOnlyRightSide: true)
        m_lblHeader.font = UIFont().RobotoMedium(size: 25.0)
        m_lblSubtitle.font = UIFont().RobotoRegular(size: 15.0)
        m_txtFld.font = UIFont().RobotoRegular(size: 20.0)
        m_btnVerifyEmail.titleLabel?.font = UIFont().RobotoMedium(size: 20.0    )
    }
    
    func dataValidation() -> (Bool,String) {
        if (m_txtFld.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty)! {
            AppSingleton.shared.showErrorAlert(msg: "Please enter email for verification")
            return (false,"")
        }
        return (true,(m_txtFld.text)!)
    }
    
    //MARK:- Email send api
    
    @IBAction func actionSend(_ sender: UIButton) {
        self.view.endEditing(true)
        let returnValue = dataValidation()
        if returnValue.0 {
            AppSingleton.shared.startLoading()
            let params = ["token":UserDefaults.standard.currentToken() ?? "", "email":returnValue.1]
            Alamofire.request(AppConstant.API.verifyEmail, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
                switch response.result {
                case .success:
                    if let value = response.result.value {
                        AppSingleton.shared.stopLoading()
                        let responseObject = JSON(value).dictionaryValue
                        if responseObject["status"]?.intValue == 0 {
                            AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                        } else if responseObject["status"]?.intValue == 3 {
                            AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                        } else {
                            AppSingleton.shared.showSuccessAlert(msg: (responseObject["message"]?.stringValue)!)
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
}
