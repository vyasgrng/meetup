//
//  InboxVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 18/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class InboxVC: BaseVC {

    @IBOutlet weak var m_tblViewInbox: UITableView!
    @IBOutlet weak var m_txtFldSearchbar: UITextField!
    @IBOutlet weak var m_viewSearchBar: UIView!
    @IBOutlet weak var m_constTop: NSLayoutConstraint!
    
    var arrayChatList:Array<JSON> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.basicSetUp()
        }
    }
    
    //MARK:- Set initial UI
    func basicSetUp() {
        
        m_viewSearchBar.addCornerRadius(radius: 3)
        m_tblViewInbox.addCornerRadius(radius: 3)
        m_viewSearchBar.addSearchBarShadow()
        m_tblViewInbox.addSearchBarShadow()
        addBackButton(withImage: #imageLiteral(resourceName: "Burger Button"),isFirstHierarchy: false)
        addTopTitle(title: "Chat List")
        m_tblViewInbox.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.02) {
            self.m_constTop.constant = self.topView.frame.height + 15
            self.getLists()
        }
    }
    
    override func backClicked(_ sender: UIButton) {
        self.slideMenuController()?.openLeft()
    }
    
    func getLists() {
        var dict:[String:String] = [:]

        if UserDefaults.standard.currentUserType() == UserType.Host.rawValue {
            dict["type"] = "host"
        } else {
            dict["type"] = "guest"
        }
        
        let jsonStr = AppSingleton.shared.dictToJSONString(dict: dict)
        SocketIOManager.sharedInstance.manager.defaultSocket.emitWithAck("list_chats",with: [jsonStr]).timingOut(after: 0) { data in
            print(data)
            let response = JSON(data)
            self.arrayChatList = (response.arrayValue[0].dictionaryValue["data"]!["chats"]).arrayValue
            self.m_tblViewInbox.reloadData()
        }
    }
}

extension InboxVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayChatList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InboxCell") as? InboxCell
        let data = self.arrayChatList[indexPath.row].dictionaryValue
        cell?.m_imgViewProfile.kf.setImage(with: data["profile_pic"]?.url, placeholder: UIImage(named: "profile"), options: [], progressBlock: nil, completionHandler: { (result) in
        })
        
        cell?.m_lblUserName.text = (data["first_name"]?.stringValue)! + " " + (data["last_name"]?.stringValue)!
        let messageData = data["last_message"]?.stringValue.data(using: .utf8)
        let messageWithEmojiEncoding = String.init(data: messageData!, encoding: .nonLossyASCII)
        cell?.m_lblChat.text = messageWithEmojiEncoding
        
        if data["can_review"]!.boolValue {
            cell?.m_btnReview.isHidden = false
            cell?.m_btnReview.setTitle("Review", for: .normal)
        } else {
            cell?.m_btnReview.isHidden = true
            cell?.m_btnReview.setTitle("", for: .normal)
        }
        
        if data["last_message_at"]?.stringValue == "-1" {
            cell?.m_lblTime.text = ""
        } else {
            cell?.m_lblTime.text = data["last_message_at"]?.doubleValue.timeStampToDate(format: "dd MMM YYYY")
        }
        cell?.m_imgViewProfile.addCornerRadius(radius: (cell?.m_imgViewProfile.frame.height)! / 2)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = self.arrayChatList[indexPath.row].dictionaryValue
        let chatVCObj = self.storyboard?.instantiateViewController(withIdentifier: "ChattingVC") as? ChattingVC
        chatVCObj?.chatId = (data["chat_id"]?.stringValue)!
        self.navigationController?.pushViewController(chatVCObj!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

