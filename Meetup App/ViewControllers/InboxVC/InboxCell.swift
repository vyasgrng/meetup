//
//  InboxCell.swift
//  Meetup App
//
//  Created by Birju Bhatt on 21/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class InboxCell: UITableViewCell {

    @IBOutlet weak var m_imgViewProfile: UIImageView!
    @IBOutlet weak var m_lblUserName: UILabel!
    @IBOutlet weak var m_lblTime: UILabel!
    @IBOutlet weak var m_btnReview: UIButton!
    @IBOutlet weak var m_lblChat: UILabel!
    @IBOutlet weak var m_constReviewHeight: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
