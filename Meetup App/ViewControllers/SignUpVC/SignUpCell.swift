//
//  SignUpCell.swift
//  Meetup App
//
//  Created by Birju Bhatt on 15/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class SignUpCell: UITableViewCell {

    @IBOutlet weak var m_viewContain: UIView!
    @IBOutlet weak var m_imgView: UIImageView!
    @IBOutlet weak var m_txtFld: UITextField!
    @IBOutlet weak var m_imgDropdown: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
