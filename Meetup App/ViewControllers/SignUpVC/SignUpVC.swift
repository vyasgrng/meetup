//
//  SignUpVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 14/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import DropDown
import TOWebViewController
import MICountryPicker

struct signUpData {
    var images:UIImage?
    var placeHolders:String?
    var key:String?
    var kbType:UIKeyboardType?
    var isSecuredEntry:Bool?
    var isDropDown:Bool?
}

class SignUpVC: BaseVC {

    @IBOutlet weak var m_tblSignUp: UITableView!
    @IBOutlet weak var m_lblTerms: CustomLabel!
    @IBOutlet weak var m_viewDone: UIView!
    @IBOutlet weak var m_constTblTop: NSLayoutConstraint!
    
    @IBOutlet weak var m_lblDone: UILabel!
    var structSignUp:[signUpData]?
    var dictData:Dictionary<String,String>?
    let dropDown = DropDown()
    var dialCode = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
        
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            print(countryCode)
            dialCode = "+\(AppSingleton.getCountryCallingCode(countryRegionCode: countryCode))"
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK:- Set initial UI
    func basicSetUp() {
        let radius:CGFloat = m_viewDone.frame.height / 7
        m_viewDone.addCornerRadius(radius: radius)
        m_viewDone.addBorder(width: 1.0, color: UIColor(red: 189, green: 211, blue: 229, alpha: 1.0))
       //developer2
        m_viewDone.layer.shadowColor = UIColor(red: 189, green: 211, blue: 229, alpha: 1.0).cgColor
        m_viewDone.layer.shadowOffset = CGSize(width: 0, height: 0)
        m_viewDone.layer.shadowOpacity = 1.0
        m_viewDone.layer.shadowRadius = 5.0
        m_viewDone.layer.masksToBounds = false
        m_viewDone.backgroundColor = UIColor().convertHexStringToColor(hexString: "202564")
        m_lblDone.font = UIFont().RobotoRegular(size: 16.0)
        //m_viewDone.gradientBackground(from: UIColor(red: 93, green: 73, blue: 230, alpha: 1.0), to: UIColor(red: 40, green: 117, blue: 208, alpha: 1.0), direction: .rightToLeft)

        m_constTblTop.constant = topView.frame.height
        topView.backgroundColor = .clear
        addBackButton(withImage:#imageLiteral(resourceName: "ic_back"), isFirstHierarchy: false)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(actionSignUp))
        m_viewDone.addGestureRecognizer(tapGesture)
        
        structSignUp = [
            signUpData(images: #imageLiteral(resourceName: "ic_person_outline_24px.pdf"), placeHolders: "First Name",key:"firstName",kbType:.default,isSecuredEntry:false,isDropDown:true),
            signUpData(images: #imageLiteral(resourceName: "ic_person_outline_24px.pdf"), placeHolders: "Last Name",key:"lastName",kbType:.default,isSecuredEntry:false,isDropDown:true),
            signUpData(images: #imageLiteral(resourceName: "ic_mail_outline_24px.pdf"), placeHolders: "Email ID",key:"email",kbType:.emailAddress,isSecuredEntry:false,isDropDown:true),
            signUpData(images: #imageLiteral(resourceName: "ic_phone_android_24px.pdf"), placeHolders: "Mobile",key:"mobile",kbType:.phonePad,isSecuredEntry:false,isDropDown:true),
            signUpData(images: #imageLiteral(resourceName: "ic_gender.pdf"), placeHolders: "Gender",key:"gender",kbType:.default,isSecuredEntry:false,isDropDown:false),
            signUpData(images: #imageLiteral(resourceName: "ic_password-24px.pdf"), placeHolders: "Password",key:"password",kbType:.default,isSecuredEntry:true,isDropDown:true),
            signUpData(images: #imageLiteral(resourceName: "ic_password-24px.pdf"), placeHolders: "Confirm Password",key:"confirmpassword",kbType:.default,isSecuredEntry:true,isDropDown:true)
        ]
        
        dictData = ["firstName":"","lastName":"","email":"","mobile":"","password":"","confirmpassword":"","gender":""]
        
        //mobile_cc
        
        m_tblSignUp.delegate = self
        m_tblSignUp.dataSource = self
        
        DropDown.startListeningToKeyboard()
        DropDown.appearance().textColor = UIColor.black
        DropDown.appearance().selectedTextColor = UIColor.black
        DropDown.appearance().textFont = UIFont().RobotoLight(size: 20)
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
        DropDown.appearance().cellHeight = 60
        
        m_lblTerms.font = UIFont().RobotoMedium(size: 16.0)
        m_lblTerms.setCustomWordsProperties(words: ["Terms & Conditions."], mainColor: UIColor().convertHexStringToColor(hexString: "202564"), customColor: UIColor().convertHexStringToColor(hexString: "E94A47"), font: UIFont().RobotoMedium(size: 16.0)) { (label, gesture) in
            guard let range = label.text?.range(of: "Terms & Conditions.")?.nsRange else {
                return
            }
            
            if gesture.didTapAttributedTextInLabel(label: self.m_lblTerms, inRange: range) {
                let webVC = TOWebViewController(url: URL(string: BaseUrl + "terms")!)
                webVC.showPageTitles = false
                webVC.title = "Terms & Conditions"
                let navi = UINavigationController(rootViewController: webVC)
                self.present(navi, animated: true, completion: nil)
            }
        }
    }
    
    //MARK:- Action Sign Up
    @objc func actionSignUp() {

        if dataValidation() {
            AppSingleton.shared.startLoading()
            
            let params = ["first_name":dictData!["firstName"]!,"last_name":dictData!["lastName"]!,"email":dictData!["email"]!,"mobile_no":dictData!["mobile"]!,"password":dictData!["password"]!,"gender":dictData!["gender"]!,"firebase_token":"123","device_type":"0","app_version":getAppVersion(),"mobile_cc":dialCode]
            
            Alamofire.request(AppConstant.API.register, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
                switch response.result {
                case .success:
                    if let value = response.result.value {
                        AppSingleton.shared.stopLoading()
                        let responseObject = JSON(value).dictionaryValue
                        if responseObject["status"]?.intValue == 0 {
                            AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                        } else if responseObject["status"]?.intValue == 3 {
                            AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                        } else {
                            UserDefaults.standard.set(encodable: UserModal(data: JSON(value)), forKey: "userData")
                            UserDefaults.standard.storeToken(token: responseObject["data"]!["token"].stringValue)
                            UserDefaults.standard.setUserType(type: UserType.Joinee.rawValue)
                            //====
                                                    
                            appDelegate.setRootAfterLogin()
                                                    
                            //====
                            
                        }
                    }
                case .failure(let error):
                    print(error)
                }
            }
        } 
    }
    
    //MARK:- Get app version number
    func getAppVersion() -> String {
        return (Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String)!
    }
    
    func dataValidation() -> Bool {
        self.view.endEditing(true)
        if (dictData!["firstName"]?.isEmpty)! {
            AppSingleton.shared.showErrorAlert(msg: "First Name Required.")
            return false
        } else if (dictData!["lastName"]?.isEmpty)! {
            AppSingleton.shared.showErrorAlert(msg: "Last Name Required.")
            return false
        } else if (dictData!["email"]?.isEmpty)! {
            AppSingleton.shared.showErrorAlert(msg: "Email Required.")
            return false
        } else if (dictData!["mobile"]?.isEmpty)! {
            AppSingleton.shared.showErrorAlert(msg: "Mobile Required.")
            return false
        } else if ((dictData!["mobile"]?.count)! < 7 || (dictData!["mobile"]?.count)! > 10) {
            AppSingleton.shared.showErrorAlert(msg: "Mobile Number Invalid.")
            return false
        } else if (dictData!["gender"]?.isEmpty)! {
            AppSingleton.shared.showErrorAlert(msg: "Gender Required.")
            return false
        } else if (dictData!["password"]?.isEmpty)! {
            AppSingleton.shared.showErrorAlert(msg: "Password Required.")
            return false
        } else if (dictData!["confirmpassword"]?.isEmpty)! {
            AppSingleton.shared.showErrorAlert(msg: "Confirm Password Required.")
            return false
        }
        return true
    }
    
    @objc func openCountryPicker(_ sender:UIButton) {
        let picker = MICountryPicker()
        picker.delegate = self
        picker.title = "Select Country Code"
        picker.showCallingCodes = true
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.pushViewController(picker, animated: true)
        
    }
}

extension SignUpVC: MICountryPickerDelegate {
    func countryPicker(_ picker: MICountryPicker, didSelectCountryWithName name: String, code: String) {
        print(code)
    }
    
    func countryPicker(_ picker: MICountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        picker.popVC()
        print(dialCode)
        self.dialCode = dialCode
        m_tblSignUp.reloadData()
    }
}

extension SignUpVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return structSignUp!.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SignUpPhoneCell", for: indexPath) as? SignUpPhoneCell
            cell?.m_btnCode.addTarget(self, action: #selector(openCountryPicker(_:)), for: .touchUpInside)
            cell?.m_btnCode.setTitle(dialCode, for: .normal)
//            cell?.m_viewContain.addCornerRadius(radius: (cell?.m_viewContain.frame.height)! / 7)
            cell?.m_viewContain.addShadowDefault(isOnlyRightSide: false)
           // cell?.m_viewContain.addShadow(offset: .zero, color: .lightGray, opacity: 0.6, radius: 2)
            
            cell?.m_txtFldMobile.placeholder = structSignUp![indexPath.row].placeHolders
            cell?.m_txtFldMobile.delegate = self
            cell?.m_txtFldMobile.tag = indexPath.row
            cell?.m_txtFldMobile.text = dictData![structSignUp![indexPath.row].key!]
            cell?.m_txtFldMobile.isSecureTextEntry = structSignUp![indexPath.row].isSecuredEntry!
            cell?.m_txtFldMobile.keyboardType = structSignUp![indexPath.row].kbType!
            cell?.m_txtFldMobile.font = UIFont().RobotoRegular(size: 16.0)

            cell?.m_btnCode.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            cell?.m_btnCode.titleLabel?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            cell?.m_btnCode.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            
            return cell!
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SignUpCell", for: indexPath) as? SignUpCell
            cell?.m_txtFld.placeholder = structSignUp![indexPath.row].placeHolders
            cell?.m_txtFld.delegate = self
            cell?.m_txtFld.tag = indexPath.row
            cell?.m_txtFld.text = dictData![structSignUp![indexPath.row].key!]
            cell?.m_txtFld.isSecureTextEntry = structSignUp![indexPath.row].isSecuredEntry!
            cell?.m_txtFld.keyboardType = structSignUp![indexPath.row].kbType!
            cell?.m_txtFld.font = UIFont().RobotoRegular(size: 16.0)
            cell?.m_imgView.image = structSignUp![indexPath.row].images
//            cell?.m_viewContain.addCornerRadius(radius: (cell?.m_viewContain.frame.height)! / 5)
            cell?.m_viewContain.addShadowDefault(isOnlyRightSide: false)
            //cell?.m_viewContain.addShadow(offset: .zero, color: .lightGray, opacity: 0.6, radius: 2)
            cell?.m_imgDropdown.isHidden = structSignUp![indexPath.row].isDropDown!
            if indexPath.row == 4 {
                dropDown.anchorView = cell?.m_txtFld
                dropDown.dataSource = ["Male", "Female"]
                dropDown.width = cell?.m_viewContain.frame.width
                dropDown.bottomOffset = CGPoint(x: -((cell?.m_viewContain.frame.origin.x)!), y:(cell?.m_viewContain.frame.height)!)
                dropDown.selectionAction = { (index:Int, item:String) in
                    cell?.m_txtFld.text = item
                    self.dictData![self.structSignUp![indexPath.row].key!] = "\(index)"
                }
            }
            return cell!
        }
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension SignUpVC: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        dictData![structSignUp![textField.tag].key!] = textField.text
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 4 {
            dropDown.show()
            return false
        }
        return true
    }
}
