//
//  SignUpPhoneCell.swift
//  Meetup App
//
//  Created by Birju Bhatt on 16/05/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class SignUpPhoneCell: UITableViewCell {

    @IBOutlet weak var m_viewContain: UIView!
    @IBOutlet weak var m_txtFldMobile: UITextField!
    @IBOutlet weak var m_btnCode: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        m_btnCode.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
