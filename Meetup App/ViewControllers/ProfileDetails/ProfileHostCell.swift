//
//  ProfileHostCell.swift
//  Meetup App
//
//  Created by Gaurang Vyas on 24/03/20.
//  Copyright © 2020 Birju Bhatt. All rights reserved.
//

import UIKit
import Kingfisher

class ProfileHostCell: UITableViewCell {
    @IBOutlet weak var m_wraperView: UIView!
    @IBOutlet weak var m_imgView: UIImageView!
    @IBOutlet weak var m_viewDate: UIView!
    @IBOutlet weak var m_lblDate: UILabel!
    @IBOutlet weak var m_lblName: UILabel!
    @IBOutlet weak var m_btnTheme: UIButton!
    @IBOutlet weak var m_lblPrice: UILabel!
    @IBOutlet weak var m_imgViewHost: UIImageView!
    @IBOutlet weak var m_btnMore: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        m_lblName.font = UIFont().RobotoMedium(size: 20.0)
        m_lblDate.font = UIFont().RobotoMedium(size: 16.0)
        m_lblPrice.font = UIFont().RobotoMedium(size: 18.0)
    }
    
    func setValues(info: HostDetailInfo.Event) {
        m_btnTheme.addCornerRadius(radius: 5.0)
        m_btnTheme.addBorder(width: 1, color: UIColor().convertHexStringToColor(hexString: "202564"))
        m_btnTheme.setTitle(info.theme, for: .normal)
        m_btnTheme.contentEdgeInsets = UIEdgeInsets(top: 5, left: 8, bottom: 5, right: 8)
        let priceValue = String(format: "%.3f", info.price ?? 0.0)
        m_lblPrice.text = "\(info.currencyReadable ?? "USD") \(priceValue)"
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.m_viewDate.addCornerRadiusParticularSide(rectCorner: [.topLeft,.bottomLeft], radius: ((self.m_viewDate.frame.height)/2))
        }
        let partyTime = (info.start?.timeStampToDate(format: "hh:mm a"))! + " to " + (info.end?.timeStampToDate(format: "hh:mm a"))!
        m_lblDate.text = (info.date?.timeStampToDate(format: "dd MMM yyyy"))! + " / " + partyTime + " / " + (info.city ?? "")

        m_lblName.text = info.name
        m_imgView.kf.setImage(with: URL(string: info.image ?? ""), placeholder: UIImage(named: "image01"), options: [], progressBlock: nil, completionHandler: { (result) in
            self.m_imgView.addCornerRadius(radius: 10.0)
        })
        m_imgViewHost.kf.setImage(with: URL(string: info.hostImage ?? ""), placeholder: UIImage(named: "profile"), options: [], progressBlock: nil, completionHandler: { (result) in
            self.m_imgViewHost.addCornerRadius(radius: (self.m_imgViewHost.frame.size.width)/2)
        })
        m_wraperView.addShadowWithCorner(cornerRadius: 10.0, offset: .zero, color: .lightGray , radius: 4.0, opacity: 0.6)
        
        m_btnMore.layer.cornerRadius = m_btnMore.frame.width / 2
        m_btnMore.backgroundColor = (info.type == .attended) ? UIColor.attendedColor : UIColor.organisedColor
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
