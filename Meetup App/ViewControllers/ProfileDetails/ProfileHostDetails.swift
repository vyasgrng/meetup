//
//  ProfileHostDetails.swift
//  Meetup App
//
//  Created by Gaurang Vyas on 24/03/20.
//  Copyright © 2020 Birju Bhatt. All rights reserved.
//


import Foundation

// MARK: - Welcome
struct HostDetailInfo: Codable {
    let status: Int?
    let message: String?
    let data: DataClass?
    
    // MARK: - DataClass
    struct DataClass: Codable {
        let host: Host?
    }

    // MARK: - Host
    struct Host: Codable {
        let firstName, lastName: String?
        let profilePic: String?
        let email, mobileNo, mobileCc, dob: String?
        let id: String?
        let events: [Event]?
        let reviews: [Review]?
        let organisedCount, attendedCount: Int?

        enum CodingKeys: String, CodingKey {
            case firstName = "first_name"
            case lastName = "last_name"
            case profilePic = "profile_pic"
            case email
            case mobileNo = "mobile_no"
            case mobileCc = "mobile_cc"
            case dob, id, events, reviews
            case organisedCount = "organised_count"
            case attendedCount = "attended_count"
        }
    }

    // MARK: - Event
    struct Event: Codable {
        let id, name, theme: String?
        let image: String?
        let price: Double?
        let currencyReadable: String?
        let lat, lng: Double?
        let hitlisted, isAvailable: Bool?
        let status: Int?
        let qr: String?
        let likeCount, reviewCount: Int?
        let date, start, end: Double?
        let city: String?
        let hostImage: String?
        let hostID: String?
        let updatable: Bool?
        let type: MemberType?

        enum CodingKeys: String, CodingKey {
            case id, name, theme, image, price
            case currencyReadable = "currency_readable"
            case lat, lng, hitlisted
            case isAvailable = "is_available"
            case status, qr
            case likeCount = "like_count"
            case reviewCount = "review_count"
            case date, start, end, city
            case hostImage = "host_image"
            case hostID = "host_id"
            case updatable, type
        }
    }

    enum MemberType: String, Codable {
        case attended = "attended"
        case organised = "organised"
    }

    // MARK: - Review
    struct Review: Codable {
        let firstName, lastName: String?
        let profilePic: String?
        let message: String?
        let createdAt: Int?

        enum CodingKeys: String, CodingKey {
            case firstName = "first_name"
            case lastName = "last_name"
            case profilePic = "profile_pic"
            case message
            case createdAt = "created_at"
        }
    }

}


