//
//  ProfileDetailsViewController.swift
//  Meetup App
//
//  Created by Gaurang Vyas on 24/03/20.
//  Copyright © 2020 Birju Bhatt. All rights reserved.
//
import Alamofire
import UIKit
import NVActivityIndicatorView

class ProfileDetailsViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var totalAttendedLabel: UILabel!
    @IBOutlet var totalOrganisedLabel: UILabel!
    @IBOutlet var attendedView: UIView!
    @IBOutlet var organisedView: UIView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var avatarImageView: UIImageView!
    @IBOutlet var navigationBar: UINavigationBar!
    @IBOutlet var statusBarView: UIView!
    
    static func getInstance(hostId: String) -> ProfileDetailsViewController {
        var viewController: ProfileDetailsViewController?
        let storyboard = UIStoryboard(name: "Tabbar", bundle: nil)
        if #available(iOS 13.0, *) {
            viewController = storyboard.instantiateViewController(identifier: "profileDetailsViewController") as? ProfileDetailsViewController
        } else {
            viewController = storyboard.instantiateViewController(withIdentifier: "profileDetailsViewController") as? ProfileDetailsViewController
        }
        viewController?.hostId = hostId
        return viewController!
    }
    
    var hostId: String?
    
    private var hostInfo: HostDetailInfo.DataClass? {
        didSet {
            self.setValues()
        }
    }
    
    private var eventArray: [HostDetailInfo.Event] = [] {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        self.hostInfo = nil
        self.dataRequest()
    }
    
    private func setupUI() {
        statusBarView.backgroundColor = .attendedColor
        navigationBar.barTintColor = .attendedColor
        navigationBar.tintColor = UIColor.white
        navigationBar.isTranslucent = false
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont().RobotoMedium(size: 20.0)
        label.text = "Profile Details"
        let navItems = UINavigationItem()
        navItems.leftBarButtonItems = [
            UIBarButtonItem(image: #imageLiteral(resourceName: "ic_back.png"), style: .plain, target: self, action: #selector(backPressed)),
            UIBarButtonItem(customView: UIView(frame: CGRect(origin: .zero, size: CGSize(width: 10, height: 10)))),
            UIBarButtonItem.init(customView: label)]
        navigationBar.items = [navItems]
        nameLabel.textColor = .attendedColor
        organisedView.backgroundColor = .organisedColor
        attendedView.backgroundColor = .attendedColor
    }
    
    private func setValues() {
        guard let info = self.hostInfo, let hostInfo = info.host else {
            self.tableView.tableHeaderView?.isHidden = true
            return
        }
        self.tableView.tableHeaderView?.isHidden = false
        avatarImageView.kf.indicatorType = .activity
        avatarImageView.kf.setImage(with: URL(string: hostInfo.profilePic ?? ""))
        nameLabel.text = [hostInfo.firstName, hostInfo.lastName].compactMap({$0}).joined(separator: " ")
        totalAttendedLabel.text = String(hostInfo.attendedCount ?? 0)
        totalOrganisedLabel.text = String(hostInfo.organisedCount ?? 0)
        if let headerView = tableView.tableHeaderView {
            let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            headerView.frame.size.height = height
            headerView.layoutIfNeeded()
        }
        self.eventArray = hostInfo.events ?? []
        organisedView.layer.cornerRadius = organisedView.bounds.height / 2
        attendedView.layer.cornerRadius = attendedView.bounds.height / 2
        
    }
    
    //MARK:- Set initial UI

    @objc private func backPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func dataRequest() {
        startLoading()
      
        guard let hostId = self.hostId else {
            return
        }
        
        let params = ["token":UserDefaults.standard.currentToken()!,"id":hostId]
               Alamofire.request(AppConstant.API.hostDetail, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
            self.stopLoading()
            switch response.result {
            case .success:
                do {
                    let data = try JSONDecoder().decode(HostDetailInfo.self, from: response.data!)
                    self.hostInfo = data.data
                } catch(let error) {
                    print(error.localizedDescription)
                    AppSingleton.shared.showErrorAlert(msg: "Something went wrong!")
                }
                
            break
            case .failure(let error):
                print(error)
                AppSingleton.shared.showErrorAlert(msg: "Something went wrong!")
            }
        }
    }
    
    func startLoading() {
        let blackView = UIView()
        blackView.tag = 4443
        blackView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        blackView.frame = appDelegate.window!.frame
        navigationController?.view?.addSubview(blackView)
        
        let loadingView = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100), type: .ballTrianglePath, color: .white, padding: 10)
        loadingView.tag = 4444
        loadingView.center = (blackView.center)
        loadingView.startAnimating()
        blackView.addSubview(loadingView)
    }
    
    func stopLoading() {
        let blackView = navigationController?.view?.viewWithTag(4443)
        blackView?.removeFromSuperview()
    }

}

extension ProfileDetailsViewController: UITableViewDataSource, UITableViewDelegate {
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "eventCell", for: indexPath) as! ProfileHostCell
        cell.setValues(info: eventArray[indexPath.row])
        return cell
    }
}
