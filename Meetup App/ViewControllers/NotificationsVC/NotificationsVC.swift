//
//  NotificationsVC.swift
//  Meetup App
//
//  Created by STL on 29/12/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class NotificationsVC: BaseVC {

    @IBOutlet weak var m_constTop: NSLayoutConstraint!
    @IBOutlet weak var m_tblViewNotifications: UITableView!
    @IBOutlet weak var m_imgViewEmptyEvent: UIImageView!
    
    var arrayNotificationsList:Array<JSON> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
    }
    
    //MARK:- Set initial UI
    func basicSetUp() {
        
        m_constTop.constant = topView.frame.height + 15
        addBackButton(withImage: #imageLiteral(resourceName: "ic_back").maskWithColor(color: .white)!,isFirstHierarchy: false)
        addTopTitle(title: "Notifications")
        
        //Get enjoted list
        notificationsListAPI()
        self.m_tblViewNotifications.delegate = self
        self.m_tblViewNotifications.dataSource = self
        self.m_tblViewNotifications.tableFooterView = UIView()
    }
    
    
    override func backClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Notifications List API
    func notificationsListAPI() {
        AppSingleton.shared.startLoading()
        var params = ["":""]
        var userType = "host"
        if UserDefaults.standard.currentUserType() == UserType.Joinee.rawValue {
            userType = "guest"
        }
        
        params = ["token":UserDefaults.standard.currentToken()!,"page":"0","type":userType]
    
        Alamofire.request(AppConstant.API.notifications, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        self.arrayNotificationsList = responseObject["data"]!["notifications"].arrayValue
                        self.m_imgViewEmptyEvent.isHidden = true
                        self.m_tblViewNotifications.isHidden = false
                        if self.arrayNotificationsList.count == 0 {
                            self.m_imgViewEmptyEvent.isHidden = false
                            self.m_tblViewNotifications.isHidden = true
                        }
                        self.m_tblViewNotifications.reloadData()
                    }
                } else {
                    AppSingleton.shared.stopLoading()
                }
            case .failure(let error):
                print(error)
                AppSingleton.shared.stopLoading()
            }
        }
    }
}

extension NotificationsVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayNotificationsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationsCell", for: indexPath) as? NotificationsCell
        let data = arrayNotificationsList[indexPath.row].dictionaryValue
        cell?.m_viewContain.addShadow(offset: .zero, color: .lightGray, opacity: 0.6, radius: 2)
        cell?.m_lblTitle.text = data["message"]?.stringValue
        cell?.m_lblSubtitle.text = (data["created_at"]?.doubleValue.timeStampToDate(format: "yyyy MMM dd hh:mm a"))
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

