

//
//  NotificationsCell.swift
//  Meetup App
//
//  Created by STL on 29/12/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class NotificationsCell: UITableViewCell {

    @IBOutlet weak var m_lblSubtitle: UILabel!
    @IBOutlet weak var m_lblTitle: UILabel!
    @IBOutlet weak var m_viewContain: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
