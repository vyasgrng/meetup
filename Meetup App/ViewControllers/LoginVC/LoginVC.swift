//
//  LoginVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 14/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class LoginVC: UIViewController {

    @IBOutlet weak var m_imgUserProfilePhoto: UIImageView!
    
    @IBOutlet weak var m_txtFldPwd: UITextField!
    @IBOutlet weak var m_btnLogin: UIButton!
    @IBOutlet weak var m_btnForgotPwd: UIButton!
    @IBOutlet weak var m_viewUsername: UIView!
    @IBOutlet weak var m_txtFldUname: UITextField!
    @IBOutlet weak var m_btnPolicy: UIButton!
    @IBOutlet weak var m_btnTerms: UIButton!
    @IBOutlet weak var m_viewBottom: UIView!
    @IBOutlet weak var m_viewPwd: UIView!
    @IBOutlet weak var m_imgViewUname: UIImageView!
    @IBOutlet weak var m_imgViewPwd: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    //MARK: Back Action
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.popVC()
    }
    
    //MARK:- Set initial UI
    func basicSetUp() {
       //call api for profile image
        m_viewBottom.addCornerRadiusParticularSide(rectCorner: [.topLeft,.topRight], radius: m_viewBottom.frame.size.width/5)
        m_viewUsername.addCornerRadius(radius: 5.0)
        m_viewUsername.addBorder(width: 1.0, color: .white)
        m_viewPwd.addCornerRadius(radius: 5.0)
        m_viewPwd.addBorder(width: 1.0, color: .white)
        m_btnLogin.addCornerRadius(radius: 5.0)
        let imgUname = m_imgViewUname.image?.maskWithColor(color: .white)
        let imgPwd = m_imgViewPwd.image?.maskWithColor(color: .white)
        
        m_imgViewUname.image = imgUname
        m_imgViewPwd.image = imgPwd
        
    //        m_txtFldUname.text = "birjubhatt04+11@gmail.com"
    //        m_txtFldPwd.text = "123456"
    }
    
    //MARK:- Get app version number
    func getAppVersion() -> String {
        return (Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String)!
    }
    //MARK:- Action login
    @IBAction func actionLogin(_ sender: UIButton) {
        
        if dataValidation() {
            let params = ["email":m_txtFldUname.text!,"password":m_txtFldPwd.text!,"firebase_token":"123","device_type":"0","app_version":getAppVersion()]
            
            AppSingleton.shared.startLoading()
            Alamofire.request(AppConstant.API.login, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
                switch response.result {
                case .success:
                    if let value = response.result.value {
                        
                        AppSingleton.shared.stopLoading()

                        let responseObject = JSON(value).dictionaryValue
                        
                        if responseObject["status"]?.intValue == 0 {
                            AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                        } else {
                            UserDefaults.standard.set(encodable: UserModal(data: JSON(value)), forKey: "userData")
                            UserDefaults.standard.storeToken(token: responseObject["data"]!["token"].stringValue)
                            UserDefaults.standard.setUserType(type: UserType.Joinee.rawValue)
                            //developer 2
                            appDelegate.socketConnection()
                            //
                            //====
                            
                            appDelegate.setRootAfterLogin()
                            
                            //====
                        }
                    }
                case .failure(let error):
                    print(error)
                    AppSingleton.shared.stopLoading()
                }
            }
        }
    }
    
    func dataValidation() -> Bool {
        self.view.endEditing(true)
        
        if (m_txtFldUname.text?.isEmpty)! {
            AppSingleton.shared.showErrorAlert(msg: "Username Required.")
            return false
        }
        if m_txtFldUname.text?.isValidEmail() == false {
            AppSingleton.shared.showErrorAlert(msg: "Please enter valid email address")
            return false
        }
        
        if (m_txtFldPwd.text!.isEmpty) {
            AppSingleton.shared.showErrorAlert(msg: "Password Required.")
            return false
        }
        return true
    }
    
    // MARK:- Action go to Forgot Password
    @IBAction func actionForgotPwd(_ sender: UIButton) {
        let forgotPwdObjVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPwdVC") as? ForgotPwdVC
        self.pushVC(destinationVC: forgotPwdObjVC!)
    }
}
