//
//  CalendarVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 18/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import FSCalendar
import Alamofire
import SwiftyJSON

class CalendarVC: BaseVC {
    
    var monthYearStr = ""
    var arrayDates:Array<JSON> = []
    var arrayEvents:Array<JSON> = []
    
    @IBOutlet weak var m_tblViewCalendar: UITableView!
    @IBOutlet weak var m_constTblTop: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Call basic set up
        //developer 2 changed delay time from 1 to 7
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.07) {
//            self.basicSetUp()
//        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.basicSetUp()

    }
    
    //MARK:- Set initial UI
    func basicSetUp() {
        addTopTitle(title: "Calendar")
        m_tblViewCalendar.tableFooterView = UIView()
        let values = Calendar.current.dateComponents([Calendar.Component.month, Calendar.Component.year], from: getCalendarObj().currentPage)
        setMonthYearDisplay(month: getMonthName(monthDigit: values.month!), year: "\(values.year!)")
        m_constTblTop.constant = topView.frame.height
        
        calendarAPI(month: values.month!, year: values.year!)
        
    }
    
    //MARK:- Get calendar data
    func calendarAPI(month:Int,year:Int) {
        AppSingleton.shared.startLoading()
        var type = "joined"
        if UserDefaults.standard.currentUserType() == UserType.Host.rawValue {
           type = "posted"
        }
        let params = ["token":UserDefaults.standard.currentToken()!,"month":"\(month)","year":"\(year)","type":type]
        Alamofire.request(AppConstant.API.calendar, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        self.arrayDates = responseObject["data"]!["dates"].arrayValue
                        self.m_tblViewCalendar.reloadData()
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    // MARK:- Action next month
    @IBAction func actionNext(_ sender: UIButton) {
        getCalendarObj().setCurrentPage(getNextMonth(date: getCalendarObj().currentPage), animated: true)
    }
    
    // MARK:- Action previous month
    @IBAction func actionPrevious(_ sender: UIButton) {
        getCalendarObj().setCurrentPage(getPreviousMonth(date: getCalendarObj().currentPage), animated: true)
    }
    
    func getNextMonth(date:Date)->Date {
        return  Calendar.current.date(byAdding: .month, value: 1, to:date)!
    }
    
    func getPreviousMonth(date:Date)->Date {
        return  Calendar.current.date(byAdding: .month, value: -1, to:date)!
    }
    
    func getCalendarObj() -> FSCalendar {
        let calendarObj = m_tblViewCalendar.cellForRow(at: IndexPath(item: 0, section: 0)) as? CalendarCell
        return (calendarObj?.m_calendar)!
    }
    
    func setMonthYearDisplay(month:String, year:String) {
        monthYearStr = "\(month) \(year)"
        m_tblViewCalendar.reloadData()
    }
    
    func getMonthName(monthDigit:Int) -> String {
        switch monthDigit {
        case 1:
            return "January"
        case 2:
            return "February"
        case 3:
            return "March"
        case 4:
            return "April"
        case 5:
            return "May"
        case 6:
            return "June"
        case 7:
            return "July"
        case 8:
            return "August"
        case 9:
            return "September"
        case 10:
            return "October"
        case 11:
            return "November"
        case 12:
            return "December"
        default:
            break
        }
        return ""
    }
}

extension CalendarVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayEvents.count + 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CalendarCell") as? CalendarCell
            cell?.m_calendar.delegate = self
            cell?.m_calendar.dataSource = self
            cell?.m_calendar.appearance.titleFont = UIFont().RobotoRegular(size: 17)
//            cell?.m_calendar.appearance.todayColor = UIColor().convertHexStringToColor(hexString: "2875D0")
            cell?.m_calendar.appearance.headerTitleFont = UIFont().RobotoBold(size: 19)
            cell?.m_calendar.appearance.headerTitleColor = .black
            cell?.m_calendar.appearance.weekdayFont = UIFont().RobotoMedium(size: 17)
            cell?.m_calendar.appearance.weekdayTextColor = .black
            cell?.m_btnLeft.backgroundColor = UIColor().convertHexStringToColor(hexString: "dbefff")
            cell?.m_btnRight.backgroundColor = UIColor().convertHexStringToColor(hexString: "dbefff")
            let radius = (cell?.m_btnLeft.frame.height)! / 5
            cell?.m_btnLeft.addCornerRadius(radius: radius)
            cell?.m_btnRight.addCornerRadius(radius: radius)
            cell?.m_calendar.headerHeight = 0.0
            cell?.m_lblMonthYear.text = monthYearStr
            cell?.m_calendar.reloadData()
            return cell!
        } else {
            let data = self.arrayEvents[indexPath.row - 1].dictionaryValue
            let cell = tableView.dequeueReusableCell(withIdentifier: "CalendarEventCell") as? CalendarEventCell
            if indexPath.row == 1 {
                cell?.m_lblMeetupList.text = "Meetup List"
            } else {
                cell?.m_lblMeetupList.text = ""
            }
            cell?.m_lblName.text = data["name"]?.stringValue
            cell?.m_lblTheme.text = data["theme"]?.stringValue
            cell?.m_lblTime.text = (data["start"]?.stringValue)! + " - " + (data["end"]?.stringValue)!
            cell?.m_imgViewEvent.kf.setImage(with: data["image"]?.url)
            cell?.m_imgViewEvent.addCornerRadius(radius: 5)
            cell?.m_imgViewEvent.clipsToBounds = true
            return cell!
        }
    }
}

extension CalendarVC: FSCalendarDelegate, FSCalendarDataSource {
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        AppSingleton.shared.startLoading()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        var type = "joined"
        if UserDefaults.standard.currentUserType() == UserType.Host.rawValue {
            type = "posted"
        }
        let params = ["token":UserDefaults.standard.currentToken()!,"date":dateFormatter.string(from: date),"type":type]
        Alamofire.request(AppConstant.API.onThisDay, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        self.arrayEvents = responseObject["data"]!["events"].arrayValue
                        self.m_tblViewCalendar.reloadData()
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        let values = Calendar.current.dateComponents([Calendar.Component.month, Calendar.Component.year], from: calendar.currentPage)
        setMonthYearDisplay(month: getMonthName(monthDigit: values.month!), year: "\(values.year!)")
        self.calendarAPI(month: values.month!, year: values.year!)
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        for dateStr in arrayDates {
            if(dateFormatter.string(from: date) == dateStr.stringValue) {
                return 1
            }
        }
        return 0
    }
}
