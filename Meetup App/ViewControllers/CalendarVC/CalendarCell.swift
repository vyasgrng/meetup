//
//  CalendarCell.swift
//  Meetup App
//
//  Created by Birju Bhatt on 21/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import FSCalendar

class CalendarCell: UITableViewCell {

    @IBOutlet weak var m_lblMonthYear: UILabel!
    @IBOutlet weak var m_btnRight: UIButton!
    @IBOutlet weak var m_btnLeft: UIButton!

    @IBOutlet weak var m_calendar: FSCalendar!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
