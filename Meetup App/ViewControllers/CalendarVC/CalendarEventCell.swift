//
//  CalendarEventCell.swift
//  Meetup App
//
//  Created by Birju Bhatt on 21/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class CalendarEventCell: UITableViewCell {

    @IBOutlet weak var m_lblMeetupList: UILabel!
    @IBOutlet weak var m_imgViewEvent: UIImageView!
    @IBOutlet weak var m_lblName: UILabel!
    @IBOutlet weak var m_lblTheme: UILabel!
    @IBOutlet weak var m_lblTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
