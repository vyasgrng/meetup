//
//  TabbarVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 20/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class TabbarVC: UITabBarController {

//    fileprivate lazy var defaultTabBarHeight = { tabBar.frame.size.height }()
    var centerButton = UIButton(type: .custom)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.delegate = self
        let font = UIFont().RobotoMedium(size: 12)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
        UITabBarItem.appearance().titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 0)
        UITabBar.appearance().tintColor = UIColor(red: 70, green: 29, blue: 143, alpha: 1.0)
        if UserDefaults.standard.currentUserType() == UserType.Joinee.rawValue {
            self.viewControllers = [caledndarTab,hitListTab,enjoyeTab,inboxTab,profileTab]
            self.selectedIndex = 2
        } else {
            self.viewControllers = [invitationTab,onGoingTab,caledndarTab,inboxTab,profileTab]
//            self.viewControllers = [invitationTab,onGoingTab,inboxTab]
            self.selectedIndex = 1
        }
        applyShadow()
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        self.view.bringSubviewToFront(self.tabBar)
//        self.addCenterButton()
//    }
    
    private func addCenterButton() {
        let button = UIButton(type: .custom)
        button.backgroundColor = .red
        let square = self.tabBar.frame.size.height
        button.frame = CGRect(x: 0, y: -30, width: square, height: square)
        button.center = self.tabBar.center
        
        self.view.addSubview(button)
        self.view.bringSubviewToFront(button)
        
        button.addTarget(self, action: #selector(didTouchCenterButton(_:)), for: .touchUpInside)
    }
    
    @objc
    private func didTouchCenterButton(_ sender: AnyObject) {
        print("!!")
    }
    
//    override func viewWillLayoutSubviews() {
//        super.viewWillLayoutSubviews()
//        let newTabBarHeight = defaultTabBarHeight + 16.0
//        var newFrame = tabBar.frame
//        newFrame.size.height = newTabBarHeight
//        newFrame.origin.y = view.frame.size.height - newTabBarHeight
//        tabBar.frame = newFrame
//    }
    
    func applyShadow() {
        self.tabBar.layer.shadowColor = UIColor.lightGray.cgColor
        self.tabBar.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.tabBar.layer.shadowRadius = 4
        self.tabBar.layer.shadowOpacity = 0.8
        self.tabBar.layer.masksToBounds = false
    }
    
    // Tabs for Joinee
    lazy public var enjoyeTab: EnjoyedVC = {
        
        let enjoyedTabBar = UIStoryboard(name: "Tabbar", bundle: Bundle.main).instantiateViewController(withIdentifier: "EnjoyedVC") as? EnjoyedVC
        let title:String? = "Dashboard" //Enjoyed
        //enjoy_tab
//        let defaultImage = UIImage(named: "ic_enjoyed_grey")!
//        let selectedImage = UIImage(named: "enjoyed_ic")!
        let defaultImage = UIImage()
            //UIImage(named: "ic_invitation_grey")!
        let selectedImage = UIImage()
            //UIImage(named: "ic_invitation_grey")!

        let tabBarItems = (title: title, image: defaultImage.withRenderingMode(.alwaysOriginal), selectedImage: selectedImage.withRenderingMode(.alwaysOriginal))
        let tabBarItem = UITabBarItem(title: tabBarItems.title, image: tabBarItems.image, selectedImage: tabBarItems.selectedImage)
        //tabBarItem.imageInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: -16, right: 0)
        
        if UserDefaults.standard.currentUserType() == UserType.Joinee.rawValue {
            //developer2
            self.addCenterButton(withImage : UIImage(named: "noun_explore_1048055")!, highlightImage: UIImage(named: "noun_explore_1048055")!)
        }
        enjoyedTabBar!.tabBarItem = tabBarItem
        
        
        
        return enjoyedTabBar!
    }()
    lazy public var hitListTab: HitlistVC = {
        
        let hitListTabBar = UIStoryboard(name: "Tabbar", bundle: Bundle.main).instantiateViewController(withIdentifier: "HitlistVC") as? HitlistVC
        
        let title = "Hitlist"
        
        let defaultImage = UIImage(named: "noun_wish list_1956813")!
        let selectedImage = UIImage(named: "noun_wish list_1956813")!
        let tabBarItems = (title: title, image: defaultImage.withRenderingMode(.alwaysOriginal), selectedImage: selectedImage)
        let tabBarItem = UITabBarItem(title: tabBarItems.title, image: tabBarItems.image, selectedImage: tabBarItems.selectedImage)
        hitListTabBar!.tabBarItem = tabBarItem
        
        return hitListTabBar!
    }()
    lazy public var caledndarTab: CalendarVC = {
        
        let calendarTabBar = UIStoryboard(name: "Tabbar", bundle: Bundle.main).instantiateViewController(withIdentifier: "CalendarVC") as? CalendarVC
        let title = "Calendar"
        let defaultImage = UIImage(named: "ic_calendar_grey")!
        let selectedImage = UIImage(named: "ic_calendar")!
        let tabBarItems = (title: title, image: defaultImage.withRenderingMode(.alwaysOriginal), selectedImage: selectedImage)
        let tabBarItem = UITabBarItem(title: tabBarItems.title, image: tabBarItems.image, selectedImage: tabBarItems.selectedImage)
        calendarTabBar!.tabBarItem = tabBarItem
        
        return calendarTabBar!
    }()
    lazy public var inboxTab: InboxVC = {
        
        let inboxTabBar = UIStoryboard(name: "Tabbar", bundle: Bundle.main).instantiateViewController(withIdentifier: "InboxVC") as? InboxVC
        let title = "Inbox"
        let defaultImage = UIImage(named: "ic_inbox_grey")!
        let selectedImage = UIImage(named: "ic_inbox")!
        let tabBarItems = (title: title, image: defaultImage.withRenderingMode(.alwaysOriginal), selectedImage: selectedImage)
        let tabBarItem = UITabBarItem(title: tabBarItems.title, image: tabBarItems.image, selectedImage: tabBarItems.selectedImage)
        inboxTabBar!.tabBarItem = tabBarItem
        
        return inboxTabBar!
    }()
    lazy public var profileTab: ProfileVC = {
        
        let profileTabBar = UIStoryboard(name: "Tabbar", bundle: Bundle.main).instantiateViewController(withIdentifier: "ProfileVC") as? ProfileVC
        let title = "Profile"
        let defaultImage = UIImage(named: "ic_profile_grey")!
        let selectedImage = UIImage(named: "ic_profile")!
        let tabBarItems = (title: title, image: defaultImage.withRenderingMode(.alwaysOriginal), selectedImage: selectedImage)
        let tabBarItem = UITabBarItem(title: tabBarItems.title, image: tabBarItems.image, selectedImage: tabBarItems.selectedImage)
        profileTabBar!.tabBarItem = tabBarItem
        
        return profileTabBar!
    }()
    
    // Tabs for Host
    lazy public var invitationTab: InvitationVC = {
        
        let invitationTabBar = UIStoryboard(name: "Tabbar", bundle: Bundle.main).instantiateViewController(withIdentifier: "InvitationVC") as? InvitationVC
        let title = "Invitation"
        let defaultImage = UIImage(named: "noun_wish list_1956813")!
        let selectedImage = UIImage(named: "noun_wish list_1956813")!
        let tabBarItems = (title: title, image: defaultImage.withRenderingMode(.alwaysOriginal), selectedImage: selectedImage)
        let tabBarItem = UITabBarItem(title: tabBarItems.title, image: tabBarItems.image, selectedImage: tabBarItems.selectedImage)
        invitationTabBar!.tabBarItem = tabBarItem
        
        return invitationTabBar!
    }()
    lazy public var onGoingTab: OnGoingVC = {
        
        let onGoingTabBar = UIStoryboard(name: "Tabbar", bundle: Bundle.main).instantiateViewController(withIdentifier: "OnGoingVC") as? OnGoingVC
        let title = "On Going"
        let defaultImage = UIImage(named: "ic_enjoyed_grey")!
        let selectedImage = UIImage(named: "enjoyed_ic")!
        let tabBarItems = (title: title, image: defaultImage.withRenderingMode(.alwaysOriginal), selectedImage: selectedImage)
        let tabBarItem = UITabBarItem(title: tabBarItems.title, image: tabBarItems.image, selectedImage: tabBarItems.selectedImage)
        onGoingTabBar!.tabBarItem = tabBarItem
        
        return onGoingTabBar!
    }()
}

extension TabbarVC: UITabBarControllerDelegate {
    func addCenterButton(withImage buttonImage : UIImage, highlightImage: UIImage) {
        if !centerButton.isUserInteractionEnabled{
            centerButton.removeFromSuperview()
        }
        //centerButton.backgroundColor = UIColor().convertHexStringToColor(hexString: "EFEFEF").withAlphaComponent(0.6)
        centerButton.autoresizingMask = [.flexibleRightMargin, .flexibleTopMargin, .flexibleLeftMargin, .flexibleBottomMargin]
        centerButton.frame = CGRect(x: 0.0, y: 0.0, width: tabBar.frame.height-3, height: tabBar.frame.height-3)
        centerButton.setBackgroundImage(buttonImage, for: .normal)
        centerButton.setBackgroundImage(highlightImage, for: .highlighted)
        centerButton.isUserInteractionEnabled = false
        
        let heightdif: CGFloat = buttonImage.size.height - (self.tabBar.frame.size.height);
        
        if (heightdif < 0){
            centerButton.center = (self.tabBar.center)
        }
        else{
            var center: CGPoint = (self.tabBar.center)
            if #available(iOS 11.0, *) {
                let window = UIApplication.shared.keyWindow
                let bottomPadding = window?.safeAreaInsets.bottom
                if (bottomPadding ?? 27.0) > CGFloat(0.0){
                    centerButton.frame = CGRect(x: 0.0, y: 0.0, width: tabBar.frame.height+3, height: tabBar.frame.height+3)
                }
                center.y = center.y - (((bottomPadding ?? 34.0) > 0) ? (bottomPadding ?? 34) : 0)

            }
          //  center.y = center.y - 27
                //- 24
            centerButton.center = center
        }
        
        self.view.addSubview(centerButton)
        self.tabBar.bringSubviewToFront(centerButton)
        
//        if let count = self.tabBar.items?.count
//        {
//            let i = floor(Double(count / 2))
//            let item = self.tabBar.items![Int(i)]
//            item.title = ""
//        }
    }
}
