//
//  QRCamVC.swift
//  Meetup App
//
//  Created by STL on 07/01/20.
//  Copyright © 2020 Birju Bhatt. All rights reserved.
//

import UIKit
import AVFoundation


protocol QRCamDelegate {
    func scannedQRCode(qr_Code:String)
}

class QRCamVC: UIViewController {
    
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    var delegate:QRCamDelegate?
    let lightOff = UIImage(named: "lightbulbOff")?.maskWithColor(color: .white)
    let lightOn = UIImage(named: "lightbulbOn")
    let close = UIImage(named: "ic_close")?.maskWithColor(color: .white)
    let btnTorch = UIButton()
    let btnClose = UIButton()
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        camSetUp()
        addButtonsOnCam()
        
        //Add Notification for background
        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToForeground), name: UIApplication.didBecomeActiveNotification, object: nil)

                
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    //MARK:- Camera Set Up
    func camSetUp() {
        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)
        
        captureSession.startRunning()
    }
    //MARK:- Add Button on Camera
    func addButtonsOnCam() {
        btnTorch.setImage(lightOff, for: .normal)
        btnTorch.frame = CGRect(x: 0, y: 0, width: 150, height: 50)
        btnTorch.addTarget(self, action: #selector(actionTorch(_:)), for: .touchUpInside)
        btnTorch.addShadowWithCorner(cornerRadius: 3, offset: .zero, color: .white, radius: 4, opacity: 0.8)
        view.addSubview(btnTorch)
        
        btnTorch.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(30)
            make.bottom.equalToSuperview().offset(-30)
            make.width.equalTo(lightOff!.size.width + 10)
            make.height.equalTo(lightOff!.size.height + 10)
        }
        
        
        btnClose.setImage(close, for: .normal)
        btnClose.frame = CGRect(x: 0, y: 0, width: 150, height: 50)
        btnClose.addTarget(self, action: #selector(closeCamera(_:)), for: .touchUpInside)
        btnClose.addShadowWithCorner(cornerRadius: 3, offset: .zero, color: .white, radius: 4, opacity: 0.8)
        view.addSubview(btnClose)
        
        btnClose.snp.makeConstraints { (make) in
            make.trailing.equalToSuperview().offset(-30)
            make.bottom.equalToSuperview().offset(-30)
            make.width.equalTo(close!.size.width + 10)
            make.height.equalTo(close!.size.height + 10)
        }
    }
    //MARK:- Action Torch
    @objc func actionTorch(_ sender:UIButton) {
        if sender.isSelected {
            sender.setImage(lightOff, for: .normal)
        } else {
            sender.setImage(lightOn, for: .normal)
        }
        sender.isSelected = !sender.isSelected
        toggleTorch(on: sender.isSelected)
    }
    //MARK:- Close Camera
    @objc func closeCamera(_ sender:UIButton) {
        captureSession.stopRunning()
        dismiss(animated: true)
    }
    //MARK:- Torch On/Off
    func toggleTorch(on: Bool) {
        guard
            let device = AVCaptureDevice.default(for: AVMediaType.video),
            device.hasTorch
        else { return }

        do {
            try device.lockForConfiguration()
            device.torchMode = on ? .on : .off
            device.unlockForConfiguration()
        } catch {
            print("Torch could not be used")
        }
    }
    //MARK:- Camera/Scanning not supported.
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    //MARK:- Set portrait orientation
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    //MARK:- Called when app is in background
    @objc func appMovedToBackground() {
        captureSession.stopRunning()
        btnTorch.isSelected = false
        btnTorch.setImage(lightOff, for: .normal)
    }
    //MARK:- Called when app is in foreground
    @objc func appMovedToForeground() {
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }
}

//MARK:- AVCapture Delegate
extension QRCamVC: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }
        dismiss(animated: true)
    }
    
    func found(code: String) {
        print(code)
        delegate?.scannedQRCode(qr_Code: code)
    }
}
