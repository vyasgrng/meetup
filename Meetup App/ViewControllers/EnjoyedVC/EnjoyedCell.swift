//
//  EnjoyedCell.swift
//  Meetup App
//
//  Created by Birju Bhatt on 20/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

class EnjoyedCell: UITableViewCell {

    @IBOutlet weak var m_wraperView: UIView!
    @IBOutlet weak var m_imgView: UIImageView!
    @IBOutlet weak var m_viewDate: UIView!
    @IBOutlet weak var m_lblDate: UILabel!
    @IBOutlet weak var m_lblName: UILabel!
    @IBOutlet weak var m_btnTheme: UIButton!
    @IBOutlet weak var m_lblPrice: UILabel!
    @IBOutlet weak var m_lblLikeCount: UILabel!
    @IBOutlet weak var m_lblCommentCount: UILabel!
    @IBOutlet weak var m_btnJoin: UIButton!
    @IBOutlet weak var m_imgViewHost: UIImageView!
    @IBOutlet weak var m_btnMore: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        
        m_lblName.font = UIFont().RobotoMedium(size: 20.0)
        m_lblDate.font = UIFont().RobotoMedium(size: 16.0)
        m_lblPrice.font = UIFont().RobotoMedium(size: 18.0)
        m_lblLikeCount.font = UIFont().RobotoRegular(size: 15.0)
        m_lblCommentCount.font = UIFont().RobotoRegular(size: 15.0)
    }
}
