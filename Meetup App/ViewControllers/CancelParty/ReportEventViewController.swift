//
//  CancelEventViewController.swift
//  Meetup App
//
//  Created by Gaurang Vyas on 30/03/20.
//  Copyright © 2020 Birju Bhatt. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ReportEventViewController: UIViewController {
    
    static func getIntance(eventId: String) -> ReportEventViewController {
        let vc = StoryboardID.tabbar.getViewController(sceneId: "reportEventViewController") as! ReportEventViewController
        vc.eventId = eventId
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        return vc
    }
    
    @IBOutlet var reasonTextView: UITextView!
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var reportButton: UIButton!
    @IBOutlet var contentView: UIView!
    private var eventId: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAppearance()
    }
    
    private func setupAppearance() {
        reasonTextView.addBorder(width: 1, color: .lightGray)
        cancelButton.addBorder(width: 1, color: UIColor().convertHexStringToColor(hexString: "E94A47"))
        cancelButton.addCornerRadius(radius: 8)
    }
    
    @IBAction func cancelTapped() {
        self.dismiss(animated: true)
    }
    
    @IBAction func reportTapped() {
        self.view.endEditing(true)
        AppSingleton.shared.startLoading()
        
        let params: [String: String] = ["token":UserDefaults.standard.currentToken() ?? "",
                      "id":eventId,
                      "reason": reasonTextView.text ?? ""]
        
            Alamofire.request(AppConstant.API.report,
                              method: .post,
                              parameters: params as Parameters,
                              encoding: URLEncoding.default)
                .responseJSON { response in
                    
              AppSingleton.shared.stopLoading()
              switch response.result {
              case .success:
                if let value = response.result.value {
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        self.dismiss(animated: true) {
                            AppSingleton.shared.showSuccessAlert(msg: "Report Added")
                        }
                    }
                }
              break
              case .failure(let error):
                  print(error)
                  AppSingleton.shared.showErrorAlert(msg: "Something went wrong!")
              }
          }
    }
        
}
