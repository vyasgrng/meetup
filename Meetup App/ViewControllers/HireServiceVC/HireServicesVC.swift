//
//  HireServicesVC.swift
//  Meetup App
//
//  Created by STL on 01/01/20.
//  Copyright © 2020 Birju Bhatt. All rights reserved.
//

import UIKit

class HireServicesVC: BaseVC {

    @IBOutlet weak var m_viewCleaner: UIView!
    @IBOutlet weak var m_viewRestaurant: UIView!
    @IBOutlet weak var m_viewClub: UIView!
    @IBOutlet weak var m_viewTransportation: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        m_viewCleaner.addShadowDefault(isOnlyRightSide: true)
        m_viewRestaurant.addShadowDefault(isOnlyRightSide: true)
        m_viewClub.addShadowDefault(isOnlyRightSide: true)
        m_viewTransportation.addShadowDefault(isOnlyRightSide: true)
        
        addBackButton()
        addTopTitle(title: "Hire Services")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
