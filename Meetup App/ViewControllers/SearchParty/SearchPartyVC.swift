//
//  SearchPartyVC.swift
//  Meetup App
//
//  Created by STL on 22/12/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import GooglePlaces

protocol searchPartyDelegate {
    func searchParties(lat:String,long:String, dt:String)
}

class SearchPartyVC: BaseVC {

    @IBOutlet weak var m_constTop: NSLayoutConstraint!
    @IBOutlet weak var m_viewWhen: UIView!
    @IBOutlet weak var m_viewBudget: UIView!
    @IBOutlet weak var m_viewWhere: UIView!
    @IBOutlet weak var m_lblWhen: UILabel!
    @IBOutlet weak var m_lblBudget: UILabel!
    @IBOutlet weak var m_lblWhere: UILabel!
    @IBOutlet weak var m_datePicker: UIDatePicker!
    @IBOutlet weak var m_constBottomPicker: NSLayoutConstraint!
    
    var latitude = ""
    var longitude = ""
    var delegate:searchPartyDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
        m_constBottomPicker.constant = -(UIScreen.main.bounds.height)

    }

    func basicSetUp() {
        m_constTop.constant = topView.frame.height + 15
        addBackButton(withImage: #imageLiteral(resourceName: "ic_close").maskWithColor(color: .white)!,isFirstHierarchy: false)
        addTopTitle(title: "Search")
        m_viewWhen.addShadowDefault(isOnlyRightSide: true)
        m_viewBudget.addShadowDefault(isOnlyRightSide: true)
        m_viewWhere.addShadowDefault(isOnlyRightSide: true)
        
        
        let tapGestureWhen = UITapGestureRecognizer(target: self, action: #selector(showDatePicker))
        tapGestureWhen.numberOfTapsRequired = 1
        m_viewWhen.isUserInteractionEnabled = true
        m_viewWhen.addGestureRecognizer(tapGestureWhen)
        
        let tapGestureWhere = UITapGestureRecognizer(target: self, action: #selector(openGooglePlacePicker))
        tapGestureWhere.numberOfTapsRequired = 1
        m_viewWhere.isUserInteractionEnabled = true
        m_viewWhere.addGestureRecognizer(tapGestureWhere)
    }

    override func backClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Open date picker
    @objc func showDatePicker(){
        //Formate Date
        m_datePicker.datePickerMode = .date
        m_constBottomPicker.constant = 0
        
        UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0.1, options: .curveEaseInOut, animations: {
            self.m_constBottomPicker.constant = 0
            self.view.layoutIfNeeded()
        }) { _ in

        }
    }
    @IBAction func donedatePicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        m_lblWhen.text = formatter.string(from: m_datePicker.date)
        //dismiss date picker dialog
        
        UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            self.m_constBottomPicker.constant = -(UIScreen.main.bounds.height)
            self.view.layoutIfNeeded()
        }) { _ in
            
        }

    }
    @IBAction func cancelDatePicker(){
        //cancel button dismiss datepicker dialog
        UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            self.m_constBottomPicker.constant = -(UIScreen.main.bounds.height)
            self.view.layoutIfNeeded()
        }) { _ in
            
        }
    }
    
    //MARK:- Open google location controller
    @objc func openGooglePlacePicker() {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.formattedAddress.rawValue) | UInt(GMSPlaceField.name.rawValue) | UInt(GMSPlaceField.coordinate.rawValue) |
            UInt(GMSPlaceField.placeID.rawValue))!
        autocompleteController.placeFields = fields
        
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .geocode
        autocompleteController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)

    }
    
    @IBAction func actionSearch(_ sender: DefaultButton) {
        self.delegate.searchParties(lat: latitude, long: longitude, dt: m_lblWhen.text!)
        self.dismiss(animated: true, completion: nil)
    }
}

extension SearchPartyVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//        print("Place name: \(place.name)")
//        print("Place ID: \(place.placeID)")
//        print("Place attributions: \(place.attributions)")
//        print(place.coordinate.latitude,place.coordinate.longitude)
        dismiss(animated: true, completion: nil)
        
        latitude = "\(place.coordinate.latitude)"
        longitude = "\(place.coordinate.longitude)"
        m_lblWhere.text = place.name
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
