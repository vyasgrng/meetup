//
//  UpcomingPartiesVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 18/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import GooglePlaces



class UpcomingPartiesVC: BaseVC{
    
    // Variables
    var arrayPartyList:Array<JSON> = []
    var partyId = ""
    var objBottomView: ShareEditDelView?
    
    // Outlets
    @IBOutlet weak var m_tblViewEnjoy: UITableView!
    @IBOutlet weak var m_imgViewEmptyEvent: UIImageView!
    @IBOutlet weak var m_constTop: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        // Call basic set up
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.basicSetUp()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //Get enjoted list
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.02) {
            self.m_constTop.constant = self.topView.frame.height + 15
            self.pastPartiesListAPI()
        }
    }
    
    //MARK:- Set initial UI
    func basicSetUp() {
        
        addBackButton(withImage: #imageLiteral(resourceName: "ic_back").maskWithColor(color: .white)!)
        addTopTitle(title: "Upcoming Parties")
        self.m_constTop.constant = self.topView.frame.height + 15
        self.m_tblViewEnjoy.delegate = self
        self.m_tblViewEnjoy.dataSource = self
    }
    
    override func backClicked(_ sender: UIButton) {
        self.slideMenuController()?.openLeft()
    }
    
    //MARK:- Get enjoyed list
    func pastPartiesListAPI(isFilter:Bool = false) {
        AppSingleton.shared.startLoading()
        var params = ["":""]
        params = ["token":UserDefaults.standard.currentToken()!,"page":"0"]
        
        Alamofire.request(AppConstant.API.upcomingParties, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        self.arrayPartyList = responseObject["data"]!["events"].arrayValue
                        self.m_imgViewEmptyEvent.isHidden = true
                        self.m_tblViewEnjoy.isHidden = false
                        if self.arrayPartyList.count == 0 {
                            self.m_imgViewEmptyEvent.isHidden = false
                            self.m_tblViewEnjoy.isHidden = true
                        }
                        self.m_tblViewEnjoy.reloadData()
                        
                    }
                } else {
                    AppSingleton.shared.stopLoading()
                }
            case .failure(let error):
                print(error)
                AppSingleton.shared.stopLoading()
            }
        }
    }
    
    @objc func actionMore(_ sender:UIButton) {
        if let data = arrayPartyList[sender.tag].dictionary,
            let id = data["id"]?.string {
            let view = ShareEditDelView.getInstance(partyId: id, owner: self, refresh: {
                self.pastPartiesListAPI()
            })
            view.isUpcoming = true
            view.show()
        }
    }
}


extension UpcomingPartiesVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayPartyList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data = arrayPartyList[indexPath.row].dictionaryValue
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "EnjoyedCell", for: indexPath) as? EnjoyedCell
        cell?.m_btnJoin.addCornerRadius(radius: 3.0)
        cell?.m_btnJoin.isHidden = true
        cell?.m_btnTheme.addCornerRadius(radius: 5.0)
        cell?.m_btnTheme.addBorder(width: 1, color: UIColor().convertHexStringToColor(hexString: "202564"))
        cell?.m_btnTheme.setTitle(data["theme"]?.stringValue, for: .normal)
        cell?.m_btnTheme.contentEdgeInsets = UIEdgeInsets(top: 5, left: 8, bottom: 5, right: 8)
        let priceValue = String(format: "%.3f", data["price"]!.floatValue)
        cell?.m_lblPrice.text = "\(data["currency_readable"]?.stringValue ?? "USD") \(priceValue)"
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            cell?.m_viewDate.addCornerRadiusParticularSide(rectCorner: [.topLeft,.bottomLeft], radius: ((cell?.m_viewDate.frame.height)!/2))
        }
        let partyTime = (data["start"]?.doubleValue.timeStampToDate(format: "hh:mm a"))! + " to " + (data["end"]?.doubleValue.timeStampToDate(format: "hh:mm a"))!
        cell?.m_lblDate.text = (data["date"]?.doubleValue.timeStampToDate(format: "dd MMM yyyy"))! + " / " + partyTime + " / " + data["city"]!.stringValue
        cell?.m_lblName.text = data["name"]?.stringValue
        cell?.m_imgView.kf.setImage(with: URL(string: (data["image"]?.stringValue)!), placeholder: UIImage(named: "image01"), options: [], progressBlock: nil, completionHandler: { (result) in
            cell?.m_imgView.addCornerRadius(radius: 10.0)
        })
        cell?.m_imgViewHost.kf.setImage(with: URL(string: (data["host_image"]?.stringValue)!), placeholder: UIImage(named: "profile"), options: [], progressBlock: nil, completionHandler: { (result) in
            cell?.m_imgViewHost.addCornerRadius(radius: (cell?.m_imgViewHost.frame.size.width)!/2)
        })

        cell?.m_wraperView.addShadowWithCorner(cornerRadius: 10.0, offset: .zero, color: .lightGray , radius: 4.0, opacity: 0.6)
        cell?.m_lblLikeCount.text = data["like_count"]?.stringValue
        cell?.m_lblCommentCount.text = data["review_count"]?.stringValue
        cell?.m_btnMore.tag = indexPath.row
        cell?.m_btnMore.addTarget(self, action: #selector(actionMore(_:)), for: .touchUpInside)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = arrayPartyList[indexPath.row].dictionaryValue
        let vcObj = UIStoryboard(name: "Party", bundle:Bundle.main).instantiateViewController(withIdentifier: "PartyDetailVC") as? PartyDetailVC
        vcObj?.partyId = (data["id"]?.stringValue)!
        self.navigationController?.pushViewController(vcObj!, animated: true)
    }
}


