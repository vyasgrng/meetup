//
//  SideMenuVC.swift
//  Meetup App
//
//  Created by Birju Bhatt on 09/12/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import GoogleSignIn
import FBSDKLoginKit

class SideMenuVC: UIViewController {

    @IBOutlet weak var m_lblEmail: UILabel!
    @IBOutlet weak var m_lblName: UILabel!
    @IBOutlet weak var m_tblView: UITableView!
    @IBOutlet weak var m_imgViewUser: UIImageView!
    var isSubOpen = Bool()
    var isSubOpenHire = Bool()
    //var arrImg = [UIImage()]
    //var arrImgWithSub = [UIImage()]
    //var arrImgWithSubHire = [UIImage()]
    //var arrTitle = [""]
   // var arrTitleWithSub = [""]
    //var arrTitleWithSubHire = [""]
    var arrMainTitles = [""]
    var arrMainImages = [UIImage()]
    var mainViewController: UIViewController!
    var objRevenueView: RevenueView?
    var isJoinee = false

    override func viewDidLoad() {
        super.viewDidLoad()
        isJoinee = UserDefaults.standard.currentUserType() == UserType.Joinee.rawValue
        // Do any additional setup after loading the view.
        setProperty()
        setArrayValues()

    }
    
    func setArrayValues() {
        if isJoinee {
            if isSubOpen {
                arrMainImages = [#imageLiteral(resourceName: "noun_Gift_797120"),#imageLiteral(resourceName: "noun_verified_426713"),#imageLiteral(resourceName: "noun_Settings_893715"),#imageLiteral(resourceName: "noun_switch_369637"),#imageLiteral(resourceName: "noun_Party_1534892"),#imageLiteral(resourceName: "noun_past_856837"),#imageLiteral(resourceName: "noun_Future_856834"),#imageLiteral(resourceName: "noun_Ticket_1005396"),#imageLiteral(resourceName: "noun_help_1009845"),#imageLiteral(resourceName: "noun_paper plane_2900967"),#imageLiteral(resourceName: "noun_logout_2815367")]
                arrMainTitles = ["Invite Friends","Verification","Account Settings","Switch to Host","Your Parties","Past Parties","Upcoming Parties","Tickets","Help","Feedback","Logout"]
            } else {
                arrMainImages = [#imageLiteral(resourceName: "noun_Gift_797120"),#imageLiteral(resourceName: "noun_verified_426713"),#imageLiteral(resourceName: "noun_Settings_893715"),#imageLiteral(resourceName: "noun_switch_369637"),#imageLiteral(resourceName: "noun_Party_1534892"),#imageLiteral(resourceName: "noun_help_1009845"),#imageLiteral(resourceName: "noun_paper plane_2900967"),#imageLiteral(resourceName: "noun_logout_2815367")]
                arrMainTitles = ["Invite Friends","Verification","Account Settings","Switch to Host","Your Parties","Help","Feedback","Logout"]
            }
        } else {
        
            arrMainImages = [#imageLiteral(resourceName: "noun_revenue_831859"),#imageLiteral(resourceName: "noun_Block User_541488"),#imageLiteral(resourceName: "noun_Gift_797120"),#imageLiteral(resourceName: "noun_verified_426713"),#imageLiteral(resourceName: "noun_Settings_893715"),#imageLiteral(resourceName: "noun_switch_369637"),#imageLiteral(resourceName: "noun_Party_1534892"),#imageLiteral(resourceName: "noun_service_842366"),#imageLiteral(resourceName: "noun_help_1009845"),#imageLiteral(resourceName: "noun_paper plane_2900967"),#imageLiteral(resourceName: "noun_logout_2815367")]
            arrMainTitles = ["Revenue","Blocked Users","Invite Friends","Verification","Account Settings","Switch to Joinee","Your Parties","Hire Services","Help","Feedback","Logout"]
            if isSubOpen {
                let startIndex = arrMainTitles.index(of: "Your Parties")! + 1
                arrMainImages.insert(#imageLiteral(resourceName: "noun_past_856837"), at: startIndex)
                arrMainTitles.insert("Past Parties", at: startIndex)
                arrMainImages.insert(#imageLiteral(resourceName: "noun_qr_1635947"), at: startIndex + 1)
                arrMainTitles.insert("QR List", at: startIndex + 1)
            }
            if isSubOpenHire {
                let startIndex = arrMainTitles.index(of: "Hire Services")! + 1
                arrMainImages.insert(#imageLiteral(resourceName: "noun_cleaner_1753623-1"), at: startIndex)
                arrMainImages.insert(#imageLiteral(resourceName: "noun_Restaurant_1988887-1"), at: startIndex + 1)
                arrMainImages.insert(#imageLiteral(resourceName: "noun_Clubs_2020528-1"), at: startIndex + 2)
                arrMainImages.insert(#imageLiteral(resourceName: "noun_Transportation_2231965-1"), at: startIndex + 3)
                arrMainTitles.insert("Cleaner", at: startIndex)
                arrMainTitles.insert("Restaurant", at: startIndex + 1)
                arrMainTitles.insert("Clubs", at: startIndex + 2)
                arrMainTitles.insert("Transportation", at: startIndex + 3)
            }
        }
    }

    @objc func editProfGesture() {
        openEditProfile()
    }
    
    @IBAction func actionEditProfile(_ sender: UIButton) {
        openEditProfile()
    }
    
    func openEditProfile() {
        let storyboard = UIStoryboard(name: "InnerProfileSection", bundle: Bundle.main)
        let showProfVC = storyboard.instantiateViewController(withIdentifier: "ShowProfileVC") as! ShowProfileVC
        let nvc = UINavigationController(rootViewController: showProfVC)
        nvc.navigationBar.isHidden = true
        self.slideMenuController()?.changeMainViewController(nvc, close: true)
    }
    
    func setProperty() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(editProfGesture))
        tapGesture.numberOfTapsRequired = 1
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.m_imgViewUser.addGestureRecognizer(tapGesture)
            self.m_imgViewUser.isUserInteractionEnabled = true
        }
        
        m_lblName.text = (AppSingleton.shared.userData!.first_name)! + " " + (AppSingleton.shared.userData!.last_name)!
        m_lblEmail.text = AppSingleton.shared.userData!.email
        
        m_imgViewUser.kf.setImage(with: URL(string: (AppSingleton.shared.userData!.profile_pic)!), placeholder: #imageLiteral(resourceName: "profile"), options: [], progressBlock: nil, completionHandler: { (result) in
            self.m_imgViewUser.addCornerRadius(radius: self.m_imgViewUser.frame.width / 2)
        })
    }
    
    func addRevenueView(amount:String) {
        objRevenueView = (Bundle.main.loadNibNamed("Revenue", owner: self, options: nil)![0] as! RevenueView)
        objRevenueView?.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        objRevenueView?.m_btnDone.addTarget(self, action: #selector(removeRevenueView), for: .touchUpInside)
        appDelegate.window?.addSubview(objRevenueView!)
        
        objRevenueView?.m_lblRevenueValue.text = amount
        
    }
    
    func getRevenueAmount() {
        AppSingleton.shared.startLoading()
        var params = ["":""]
        params = ["token":UserDefaults.standard.currentToken()!]
        
        Alamofire.request(AppConstant.API.revenue, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    AppSingleton.shared.stopLoading()
                    let responseObject = JSON(value).dictionaryValue
                    if responseObject["status"]?.intValue == 0 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else if responseObject["status"]?.intValue == 3 {
                        AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                    } else {
                        self.addRevenueView(amount: responseObject["data"]!["income"].stringValue)
                    }
                } else {
                    AppSingleton.shared.stopLoading()
                }
            case .failure(let error):
                print(error)
                AppSingleton.shared.stopLoading()
            }
        }
    }
    
    @objc func removeRevenueView() {
        objRevenueView?.removeFromSuperview()
    }
    
    fileprivate func toggleArrowCell(indexPath: IndexPath, totalExpRows: Int, isOpen: Bool) {
        let startRow = indexPath.row + 1
        m_tblView.beginUpdates()
        let array = (startRow ..< startRow + totalExpRows).map({IndexPath(row: $0, section: 0)})
        if !isOpen {
            m_tblView.deleteRows(at: array, with: .automatic)
        } else {
            m_tblView.insertRows(at: array, with: .automatic)
        }
        m_tblView.endUpdates()
        if let cell = m_tblView.cellForRow(at: indexPath) as? SideMenuCell {
            let transform = isOpen ? CGAffineTransform(rotationAngle: .pi / 2) : CGAffineTransform.identity
            UIView.animate(withDuration: 0.3, animations: {
                cell.m_imgViewArrow.transform = transform
            })
        }
    }
    
    func cellSelectionForJoinee(indexPath:IndexPath) {
        
        let storyboardInnerProfile = UIStoryboard(name: "InnerProfileSection", bundle: Bundle.main)
        let storyboardTab = UIStoryboard(name: "Tabbar", bundle: Bundle.main)
        
        switch arrMainTitles[indexPath.row] {
        case "Revenue":
            self.slideMenuController()?.closeLeft()
            getRevenueAmount()
            break
        case "Blocked Users":
            let vcObj = storyboardInnerProfile.instantiateViewController(withIdentifier: "BlockedUsersVC") as! BlockedUsersVC
            vcObj.topTitle = "Blocked Users"
            vcObj.rightBtnTitle = "Unblock"
            let nvc = UINavigationController(rootViewController: vcObj)
            nvc.navigationBar.isHidden = true
            self.slideMenuController()?.changeMainViewController(nvc, close: true)

            break
        case "Invite Friends":
            let inviteFrndVC = storyboardInnerProfile.instantiateViewController(withIdentifier: "InviteFriendVC") as! InviteFriendVC
            let nvc = UINavigationController(rootViewController: inviteFrndVC)
            nvc.navigationBar.isHidden = true
            self.slideMenuController()?.changeMainViewController(nvc, close: true)
            break
        case "Verification":
            let verificationVC = storyboardInnerProfile.instantiateViewController(withIdentifier: "VerificationVC") as! VerificationVC
            let nvc = UINavigationController(rootViewController: verificationVC)
            nvc.navigationBar.isHidden = true
            self.slideMenuController()?.changeMainViewController(nvc, close: true)
            break
        case "Account Settings":
            let accSettingVC = storyboardInnerProfile.instantiateViewController(withIdentifier: "AccountSettingVC") as! AccountSettingVC
            let nvc = UINavigationController(rootViewController: accSettingVC)
            nvc.navigationBar.isHidden = true
            self.slideMenuController()?.changeMainViewController(nvc, close: true)
            break
        case "Switch to Host","Switch to Joinee":
            if UserDefaults.standard.currentUserType() == UserType.Joinee.rawValue {
                UserDefaults.standard.setUserType(type: UserType.Host.rawValue)
            } else {
                UserDefaults.standard.setUserType(type: UserType.Joinee.rawValue)
            }
            isJoinee = !isJoinee
            appDelegate.setRootAfterLogin()
            break
        case "Your Parties":
            //isSubOpenHire = false
            isSubOpen = !isSubOpen
            setArrayValues()
            toggleArrowCell(indexPath: indexPath, totalExpRows: isJoinee ? 3 : 2, isOpen: isSubOpen)
            break
        case "Past Parties":
            let vcObj = storyboardTab.instantiateViewController(withIdentifier: "PastPartiesVC") as! PastPartiesVC
            let nvc = UINavigationController(rootViewController: vcObj)
            nvc.navigationBar.isHidden = true
            self.slideMenuController()?.changeMainViewController(nvc, close: true)
            break
        case "Hire Services":
            //isSubOpen = false
            isSubOpenHire = !isSubOpenHire
            setArrayValues()
            toggleArrowCell(indexPath: indexPath, totalExpRows: 4, isOpen: isSubOpenHire)
            break
        case "Upcoming Parties":
            let vcObj = storyboardTab.instantiateViewController(withIdentifier: "UpcomingPartiesVC") as! UpcomingPartiesVC
            let nvc = UINavigationController(rootViewController: vcObj)
            nvc.navigationBar.isHidden = true
            self.slideMenuController()?.changeMainViewController(nvc, close: true)

            break
        case "Tickets":
            let ticketsVC = storyboardTab.instantiateViewController(withIdentifier: "TicketsVC") as! TicketsVC
            let nvc = UINavigationController(rootViewController: ticketsVC)
            nvc.navigationBar.isHidden = true
            self.slideMenuController()?.changeMainViewController(nvc, close: true)
            break
        case "Help":
            let helpVC = storyboardInnerProfile.instantiateViewController(withIdentifier: "HelpVC") as! HelpVC
            let nvc = UINavigationController(rootViewController: helpVC)
            nvc.navigationBar.isHidden = true
            self.slideMenuController()?.changeMainViewController(nvc, close: true)

            break
        case "Feedback":
            let vcObj = storyboardInnerProfile.instantiateViewController(withIdentifier: "ContactUSVC") as? ContactUSVC
            vcObj?.topTitle = "Feedback"
            vcObj?.isFirstHierarchy = true
            let nvc = UINavigationController(rootViewController: vcObj!)
            nvc.navigationBar.isHidden = true
            self.slideMenuController()?.changeMainViewController(nvc, close: true)
            break
        case "Logout":
            showLogoutAlert()
            break
        case "Cleaner","Restaurant","Clubs","Transportation":
            let vcObj = storyboardTab.instantiateViewController(withIdentifier: "HireServicesVC") as! HireServicesVC
            let nvc = UINavigationController(rootViewController: vcObj  )
            nvc.navigationBar.isHidden = true
            self.slideMenuController()?.changeMainViewController(nvc, close: true)
            break
        case "QR List":
            let vcObj = storyboardTab.instantiateViewController(withIdentifier: "QRListVC") as! QRListVC
            let nvc = UINavigationController(rootViewController: vcObj)
            nvc.navigationBar.isHidden = true
            self.slideMenuController()?.changeMainViewController(nvc, close: true)
            break
        default:
            break
        }
    }
    
    
    //MARK:- Logout Alert
    func showLogoutAlert() {
        
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Meet Up", message: "You can always access your content by signing back in",preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { _ in
                //Cancel Action
            }))
            alert.addAction(UIAlertAction(title: "Log out",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            //Sign out action
                                            
                    AppSingleton.shared.startLoading()
                    let params = ["token":UserDefaults.standard.currentToken()!]
                    Alamofire.request(AppConstant.API.logOut, method: .post, parameters: params as Parameters, encoding: URLEncoding.default).responseJSON { response in
                        switch response.result {
                        case .success:
                            if let value = response.result.value {
                                AppSingleton.shared.stopLoading()
                                let responseObject = JSON(value).dictionaryValue
                                if responseObject["status"]?.intValue == 0 {
                                    AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                                } else if responseObject["status"]?.intValue == 3 {
                                    AppSingleton.shared.showErrorAlert(msg: (responseObject["message"]?.stringValue)!)
                                } else {
                                    
                                    SocketIOManager.sharedInstance.disconnectSocket()
                                    GIDSignIn.sharedInstance()?.signOut()
                                    LoginManager().logOut()
                                    let domain = Bundle.main.bundleIdentifier!
                                    UserDefaults.standard.removePersistentDomain(forName: domain)
                                    UserDefaults.standard.synchronize()
                                    var isVCFound = Bool()
                                    if self.navigationController?.viewControllers.count != nil {
                                        for vcs in (self.navigationController?.viewControllers)! {
                                            if vcs is WelcomeVC {
                                                isVCFound = true
                                                self.navigationController?.popToViewController(vcs, animated: true)
                                            }
                                        }
                                    }
                                    
                                    if !isVCFound {
                                        let vcObj = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WelcomeVC") as? WelcomeVC
                                        let navi = UINavigationController(rootViewController: vcObj!)
                                        navi.navigationBar.isHidden = true
                                        appDelegate.window?.rootViewController = navi
                                        appDelegate.window?.makeKeyAndVisible()
                                    }
                                    UserDefaults.standard.removeToken()
                                    UserDefaults.standard.setUserType(type: UserType.Joinee.rawValue)
                                    self.isJoinee = true
                                }
                            }
                        case .failure(let error):
                            print(error)
                        }
                    }
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- Cell setup normal
    func cellFillUpNormalCell(tableView:UITableView,indexPath:IndexPath) -> SideMenuCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as? SideMenuCell
        cell?.m_lbl.text = arrMainTitles[indexPath.row]
        cell?.m_imgViewLeft.image = arrMainImages[indexPath.row]
        cell?.m_lbl.font = UIFont().RobotoMedium(size: 14.0)
        cell?.contentView.backgroundColor = .white
        cell?.m_imgViewArrow.isHidden = true
        
        if UserDefaults.standard.currentUserType() == UserType.Joinee.rawValue {
            if indexPath.row == 4 {
                cell?.m_imgViewArrow.isHidden = false
                cell?.m_imgViewArrow.image = #imageLiteral(resourceName: "right_arrow")
            }
        } else {
            if indexPath.row == 6 || indexPath.row == 7 {
                cell?.m_imgViewArrow.isHidden = false
                cell?.m_imgViewArrow.image = #imageLiteral(resourceName: "right_arrow")
            }
        }
        
        if indexPath.row % 2 != 0 {
            cell?.contentView.backgroundColor = UIColor().convertHexStringToColor(hexString: "F9F9F9")
        }

        return cell!
    }

    //MARK:- Cell setup for Joinee
    func cellFillUpNormalCellForJoinee(tableView:UITableView,indexPath:IndexPath) -> SideMenuCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as? SideMenuCell
        cell?.m_lbl.text = arrMainTitles[indexPath.row]
        cell?.m_imgViewLeft.image = arrMainImages[indexPath.row]
        cell?.m_lbl.font = UIFont().RobotoMedium(size: 14.0)
        cell?.contentView.backgroundColor = .white
        cell?.m_imgViewArrow.isHidden = true
        if indexPath.row == 4 {
            cell?.m_imgViewArrow.isHidden = false
            cell?.m_imgViewArrow.image = #imageLiteral(resourceName: "right_arrow.png")
        }
        if indexPath.row % 2 != 0 {
            cell?.contentView.backgroundColor = UIColor().convertHexStringToColor(hexString: "F9F9F9")
        }
        return cell!
    }
    func cellFillUpSubCellForJoinee(tableView:UITableView,indexPath:IndexPath) -> SideMenuSubCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuSubCell", for: indexPath) as? SideMenuSubCell
        cell?.m_lbl.font = UIFont().RobotoMedium(size: 14.0)
        cell?.contentView.backgroundColor = .white
        cell?.m_lbl.text = arrMainTitles[indexPath.row]
        cell?.m_btnImg.setImage(arrMainImages[indexPath.row], for: .normal)
        if indexPath.row % 2 != 0 {
            cell?.contentView.backgroundColor = UIColor().convertHexStringToColor(hexString: "F9F9F9")
        }
        return cell!
    }

    
    //MARK:- Cell setup for Host
    func cellFillUpNormalCellForHost(tableView:UITableView,indexPath:IndexPath) -> SideMenuCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as! SideMenuCell
        
        cell.m_lbl.text = arrMainTitles[indexPath.row]
        cell.m_imgViewLeft.image = arrMainImages[indexPath.row]
        cell.m_lbl.font = UIFont().RobotoMedium(size: 14.0)
        cell.contentView.backgroundColor = .white
        
        let partyIndex = arrMainTitles.index(of: "Your Parties")
        let serviesIndex = arrMainTitles.index(of: "Hire Services")
        let arrowCells = [partyIndex, serviesIndex].compactMap({$0})
        
        let isArrowCell = arrowCells.contains(indexPath.row)
        cell.m_imgViewArrow.isHidden = !isArrowCell
        
        if isArrowCell {
            cell.m_imgViewArrow.image = #imageLiteral(resourceName: "right_arrow")
            if (isSubOpen && partyIndex == indexPath.row) || (isSubOpenHire && serviesIndex == indexPath.row) {
                cell.m_imgViewArrow.transform = CGAffineTransform(rotationAngle: .pi / 2)
            }
        
        }
        
        if indexPath.row % 2 != 0 {
            cell.contentView.backgroundColor = UIColor().convertHexStringToColor(hexString: "F9F9F9")
        }
        return cell
    }
    func cellFillUpSubCellForHost(tableView:UITableView,indexPath:IndexPath) -> SideMenuSubCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuSubCell", for: indexPath) as? SideMenuSubCell
        cell?.m_lbl.font = UIFont().RobotoMedium(size: 14.0)
        cell?.contentView.backgroundColor = .white
        cell?.m_lbl.text = arrMainTitles[indexPath.row]
        cell?.m_btnImg.setImage(arrMainImages[indexPath.row], for: .normal)
        if indexPath.row % 2 != 0 {
            cell?.contentView.backgroundColor = UIColor().convertHexStringToColor(hexString: "F9F9F9")
        }
        return cell!
    }
    
    //MARK:- Cell setup for Host when Hire clicked
    func cellFillUpNormalCellForHostHire(tableView:UITableView,indexPath:IndexPath) -> SideMenuCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as? SideMenuCell
        cell?.m_lbl.text = arrMainTitles[indexPath.row]
        cell?.m_imgViewLeft.image = arrMainImages[indexPath.row]
        cell?.m_lbl.font = UIFont().RobotoMedium(size: 14.0)
        cell?.contentView.backgroundColor = .white
        cell?.m_imgViewArrow.isHidden = true
        if indexPath.row == 6 {
            cell?.m_imgViewArrow.isHidden = false
            cell?.m_imgViewArrow.image = #imageLiteral(resourceName: "right_arrow")

        }
        
        if indexPath.row == 7 {
            cell?.m_imgViewArrow.isHidden = false
            cell?.m_imgViewArrow.image = #imageLiteral(resourceName: "down_arrow")
        }
        
        if indexPath.row % 2 != 0 {
            cell?.contentView.backgroundColor = UIColor().convertHexStringToColor(hexString: "F9F9F9")
        }
        return cell!
    }
    func cellFillUpSubCellForHostHire(tableView:UITableView,indexPath:IndexPath) -> SideMenuSubCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuSubCell", for: indexPath) as? SideMenuSubCell
        cell?.m_lbl.font = UIFont().RobotoMedium(size: 14.0)
        cell?.contentView.backgroundColor = .white
        cell?.m_lbl.text = arrMainTitles[indexPath.row]
        cell?.m_btnImg.setImage(arrMainImages[indexPath.row], for: .normal)
        if indexPath.row % 2 != 0 {
            cell?.contentView.backgroundColor = UIColor().convertHexStringToColor(hexString: "F9F9F9")
        }
        return cell!
    }
}

extension SideMenuVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMainTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if UserDefaults.standard.currentUserType() == UserType.Joinee.rawValue {
            if isSubOpen {
                switch indexPath.row {
                case 5,6,7:
                   return cellFillUpSubCellForJoinee(tableView: tableView, indexPath: indexPath)
                default:
                    return cellFillUpNormalCellForJoinee(tableView: tableView, indexPath: indexPath)
                }
            } else {
                return cellFillUpNormalCell(tableView: tableView, indexPath: indexPath)
            }
        }
        
        var subRows: [Int] = []
        
        if isSubOpen {
            subRows = [7,8]
        }
        
        if isSubOpenHire {
            let firstIndex = arrMainTitles.index(of: "Cleaner")!
            subRows += [firstIndex, firstIndex + 1, firstIndex + 2, firstIndex + 3]
        }
        
        if subRows.contains(indexPath.row) {
            return cellFillUpSubCellForHost(tableView: tableView, indexPath: indexPath)
        } else {
            return cellFillUpNormalCellForHost(tableView: tableView, indexPath: indexPath)
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        cellSelectionForJoinee(indexPath: indexPath)
    }
}

//extension SideMenuVC : SlideMenuControllerDelegate {
//    func leftWillOpen() {
//    }
//}
