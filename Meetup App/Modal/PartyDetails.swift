//
//  PartyDetails.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 4, 2020

import Foundation

struct PartyDetails : Codable {
    
    let address : String?
    let city : String?
    let country : String?
    let currencyReadable : String?
    let date : Double?
    let descriptionField : String?
    let end : Double?
    let id : String?
    let images : [String]?
    let lat : String
    let lng : String
    let myStatus : Int?
    let name : String?
    let noOfGuest : String
    let price : String
    let scanQr : Int?
    let start : Double?
    let state : String?
    let theme : String?
    let zipCode : String?
    let facilities: String?
    let terms: String?
    
    enum CodingKeys: String, CodingKey {
        case address
        case city
        case country
        case currencyReadable = "currency_readable"
        case date
        case descriptionField = "description"
        case end
        case id
        case images
        case lat
        case lng
        case myStatus = "my_status"
        case name
        case noOfGuest = "no_of_guest"
        case price
        case scanQr = "scan_qr"
        case start
        case state
        case theme
        case zipCode = "zip_code"
        case facilities, terms
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        address = try? values.decode(String.self, forKey: .address)
        city = try? values.decode(String.self, forKey: .city)
        country = try? values.decode(String.self, forKey: .country)
        currencyReadable = try? values.decode(String.self, forKey: .currencyReadable)
        date = try? values.decode(Double.self, forKey: .date)
        descriptionField = try? values.decode(String.self, forKey: .descriptionField)
        end = try? values.decode(Double.self, forKey: .end)
        id = try? values.decode(String.self, forKey: .id)
        images = try? values.decode([String].self, forKey: .images)
        if let lat = try? values.decode(Float.self, forKey: .lat) {
            self.lat = String(lat)
        } else {
            lat = ""
        }
        if let lng = try? values.decode(Float.self, forKey: .lng) {
            self.lng = String(lng)
        } else {
            lng = ""
        }
        myStatus = try? values.decode(Int.self, forKey: .myStatus)
        name = try? values.decode(String.self, forKey: .name)
        if let noOfGuest = try? values.decode(Int.self, forKey: .noOfGuest) {
            self.noOfGuest = String(noOfGuest)
        } else {
            self.noOfGuest = ""
        }
        if let price = try? values.decode(Int.self, forKey: .price) {
            self.price = String(price)
        } else {
            self.price = ""
        }
        scanQr = try? values.decode(Int.self, forKey: .scanQr)
        start = try? values.decode(Double.self, forKey: .start)
        state = try? values.decode(String.self, forKey: .state)
        theme = try? values.decode(String.self, forKey: .theme)
        zipCode = try? values.decode(String.self, forKey: .zipCode)
        facilities = try? values.decode(String.self, forKey: .facilities)
        terms = try? values.decode(String.self, forKey: .terms)
    }
    
}
