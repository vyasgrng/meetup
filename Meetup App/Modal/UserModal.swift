//
//  UserModal.swift
//  Meetup App
//
//  Created by Birju Bhatt on 28/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import SwiftyJSON

class UserModal: NSObject, Codable {
    var contact_verified:String?
    var dob:String?
    var document_verified:String?
    var documents:Array<JSON>?
    var email:String?
    var email_verified:String?
    var facebook_linked:String?
    var first_name:String?
    var gender:String?
    var google_linked:String?
    var hobby:String?
    var id:String?
    var language:String?
    var last_name:String?
    var mobile_no:String?
    var mobile_cc:String?
    var profile_pic:String?
    var school:String?
    var token:String?
    var university:String?
    var work:String?
    var currency_readable:String?
    var currency:String?
    var address:String?
    var city:String?
    var state:String?
    var zip_code:String?
    var country:String?
    var notification_support:String?
    var notification_event:String?
    var invite_code:String?
    var payment_account:String?
    var notification_reminder:String?
    var notification_chat:String?
    var wallet:String?
    var referred:String?
    
    
    init(data:JSON) {
        let finalData = data["data"].dictionaryValue
        self.address = finalData["address"]!.stringValue
        self.city = finalData["city"]!.stringValue
        self.state = finalData["state"]!.stringValue
        self.zip_code = finalData["zip_code"]!.stringValue
        self.country = finalData["country"]!.stringValue
        self.contact_verified = finalData["contact_verified"]!.stringValue
        self.dob = finalData["dob"]!.stringValue
        self.document_verified = finalData["document_verified"]!.stringValue
        self.documents = finalData["documents"]!.arrayValue
        self.email = finalData["email"]!.stringValue
        self.email_verified = finalData["email_verified"]!.stringValue
        self.facebook_linked = finalData["facebook_linked"]!.stringValue
        self.first_name = finalData["first_name"]!.stringValue
        self.gender = finalData["gender"]!.stringValue
        self.google_linked = finalData["google_linked"]!.stringValue
        self.hobby = finalData["hobby"]!.stringValue
        self.id = finalData["id"]!.stringValue
        self.language = finalData["language"]!.stringValue
        self.last_name = finalData["last_name"]!.stringValue
        self.mobile_no = finalData["mobile_no"]!.stringValue
        self.profile_pic = finalData["profile_pic"]!.stringValue
        self.school = finalData["school"]!.stringValue
        self.token = finalData["token"]!.stringValue
        self.university = finalData["university"]!.stringValue
        self.work = finalData["work"]!.stringValue
        self.currency_readable = finalData["currency_readable"]?.stringValue
        self.currency = finalData["currency"]?.stringValue
        self.mobile_cc = finalData["mobile_cc"]?.stringValue
        self.notification_support = finalData["notification_support"]?.stringValue
        self.notification_event = finalData["notification_event"]?.stringValue
        self.invite_code = finalData["invite_code"]?.stringValue
        self.payment_account = finalData["payment_account"]?.stringValue
        self.notification_reminder = finalData["notification_reminder"]?.stringValue
        self.notification_chat = finalData["notification_chat"]?.stringValue
        self.wallet = finalData["wallet"]?.stringValue
        self.referred = finalData["referred"]?.stringValue
    }
}
