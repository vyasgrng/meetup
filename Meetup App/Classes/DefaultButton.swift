//
//  DefaultButton.swift
//  Meetup App
//
//  Created by Aditi Pancholi on 26/07/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import Foundation
import UIKit

class DefaultButton : UIButton {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setUp()
    }
    
    func setUp() {
        self.backgroundColor = UIColor().convertHexStringToColor(hexString: "202564")
        self.addCornerRadius(radius: 8)
//        self.addBorder(width: 1.0, color: UIColor(red: 189, green: 211, blue: 229, alpha: 1.0))
//        applyShadow()
        
    }
    func applyShadow() {
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.10).cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowOpacity = 0.6
        self.layer.shadowRadius = 2.0
        self.layer.masksToBounds = false
    }
    
    func removeCorners() {
        self.cornerRadious = 0.0
    }
}
