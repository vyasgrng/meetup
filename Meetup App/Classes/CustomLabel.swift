
import UIKit

typealias completionHandler = (_ object:CustomLabel,_ tapGesture:UITapGestureRecognizer) -> ()

class CustomLabel: UILabel {

    var customWords:Array<String> = []
    var closure:completionHandler?
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    func commonInit(){
        self.clipsToBounds = true
        self.textColor = UIColor.black
        self.gestureSetUp()
    }
    func gestureSetUp() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapped(gesture:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        self.addGestureRecognizer(tap)
        self.isUserInteractionEnabled = true
    }
    
    @objc func tapped(gesture:UITapGestureRecognizer) {
        self.tapLabel(closure: self.closure!, tap: gesture)
    }
    
    // MARK:- Action terms/privacy tapped
     func tapLabel(closure:@escaping completionHandler,tap:UITapGestureRecognizer) {
        self.closure!(self,tap)
    }

    func setCustomWordsProperties(words:[String],mainColor:UIColor,customColor:UIColor,font:UIFont,customClosure:@escaping completionHandler) {
        closure = nil
        self.closure = customClosure
        self.textColor = mainColor
        self.setAttributedStringArrayWithBold(wholeText: self.text!, boldStringArr: words, withFont: self.font, boldStringFont: font,color: customColor)
    }
}
extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}
extension Range where Bound == String.Index {
    var nsRange:NSRange {
        return NSRange(location: self.lowerBound.encodedOffset,
                       length: self.upperBound.encodedOffset -
                        self.lowerBound.encodedOffset)
    }
}
private extension UILabel {
    func setAttributedStringArrayWithBold(wholeText:String,boldStringArr:Array<String>,withFont:UIFont,boldStringFont:UIFont,color:UIColor)
    {
        let string = wholeText as NSString
        let attributedString = NSMutableAttributedString(string: wholeText as String, attributes: [NSAttributedString.Key.font:withFont])
        let boldFontAttribute = [NSAttributedString.Key.font: boldStringFont]
        let colorFontAttribute = [NSAttributedString.Key.foregroundColor:color]
        for i in boldStringArr {
            attributedString.addAttributes(boldFontAttribute, range: string.range(of: i))
            attributedString.addAttributes(colorFontAttribute, range: string.range(of: i))
        }
        self.attributedText = attributedString
    }
}
