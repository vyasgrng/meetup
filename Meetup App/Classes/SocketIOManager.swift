//
//  SocketIOManager.swift
//  Meetup App
//
//  Created by Birju Bhatt on 24/04/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import SocketIO
import SwiftyJSON

class SocketIOManager: NSObject {
    static let sharedInstance = SocketIOManager()
    let manager = SocketManager(socketURL: URL(string: "https://chillohome.com")!, config: [.log(false), .compress])
    var isConnect = Bool()
    override init() {
        super.init()
    }
    
    func socketConnect() {
        let socket = manager.defaultSocket
        socket.connect()
        socket.on(clientEvent: .connect) {data, ack in
            print("Socket connected")
            self.joinUser()
        }

        socket.on(clientEvent: .disconnect) {data, ack in
            print("Socket Disconnect")
            self.isConnect = false
            socket.on(clientEvent: .reconnect, callback: { data, ack in
                self.joinUser()
            })
        }
        
        socket.on(clientEvent: .error) {data, ack in
            print("Socket Error")
            self.isConnect = false
            socket.on(clientEvent: .reconnect, callback: { data, ack in
                self.joinUser()
            })
        }
    }
    
    //MARK:- Join Socket with User
    func joinUser() {
        let data:[String:String] = ["token":(AppSingleton.shared.userData?.token)!]
        let jsonStr = AppSingleton.shared.dictToJSONString(dict: data)
        SocketIOManager.sharedInstance.manager.defaultSocket.emitWithAck("join",with: [jsonStr]).timingOut(after: 0) { data in
            print(data)
        }
    }
    
    //MARK: disconnect from socket
    func disconnectSocket(){
        let socket = manager.defaultSocket
        socket.disconnect()
        self.isConnect = false
    }
}
