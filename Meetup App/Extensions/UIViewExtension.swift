//
//  UIViewExtension.swift
//  Meetup App
//
//  Created by Birju Bhatt on 13/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit


enum GradientDirection {
    case leftToRight
    case rightToLeft
    case topToBottom
    case bottomToTop
}

extension UIView {
    
    //MARK:- Add corner radius
    func addCornerRadius(radius:CGFloat) {
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
    
    //MARK:- Add shadow
    func addShadow(offset:CGSize, color:UIColor, opacity:Float, radius:CGFloat) {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offset
        self.layer.shadowRadius = radius
        self.layer.masksToBounds = false
    }
    
    
    //MARK: Add Shadow and corner radious
    func addShadowForRoundedcorners(viewToAddShadow : UIView, cornerRadius: CGFloat, shadowColor : UIColor,shadowOpacity : Float, shadowOffset:CGSize) {
        let shadowView = UIView()
        shadowView.backgroundColor = shadowColor
        shadowView.layer.opacity = shadowOpacity
        shadowView.layer.cornerRadius = cornerRadius
        shadowView.frame = CGRect(origin: CGPoint(x: viewToAddShadow.frame.origin.x + shadowOffset.width, y: viewToAddShadow.frame.origin.y + shadowOffset.height), size: CGSize(width: viewToAddShadow.bounds.width - shadowOffset.width, height: viewToAddShadow.bounds.height))
        self.insertSubview(shadowView, at: 0)
        self.bringSubviewToFront(viewToAddShadow)
    }
    
    func addShadowWithCorner(cornerRadius:CGFloat,offset: CGSize, color: UIColor, radius: CGFloat, opacity: Float) {
        
        clipsToBounds = true
        layer.cornerRadius = cornerRadius
        
        layer.masksToBounds = false
        layer.shadowOffset = offset
        layer.shadowColor = color.cgColor
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
        
        let backgroundCGColor = backgroundColor?.cgColor
        backgroundColor = nil
        layer.backgroundColor =  backgroundCGColor
    }
    
    // MARK:- Add common application shadow
    func addShadowDefault(isOnlyRightSide:Bool = false) {
        if isOnlyRightSide {
            self.addShadow(offset: CGSize(width: 3, height: 0), color: .lightGray, opacity: 1, radius: 1)
            self.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
            self.layer.borderWidth = 0.5
        } else {
            self.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
            self.layer.borderWidth = 0.5
            self.layer.backgroundColor = UIColor(displayP3Red: 247/255, green: 247/255, blue: 247/255, alpha: 1).cgColor
            self.addShadow(offset: CGSize(width: 0, height: 2), color: .lightGray, opacity: 0.6, radius: 1)
        }
    }
    
    func addSearchBarShadow() {
        self.addShadow(offset: CGSize.zero, color: .lightGray, opacity: 0.6, radius: 2)
    }

    
    //MARK:- Add border
    func addBorder(width:CGFloat = 1.0, color:UIColor = .red) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
    }
    
    //MARK:- Add corner radius particular side
    func addCornerRadiusParticularSide(rectCorner:UIRectCorner,radius:CGFloat) {
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.frame
        rectShape.position = self.center
        rectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: rectCorner, cornerRadii: CGSize(width: radius, height: radius)).cgPath
        self.layer.mask = rectShape
    }
    
    //MARK:- Add gradient bg
    func addGradient(from:UIColor = UIColor(red: 93, green: 73, blue: 230, alpha: 1.0), to:UIColor = UIColor(red: 40, green: 117, blue: 208, alpha: 1.0)) {
    
        self.addCornerRadius(radius: (self.frame.height) / 5)
        self.addBorder(width: 1.0, color: UIColor().convertHexStringToColor(hexString: "455ce4"))
        self.gradientBackground(from: from, to: to, direction: .rightToLeft)
        self.clipsToBounds = true

    }
    
    // MARK:- Remove layer at particular index
    func removeSublayer(layerIndex index: Int) {
        guard let sublayers = self.layer.sublayers else {
            print("The view does not have any sublayers.")
            return
        }
        if sublayers.count > index {
            self.layer.sublayers!.remove(at: index)
        } else {
            print("There are not enough sublayers to remove that index.")
        }
    }
    
    //MARK:- Add gradient background
    func gradientBackground(from color1: UIColor, to color2: UIColor, direction: GradientDirection) {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = [color1.cgColor, color2.cgColor]
        
        switch direction {
        case .leftToRight:
            gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        case .rightToLeft:
            gradient.startPoint = CGPoint(x: 1.0, y: 0.5)
            gradient.endPoint = CGPoint(x: 0.0, y: 0.5)
        case .bottomToTop:
            gradient.startPoint = CGPoint(x: 0.5, y: 1.0)
            gradient.endPoint = CGPoint(x: 0.5, y: 0.0)
        default:
            break
        }
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    //MARK:- Add tap gesture
    // In order to create computed properties for extensions, we need a key to
    // store and access the stored property
    fileprivate struct AssociatedObjectKeys {
        static var tapGestureRecognizer = "MediaViewerAssociatedObjectKey_mediaViewer"
    }
    
    fileprivate typealias Action = (() -> Void)?
    
    // Set our computed property type to a closure
    fileprivate var tapGestureRecognizerAction: Action? {
        set {
            if let newValue = newValue {
                // Computed properties get stored as associated objects
                objc_setAssociatedObject(self, &AssociatedObjectKeys.tapGestureRecognizer, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            }
        }
        get {
            let tapGestureRecognizerActionInstance = objc_getAssociatedObject(self, &AssociatedObjectKeys.tapGestureRecognizer) as? Action
            return tapGestureRecognizerActionInstance
        }
    }
    
    // This is the meat of the sauce, here we create the tap gesture recognizer and
    // store the closure the user passed to us in the associated object we declared above
    public func addTapGestureRecognizer(action: (() -> Void)?) {
        self.isUserInteractionEnabled = true
        self.tapGestureRecognizerAction = action
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
        self.addGestureRecognizer(tapGestureRecognizer)
    }
    
    // Every time the user taps on the UIImageView, this function gets called,
    // which triggers the closure we stored
    @objc fileprivate func handleTapGesture(sender: UITapGestureRecognizer) {
        if let action = self.tapGestureRecognizerAction {
            action?()
        } else {
            print("no action")
        }
    }
    
    
}


//MARK: UIView ibinspectable
extension UIView {
    
    @IBInspectable
    var cornerRadious: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
}
