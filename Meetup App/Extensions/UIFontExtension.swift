//
//  UIFontExtension.swift
//  Meetup App
//
//  Created by Birju Bhatt on 20/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

extension UIFont {
    //SFProDisplay-Regular
    //SFProDisplay-Bold
    //SFProDisplay-Semibold
    //SFProDisplay-Medium
    
    
//    "", "", "Roboto-", "Robot", "Roboto", "Roboto", "Roboto-", "Roboto-"
    
    func SFRegular(size:CGFloat) -> UIFont {
        return UIFont(name: "SFProDisplay-Regular", size: size)!
    }
    func RobotoRegular(size:CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Regular", size: size)!
    }
    func RobotoBlack(size:CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Black", size: size)!
    }
    func RobotoLight(size:CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Light", size: size)!
    }
    func RobotoBoldItalic(size:CGFloat) -> UIFont {
        return UIFont(name: "Roboto-BoldItalic", size: size)!
    }
    func RobotoLightItalic(size:CGFloat) -> UIFont {
        return UIFont(name: "Roboto-LightItalic", size: size)!
    }
    func RobotoThin(size:CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Thin", size: size)!
    }
    func RobotoMediumItalic(size:CGFloat) -> UIFont {
        return UIFont(name: "Roboto-MediumItalic", size: size)!
    }
    func RobotoMedium(size:CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Medium", size: size)!
    }
    func RobotoBold(size:CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Bold", size: size)!
    }
    func RobotoBlackItalic(size:CGFloat) -> UIFont {
        return UIFont(name: "Roboto-BlackItalic", size: size)!
    }
    func RobotoItalic(size:CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Italic", size: size)!
    }
    func RobotoThinItalic(size:CGFloat) -> UIFont {
        return UIFont(name: "Roboto-ThinItalic", size: size)!
    }

    func SFBold(size:CGFloat) -> UIFont {
        return UIFont(name: "SFProDisplay-Bold", size: size)!
    }
    func SFSemibold(size:CGFloat) -> UIFont {
        return UIFont(name: "SFProDisplay-Semibold", size: size)!
    }
    func SFMedium(size:CGFloat) -> UIFont {
        return UIFont(name: "SFProDisplay-Medium", size: size)!
    }
    
    func ABBook(size:CGFloat) -> UIFont {
        return UIFont(name: "AirbnbCerealApp-Book", size: size)!
    }
    func ABBold(size:CGFloat) -> UIFont {
        return UIFont(name: "AirbnbCerealApp-Bold", size: size)!
    }
    func ABLight(size:CGFloat) -> UIFont {
        return UIFont(name: "AirbnbCerealApp-Light", size: size)!
    }
    func ABMedium(size:CGFloat) -> UIFont {
        return UIFont(name: "AirbnbCerealApp-Medium", size: size)!
    }
}
