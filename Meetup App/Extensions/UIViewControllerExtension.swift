//
//  UIViewControllerExtension.swift
//  Meetup App
//
//  Created by Birju Bhatt on 14/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

extension UIViewController {
    
    // Push view controller
    func pushVC(destinationVC:UIViewController) {
        self.navigationController?.pushViewController(destinationVC, animated: true)
    }
    
    // Pop view controller
    func popVC() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //Pop view controller with source vc
    func popWithVC(sourceVC:UIViewController) {
        self.navigationController?.popToViewController(sourceVC, animated: true)
    }
    
    func getTabarVC() -> ParentVC? {
        return self.navigationController?.viewControllers.first(where: { $0 is ParentVC }) as? ParentVC
    }
    
}
