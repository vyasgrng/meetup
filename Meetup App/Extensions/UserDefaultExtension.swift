//
//  UserDefaultExtension.swift
//  Meetup App
//
//  Created by Birju Bhatt on 23/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

enum UserType:String {
    case Joinee = "Joinee"
    case Host = "Host"
}

extension UserDefaults {
    func setUserType(type:String) {
        UserDefaults.standard.set(type, forKey: "user")
        UserDefaults.standard.synchronize()
    }
    
    func currentUserType() -> String? {
        return UserDefaults.standard.value(forKey: "user") as? String
    }
    
    func storeToken(token:String) {
        UserDefaults.standard.set(token, forKey: "token")
        UserDefaults.standard.synchronize()
    }
    
    func removeToken() {
        UserDefaults.standard.removeObject(forKey: "token")
        UserDefaults.standard.synchronize()
    }
    
    func currentToken() -> String? {
        return UserDefaults.standard.value(forKey: "token") as? String ?? ""
    }
    
    func setDefaultCurrency(currency:String){
        UserDefaults.standard.set(currency, forKey: "defaultCurrency")
        UserDefaults.standard.synchronize()
    }
    
    func getDefaultCurrency()->String{
        return UserDefaults.standard.value(forKey: "defaultCurrency") as? String ?? "USD"
    }
    
    func setReferralCurrency(currency:String){
        UserDefaults.standard.set(currency, forKey: "ReferralCurrency")
        UserDefaults.standard.synchronize()
    }
    
    func getReferralCurrency()->String{
        return UserDefaults.standard.value(forKey: "ReferralCurrency") as? String ?? " "
    }
    
    func introVisit() {
        UserDefaults.standard.set(true, forKey: "introVisit")
        UserDefaults.standard.synchronize()
    }
    
    func isIntroVisit() -> Bool {
        return UserDefaults.standard.bool(forKey: "introVisit")
    }
    
    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
        AppSingleton.shared.assignUserDataToVariable()
    }
    
    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
            let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}

