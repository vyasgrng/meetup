//
//  AppConstant.swift
//  Meetup App
//
//  Created by Birju Bhatt on 27/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit

//var userDetails = AppSingleton.shared.userData
let BaseUrl = AppConstant.Urls.baseUrl
let SocketUrl = AppConstant.Urls.socketUrl
class AppConstant: NSObject {
    struct Urls {
        static let baseUrl = "https://chillohome.com/api/" // Live
        static let socketUrl = "https://chillohome.com"
    }
    
    struct Google {
//        static let APIKey = "AIzaSyD_SLCkP6qPbBxNZlPbjjDa2Ivil7cpwOM"
        static let APIKey = "AIzaSyCho74gJRlJS0y3hczqtSUZ8KxcnISZDms"
        static let clientID = "33037338655-s485iaqqhkpi58amitr3ara3f9har3nf.apps.googleusercontent.com"
        static let mapKey = "AIzaSyAe_QjcIuCzrsoo3bFmo72M-4UGm0FDtZc"
    }
    
    struct API {
        static let login = BaseUrl + "login"
        static let socialLogin = BaseUrl + "social-login"
        static let register = BaseUrl + "register"
        static let autoLogin = BaseUrl + "autologin"
        static let logOut = BaseUrl + "logout"
        static let forgotPassword = BaseUrl + "forgot-password"
        static let updateProfile = BaseUrl + "update-profile"
        static let enjoyedList = BaseUrl + "events/enjoyed"
        static let hitList = BaseUrl + "events/hitlist"
        static let onGoingList = BaseUrl + "events/ongoing"
        static let invitationList = BaseUrl + "events/requests"
        static let addParty = BaseUrl + "events/add"
        static let editParty = BaseUrl + "events/edit"
        static let contactUs = BaseUrl + "contact_submit"
        static let verifyEmail = BaseUrl + "verify-email"
        static let uploadDocument = BaseUrl + "upload-documents"
        static let deleteDocument = BaseUrl + "delete-document"
        static let partyDetail = BaseUrl + "events/detail"
        static let eventJoin = BaseUrl + "events/join"
        static let eventFavourite = BaseUrl + "events/hitlist/update"
        static let eventAcceptReject = BaseUrl + "events/join-response"
        static let calendar = BaseUrl + "events/calendar"
        static let onThisDay = BaseUrl + "events/on-this-day"
        static let hostDetail = BaseUrl + "host-detail"
        static let getCurrency = BaseUrl + "currencies"
        static let setCurrency = BaseUrl + "set-currency"
        static let socketJoin = SocketUrl + "/join"
        static let mediaUpload = BaseUrl + "media-upload"
        static let getUserDetails = BaseUrl + "get-details"
        static let cardList = BaseUrl + "list-cards"
        static let addCard = BaseUrl + "add-card"
        static let removeCard = BaseUrl + "remove-card"
        static let changeDefaultCard = BaseUrl + "change-default-card"
        static let setNotification = BaseUrl + "set-notification"
        static let redeemInviteCode = BaseUrl + "redeem-invite-code"
        static let changePassword = BaseUrl + "change-password"
        static let terms = BaseUrl + "terms"
        static let tickets = BaseUrl + "events/tickets"
        static let notifications = BaseUrl + "notifications"
        static let revenue = BaseUrl + "host-income"
        static let pastParties = BaseUrl + "events/past"
        static let upcomingParties = BaseUrl + "events/upcoming"
        static let qrListEvent = BaseUrl + "events/live"
        static let checkInList = BaseUrl + "events/check-in-list"
        static let scanQRCode = BaseUrl + "events/scan-qr"
        static let makePayment = BaseUrl + "events/make-payment"
        static let cancelBooking = BaseUrl + "events/cancel-booking"
        static let cancel = BaseUrl + "events/cancel"
        static let report = BaseUrl + "report"
        static let removeImage = BaseUrl + "remove_images"
    }
}

enum StoryboardID: String {
    case tabbar = "Tabbar"
    case party = "Party"
    
    var storyboard: UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: nil)
    }
    
    func getViewController(sceneId: String) -> UIViewController? {
        if #available(iOS 13.0, *) {
            return storyboard.instantiateViewController(identifier: sceneId)
        }
        return storyboard.instantiateViewController(withIdentifier: sceneId)
    }
}
