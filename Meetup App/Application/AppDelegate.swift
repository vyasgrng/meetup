//
//  AppDelegate.swift
//  Meetup App
//
//  Created by Birju Bhatt on 12/03/19.
//  Copyright © 2019 Birju Bhatt. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces
import GoogleSignIn
import FBSDKLoginKit
import FBSDKCoreKit

let appDelegate = UIApplication.shared.delegate as! AppDelegate
let sideMenuVCObj = SlideMenuController()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UITabBarControllerDelegate {

    var window: UIWindow?
//    var tabBarController = UITabBarController()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        if UserDefaults.standard.currentUserType() == nil  {
            UserDefaults.standard.setUserType(type: UserType.Joinee.rawValue)
        }
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        GMSServices.provideAPIKey(AppConstant.Google.APIKey)
        GMSPlacesClient.provideAPIKey(AppConstant.Google.mapKey)
        
        return (ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions))
    }
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        let isGoogleUrl = GIDSignIn.sharedInstance().handle(url as URL?, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        let str_URL = url.absoluteString as NSString
        if isGoogleUrl {
            return true
        } else {
            if str_URL.contains("fb410436436192166")
            {
                return ApplicationDelegate.shared.application(
                    app,
                    open: url,
                    sourceApplication: (options[UIApplication.OpenURLOptionsKey.sourceApplication] as! String),
                    annotation: options[UIApplication.OpenURLOptionsKey.annotation])
            }
        }
        return false
    }
    
    func setRootAfterLogin() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let storyBoardTab = UIStoryboard(name: "Tabbar", bundle: nil)
        
        let mainViewController = storyBoardTab.instantiateViewController(withIdentifier: "ParentVC") as! ParentVC
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        nvc.navigationBar.isHidden = true
        leftViewController.mainViewController = nvc
        
        let slideMenuController = SideMenuInheritVC(mainViewController:nvc, leftMenuViewController: leftViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        window?.rootViewController = slideMenuController
        window?.makeKeyAndVisible()
    }
    
    //MARK:- Socket connection
    func socketConnection() {
        SocketIOManager.sharedInstance.socketConnect()
    }
}

